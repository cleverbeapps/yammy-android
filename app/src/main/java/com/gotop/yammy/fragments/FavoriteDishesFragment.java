package com.gotop.yammy.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gotop.yammy.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteDishesFragment extends Fragment {


    public FavoriteDishesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite_dishes, container, false);
    }

}
