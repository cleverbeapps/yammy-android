package com.gotop.yammy.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.drawer.dialog.FilterCousinesDialog;
import com.gotop.yammy.drawer.dialog.FilterOrgTypesDialog;
import com.gotop.yammy.drawer.vo.CuisineVO;
import com.gotop.yammy.drawer.vo.OrgTypeVO;
import com.gotop.yammy.login_register.segment_picker.SegmentedGroup;
import com.gotop.yammy.service.ApiService;
import com.gotop.yammy.service.FilterService;
import com.gotop.yammy.utils.Helper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class FilterFragment extends Fragment {

    SegmentedGroup _sortSegment;
    View _cuisineLayout;
    View _placeTypeLayout;
    View _dishCategoryLayout;
    View _locationLayout;
    TextView _sortChoiseText;

    TextView _cuisinesText;
    TextView _placeTypesText;
    TextView _dishCategoriesText;
    TextView _locationText;

    public FilterFragment() {
        // Required empty public constructor
    }

    private EventBus bus = EventBus.getDefault();

    @Subscribe
    public void onEvent(FilterService.FilterUpdate event){
        updateFilters();
    }

    @Override
    public void onStart() {
        super.onStart();
        updateFilters();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        bus.register(this);

        View view = inflater.inflate(R.layout.fragment_filter,
                container, false);

        view.setBackgroundColor(Color.WHITE);

        _sortSegment = (SegmentedGroup) view.findViewById(R.id.fr_filter_sorted_segment);
        _cuisineLayout = view.findViewById(R.id.fr_filter_cuisine_layout);
        _placeTypeLayout = view.findViewById(R.id.fr_filter_place_type_layout);
        _dishCategoryLayout = view.findViewById(R.id.fr_filter_dish_category_layout);
        _locationLayout = view.findViewById(R.id.fr_filter_location_layout);
        _sortChoiseText = (TextView) view.findViewById(R.id.fr_filter_sorted_text);

        _cuisinesText = (TextView) view.findViewById(R.id.fr_filter_cuisine_text);
        _placeTypesText = (TextView) view.findViewById(R.id.fr_filter_place_type_text);
        _dishCategoriesText = (TextView) view.findViewById(R.id.fr_filter_dish_category_text);
        _locationText = (TextView) view.findViewById(R.id.fr_filter_location_text);

        _sortSegment.check(R.id.fr_filter_sorted_name_btn);

        _sortSegment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                setSortFilter(i);
            }
        });
        _cuisineLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFilterDialog(FilterService.CUISINE_FILTER_DIALOG);
            }
        });
        _placeTypeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFilterDialog(FilterService.PLACE_TYPE_FILTER_DIALOG);
            }
        });
        _dishCategoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFilterDialog(FilterService.DISH_CATEGORY_FILTER_DIALOG);
            }
        });
        _locationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFilterDialog(FilterService.LOCATION_FILTER_DIALOG);
            }
        });

        return view;
    }

    private void setSortFilter(int id){
        switch (id){
            case R.id.fr_filter_sorted_prise_btn:
                _sortChoiseText.setText(R.string.fr_filter_sort_price);
                FilterService.set_currentSort(FilterService.SORTED_BY_PRICE);
                break;
            case R.id.fr_filter_sorted_location_btn:
                _sortChoiseText.setText(R.string.filter_sort_location);
                FilterService.set_currentSort(FilterService.SORTED_BY_LOCATION);
                break;
            case R.id.fr_filter_sorted_name_btn:
                _sortChoiseText.setText(R.string.filter_sorted_by_alphabetical);
                FilterService.set_currentSort(FilterService.SORTED_BY_NAME);
                break;
        }
    }

    private void showFilterDialog(int id){
        switch (id){
            case FilterService.CUISINE_FILTER_DIALOG:
                    Helper.showLoadingDialog(getContext(), "Loading available cuisines", "Please, wait...");
                    ApiService.getCuisines(new ApiService.ApiResponse() {
                        @Override
                        public void response(Boolean isSuccess, Object payload) {
                            Helper.dismissLoadingDialog();
                            List<CuisineVO> cuisines = (List<CuisineVO>)payload;
                            if(isSuccess && cuisines != null){
                                showCuisinesDialog(cuisines);
                            }
                        }
                    });
                break;
            case FilterService.DISH_CATEGORY_FILTER_DIALOG:
                break;
            case FilterService.PLACE_TYPE_FILTER_DIALOG:
                Helper.showLoadingDialog(getContext(), "Loading available place types", "Please, wait...");
                ApiService.getOrgTypes(getContext(), new ApiService.ApiResponse() {
                    @Override
                    public void response(Boolean isSuccess, Object payload) {
                        Helper.dismissLoadingDialog();
                        List<OrgTypeVO> orgTypeVOs = (List<OrgTypeVO>)payload;
                        if(isSuccess && orgTypeVOs != null){
                            showOrgTypeDialog(orgTypeVOs);
                        }
                    }
                });
                break;
            case FilterService.LOCATION_FILTER_DIALOG:
                break;
        }
    }

    private FilterCousinesDialog filterCousinesDialog;
    private void showCuisinesDialog(List<CuisineVO> cuisineVOs){
        if(filterCousinesDialog != null){
            filterCousinesDialog.dismiss();
        }
        filterCousinesDialog = new FilterCousinesDialog(getContext(), cuisineVOs);
        filterCousinesDialog.show();
    }

    private FilterOrgTypesDialog filterOrgTypesDialog;
    private void showOrgTypeDialog(List<OrgTypeVO> orgTypeVOs){
        if(filterOrgTypesDialog != null){
            filterOrgTypesDialog.dismiss();
        }
        filterOrgTypesDialog = new FilterOrgTypesDialog(getContext(), orgTypeVOs);
        filterOrgTypesDialog.show();
    }

    private void updateFilters(){
        List<CuisineVO> filterCuisines = FilterService.getCuisineFilter();
        String filerCuisinesText = "Any Cuisines";
        if(filterCuisines.size() > 0){
            filerCuisinesText = "";
            for (int i = 0; i < filterCuisines.size(); i++) {
                CuisineVO cuisineVO = filterCuisines.get(i);
                if(i == 0){
                    filerCuisinesText = filerCuisinesText.concat(cuisineVO.name);
                }
                else{
                    filerCuisinesText = filerCuisinesText.concat(", "+cuisineVO.name);
                }
            }
        }
        _cuisinesText.setText(filerCuisinesText);

        List<OrgTypeVO> filterOrgTypes = FilterService.getOrgTypeFilter();
        String filerOrgTypesText = "Any Place Type";
        if(filterOrgTypes.size() > 0){
            filerOrgTypesText = "";
            for (int i = 0; i < filterOrgTypes.size(); i++) {
                OrgTypeVO orgTypeVO = filterOrgTypes.get(i);
                if(i == 0){
                    filerOrgTypesText = filerOrgTypesText.concat(orgTypeVO.name);
                }
                else{
                    filerOrgTypesText = filerOrgTypesText.concat(", "+orgTypeVO.name);
                }
            }
        }
        _placeTypesText.setText(filerOrgTypesText);


    }
}
