package com.gotop.yammy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gotop.yammy.drawer.DrawerActivity;
import com.gotop.yammy.drawer.OrgInfoActivity;
import com.gotop.yammy.drawer.vo.OrgVO;
import com.gotop.yammy.login_register.LoginRegisterActivity;
import com.gotop.yammy.service.ApiService;
import com.gotop.yammy.service.DemoService;
import com.gotop.yammy.service.OrderService;
import com.gotop.yammy.service.UserService;
import com.gotop.yammy.utils.Helper;
import com.facebook.FacebookSdk;
import com.squareup.picasso.Picasso;

public class InitialActivity extends AppCompatActivity {

    StartPageViewAdapter mStartPageViewAdapter;
    ViewPager mViewPager;
    LinearLayout pageIndicatorContainer;

    Button btnDemo, btnLogin;
    ProgressBar spinner;

    private static Boolean isFirstVisit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initServices();

        setContentView(R.layout.activity_initial);

        btnDemo = (Button)findViewById(R.id.btn_demo);
        btnLogin = (Button)findViewById(R.id.btn_login);
        spinner = (ProgressBar)findViewById(R.id.initial_circle_progress);
        mViewPager = (ViewPager) findViewById(R.id.start_view_pager);
        pageIndicatorContainer = (LinearLayout)findViewById(R.id.start_page_indicators);

        btnDemo.setAlpha(0f);
        btnLogin.setAlpha(0f);
        spinner.setAlpha(0f);
        mViewPager.setAlpha(0f);
        pageIndicatorContainer.setAlpha(0f);

        ImageView bg = (ImageView) findViewById(R.id.splash_image_bg);
        Picasso.with(getBaseContext())
                .load(R.drawable.splash)
                .fit()
                .centerCrop() //or .centerInside() to avoid a stretched image
                .into(bg, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        init();
                    }
                    @Override
                    public void onError() {

                    }
                });
    }

    private void initServices() {

        Helper.appContext = getApplicationContext();

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        OrderService.init();
        UserService.init();
        ApiService.init();
    }

    private void init(){

        spinner.animate().alpha(1f).setDuration(500);
        mViewPager.animate().alpha(1f).setDuration(500);
        pageIndicatorContainer.animate().alpha(1f).setDuration(500);

        mStartPageViewAdapter = new StartPageViewAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mStartPageViewAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {

                if (pageIndicatorContainer != null)
                {
                    for (int i = 0; i < pageIndicatorContainer.getChildCount(); i++)
                    {
                        Button btn = (Button)pageIndicatorContainer.getChildAt(i);
                        btn.setEnabled(false);

                        if(i == position){
                            btn.setEnabled(true);
                        }
                    }
                }
            }

        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogin();
            }
        });
        btnDemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDemo();
            }
        });

        if(isFirstVisit){

            isFirstVisit = false;

            hideButtons();

            String username = UserService.getUserVO().login;
            String password = UserService.getUserVO().password;

            if(TextUtils.isEmpty(username) || TextUtils.isEmpty(password)){
                showButtons();
            }
            else{
                if(Helper.isConnectionAvailable(this, false)){
                    ApiService.login(username, password, new ApiService.ApiResponse() {
                        @Override
                        public void response(Boolean isSuccess, Object payload) {
                            if(isSuccess){
                                showMainDrawer();
                            }
                            else{
                                showButtons();
                            }
                        }
                    });
                }
                else{
                    Toast.makeText(this, R.string.alert_no_connection_title, Toast.LENGTH_LONG).show();
                    showButtons();
                }
            }
        }
        else{
            showButtons();
        }
    }

    private void showMainDrawer(){

        DemoService.setIsDemoMode(false);

        Intent intent = new Intent(this, DrawerActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        //finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        DemoService.setIsDemoMode(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DemoService.setIsDemoMode(false);
        // Facebook logs 'install' and 'app activate' App Events.
        //AppEventsLogger.activateApp(getApplication());
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        //AppEventsLogger.deactivateApp(getApplication());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    private void showLogin(){

        if(Helper.isConnectionAvailable(this, false)){
            Intent intent = new Intent(this, LoginRegisterActivity.class);
            startActivity(intent);
        }
        else{
            Toast.makeText(this, R.string.alert_no_connection_title, Toast.LENGTH_LONG).show();
            showButtons();
        }
    }

    private void showDemo(){
        DemoService.setIsDemoMode(true);
        OrgVO orgVO = (OrgVO) DemoService.getDemoVO(this, DemoService.DEMO_RESTAURANT_VO_PATH);
        Intent intent = new Intent(this, OrgInfoActivity.class);
        OrgInfoActivity.setgetCurrentOrgVO(orgVO);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void hideButtons(){
        btnDemo.setClickable(false);
        btnLogin.setClickable(false);
        btnDemo.setAlpha(0f);
        btnLogin.setAlpha(0f);

        spinner.setVisibility(View.VISIBLE);
        spinner.animate().alpha(1f).setDuration(500);
    }
    private void showButtons(){
        btnDemo.setClickable(true);
        btnLogin.setClickable(true);
        btnDemo.animate().alpha(1f).setDuration(500);
        btnLogin.animate().alpha(1f).setDuration(500);

        spinner.setVisibility(View.GONE);
    }




    public static class StartPageViewAdapter extends FragmentPagerAdapter {

        public StartPageViewAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {

                default:
                    // The other sections of the app are dummy placeholders.
                    Fragment fragment = new InitialPageViewerPage();
                    Bundle args = new Bundle();
                    args.putInt(InitialPageViewerPage.ARG_SECTION_NUMBER, i + 1);
                    fragment.setArguments(args);
                    return fragment;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Section " + (position + 1);
        }
    }

    /**
     * A dummy fragment representing a section of the app, but that simply displays dummy text.
     */
    public static class InitialPageViewerPage extends Fragment {

        public static final String ARG_SECTION_NUMBER = "section_number";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            Bundle args = getArguments();
            int pageIndex = args.getInt(ARG_SECTION_NUMBER);

            View rootView = null;
            switch (pageIndex){
                case 1:
                    rootView = inflater.inflate(R.layout.start_page_viewer_page_1, container, false);
                    break;
                case 2:
                    rootView = inflater.inflate(R.layout.start_page_viewer_page_2, container, false);
                    break;
                case 3:
                    rootView = inflater.inflate(R.layout.start_page_viewer_page_3, container, false);
                    break;
                case 4:
                    rootView = inflater.inflate(R.layout.start_page_viewer_page_4, container, false);
                    break;
            }

            return rootView;
        }
    }
}
