package com.gotop.yammy.drawer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.drawer.vo.MenuDishVO;
import com.gotop.yammy.service.ApiService;
import com.gotop.yammy.service.DemoService;
import com.gotop.yammy.service.OrderService;
import com.gotop.yammy.service.UserService;
import com.gotop.yammy.utils.Helper;
import com.gotop.yammy.utils.IconsTextView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Objects;

public class OrderActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView toolbarTitle;
    private ImageButton backButton;
    private Button btnProccedOrder;
    private TextView orgNameText;
    private TextView orderTableText;
    private TextView orderTimeText;
    private IconsTextView orderTableIcon;
    private IconsTextView orderTimeIcon;
    private IconsTextView orderArrowIcon;
    private RecyclerView orderList;
    private TextView commentText;
    private IconsTextView commentIcon;
    private IconsTextView commentArrow;
    private TextView subtotalCount;
    private TextView totalCount;
    private RelativeLayout requestLayout;
    private RelativeLayout commentLayout;

    private EventBus bus = EventBus.getDefault();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        toolbar = (Toolbar)findViewById(R.id.order_toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_close_title);
        toolbarTitle.setText("Order");
        backButton = (ImageButton) toolbar.findViewById(R.id.toolbar_close_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        final Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(Color.LTGRAY);
        backButton.setColorFilter(ContextCompat.getColor(OrderActivity.this, R.color.gold));
        toolbarTitle.setHintTextColor(ContextCompat.getColor(OrderActivity.this, R.color.gold));
        toolbar.getBackground().setAlpha(255);    // // 20%  255 = 100%

        final ImageView headerImage = (ImageView) findViewById(R.id.act_order_header_image);
        final ProgressBar progressBar = (ProgressBar)findViewById(R.id.act_order_header_image_progress);

        btnProccedOrder = (Button) findViewById(R.id.act_order_btn_order);
        btnProccedOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int validationCode = OrderService.getValidationResult();
                if(validationCode == OrderService.VALID){
                    Intent intent = new Intent(OrderActivity.this, ReviewRequestActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
                else{
                    String invalidDescription = OrderService.getValidationDescription(validationCode);
                    Helper.showAlert(OrderActivity.this, "Invalid Order", invalidDescription);
                }
            }
        });

        requestLayout = (RelativeLayout)findViewById(R.id.act_order_time_layout);
        requestLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderActivity.this, RequestActivity.class);
                RequestActivity.isInvokedFromOrgInfoActivity = false;
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        });

        commentLayout = (RelativeLayout)findViewById(R.id.act_order_comment_layout);
        commentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderActivity.this, CommentActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        });

        orgNameText = (TextView) findViewById(R.id.act_order_org_name);
        orgNameText.setText(OrgInfoActivity.getCurrentOrgVO().name);
        orderList = (RecyclerView)findViewById(R.id.act_order_list);

        orderTableText = (TextView) findViewById(R.id.act_order_table_text);
        orderTimeText = (TextView) findViewById(R.id.act_order_time_text);

        subtotalCount = (TextView) findViewById(R.id.act_order_subtotal_count);
        totalCount = (TextView) findViewById(R.id.act_order_total_count);

        orderTableIcon = (IconsTextView)findViewById(R.id.act_order_table_icon);
        orderTableIcon.setText(String.valueOf((char) 0xe910));
        orderTimeIcon = (IconsTextView)findViewById(R.id.act_order_time_icon);
        orderTimeIcon.setText(String.valueOf((char) 0xe909));
        orderArrowIcon = (IconsTextView)findViewById(R.id.act_order_time_arrow_icon);
        orderArrowIcon.setText(String.valueOf((char) 0xe901));

        commentIcon = (IconsTextView)findViewById(R.id.act_order_comment_icon);
        commentIcon.setText(String.valueOf((char) 0xe91d));
        commentArrow = (IconsTextView)findViewById(R.id.act_order_comment_arrow);
        commentArrow.setText(String.valueOf((char) 0xe901));


        final OrderListAdapter dishListAdapter = new OrderListAdapter(this);
        dishListAdapter.activity = this;
        orderList.setAdapter(dishListAdapter);
        orderList.setLayoutManager(new LinearLayoutManager(this));

        updateOrder();

        bus.register(this);
        if(DemoService.getIsDemoMode()){
            int image = OrgInfoActivity.getCurrentOrgVO().header_image;
            Picasso.with(this)
                    .load(image)
                    .placeholder(R.drawable.org_cell_bg)
                    .fit()
                    .centerCrop()
                    .into(headerImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        @Override
                        public void onError() {

                        }
                    });
        }
        else{
            String imageURL = ApiService.getOrgImageURL(OrgInfoActivity.getCurrentOrgVO().header_image);
            Picasso.with(this)
                    .load(imageURL)
                    .placeholder(R.drawable.org_cell_bg)
                    .fit()
                    .centerCrop()
                    .into(headerImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        @Override
                        public void onError() {

                        }
                    });
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    @Subscribe
    public void onEvent(OrderService.OrderUpdate event){

        int orderCount = OrderService.getOrderCount();
        if(orderCount < 1){
            finish();
        }
        else {
            updateOrder();
        }
    }

    private void updateOrder(){

        Double orderPrice = OrderService.getOrderPrice();
        btnProccedOrder.setText(getString(R.string.btn_proceed_order_label) + " $" + String.format("%.2f", orderPrice));

        DateFormat dateFormat = SimpleDateFormat.getDateInstance();
        String requestDateText = dateFormat.format(OrderService.getRequestDate());

        orderTimeText.setText(requestDateText + ", " + OrderService.getRequestEarliestDateTime().getInlineString() + " - " + OrderService.getRequestLatestDateTime().getInlineString());

        if(Objects.equals(OrderService.getRequestOrderType(), OrderService.ORDER_TYPE_RESTAURANT)){
            orderTableText.setText("Table for "+OrderService.getRequestPeopleCount());
        }
        else if(Objects.equals(OrderService.getRequestOrderType(), OrderService.ORDER_TYPE_YOURSELF)){
            orderTableText.setText("Yourself");
        }
        else if(Objects.equals(OrderService.getRequestOrderType(), OrderService.ORDER_TYPE_DELIVERY)){
            orderTableText.setText("Delivery");
        }

        Double orderPriceWithTip = OrderService.getOrderPricePlusTip(UserService.getUserVO().orderTip);

        subtotalCount.setText("$" + String.format("%.2f", orderPrice));
        totalCount.setText("$" + String.format("%.2f", orderPriceWithTip));
    }

    public static class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListCell>{

        public OrderActivity activity;
        private Context mContext;

        public OrderListAdapter(Context context){

            mContext = context;
        }

        @Override
        public OrderListCell onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_list_row,parent,false);
            OrderListCell dishCell = new OrderListCell(v,viewType);
            dishCell.activity = activity;

            return dishCell;
        }

        @Override
        public void onBindViewHolder(final OrderListCell holder, int position) {

            final MenuDishVO currentDish = (MenuDishVO)OrderService.getOrder().keySet().toArray()[position];

            holder.lblDishName.setText(String.valueOf(currentDish.name));
            holder.lblDishDescription.setText(String.valueOf(currentDish.dishDescription));
            holder.lblPrice.setText("$" + String.format("%.2f", currentDish.price));

            holder.btnAddDish.setVisibility(View.VISIBLE);
            holder.btnDeleteDish.setVisibility(View.VISIBLE);
            holder.btnArrow.setVisibility(View.GONE);

            holder.setCurrentDish(currentDish);
        }

        @Override
        public void onViewDetachedFromWindow(OrderListCell holder) {
            super.onViewDetachedFromWindow(holder);

            holder.unsubscribe();
        }

        @Override
        public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
            super.onDetachedFromRecyclerView(recyclerView);
        }



        @Override
        public int getItemCount() {
            return OrderService.getOrder().keySet().toArray().length;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        public static class OrderListCell extends RecyclerView.ViewHolder{

            ImageButton btnAddDish, btnDeleteDish, btnArrow;
            TextView lblDishName, lblDishDescription, lblPrice, lblCount;
            RelativeLayout countLayout;
            Button btnRow;

            public OrderActivity activity;

            MenuDishVO currentDish = null;

            private EventBus bus = EventBus.getDefault();

            public OrderListCell(View itemView,int ViewType) {
                super(itemView);

                btnAddDish = (ImageButton) itemView.findViewById(R.id.dish_list_row_btn_add_dish);
                btnDeleteDish = (ImageButton)itemView.findViewById(R.id.btn_delete_dish);
                btnArrow = (ImageButton)itemView.findViewById(R.id.btn_arrow_right);
                lblDishName = (TextView)itemView.findViewById(R.id.text_dish_name);
                lblDishDescription = (TextView)itemView.findViewById(R.id.text_dish_description);
                lblPrice = (TextView)itemView.findViewById(R.id.text_dish_price);
                lblCount = (TextView)itemView.findViewById(R.id.dish_list_row_count_text);
                countLayout = (RelativeLayout) itemView.findViewById(R.id.dish_list_row_count_layout);
                btnRow = (Button) itemView.findViewById(R.id.dish_list_row_layout_btn);
                btnRow.setVisibility(View.GONE);

                btnAddDish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(currentDish != null)
                        {
                            OrderService.addDish(currentDish);
                        }
                    }
                });
                btnDeleteDish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(currentDish != null)
                        {
                            OrderService.removeDish(currentDish);
                        }
                    }
                });
            }

            public void setCurrentDish(MenuDishVO menuDishVO){
                this.currentDish = menuDishVO;
                updateCount();
                bus.register(this);
            }

            public void unsubscribe(){
                bus.unregister(this);
            }

            @Subscribe
            public void onEvent(OrderService.OrderUpdate event){
                updateCount();
            }

            private void updateCount(){

                if(currentDish != null){
                    int currentDishCount = OrderService.getDishCount(currentDish);
                    lblCount.setText(String.valueOf(currentDishCount));
                    countLayout.setVisibility(currentDishCount > 0 ? View.VISIBLE : View.INVISIBLE);
                }
            }
        }
    }
}
