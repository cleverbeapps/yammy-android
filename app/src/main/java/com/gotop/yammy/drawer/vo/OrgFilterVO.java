package com.gotop.yammy.drawer.vo;

import android.location.Location;

public class OrgFilterVO {

    public Location location;
    public String cityID;

    public String cuisine;
    public String type;
    public String feature;
    public String name;
}
