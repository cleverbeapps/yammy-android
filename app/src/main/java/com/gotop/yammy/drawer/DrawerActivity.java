package com.gotop.yammy.drawer;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gotop.yammy.InitialActivity;
import com.gotop.yammy.R;
import com.gotop.yammy.drawer.org_list.OrgListAdapter;
import com.gotop.yammy.drawer.org_list.OrgSearchListVO;
import com.gotop.yammy.drawer.vo.OrgFilterVO;
import com.gotop.yammy.drawer.vo.OrgVO;
import com.gotop.yammy.fragments.FilterFragment;
import com.gotop.yammy.fragments.SearchFragment;
import com.gotop.yammy.login_register.city_picker.City;
import com.gotop.yammy.login_register.city_picker.CityPickerDialog;
import com.gotop.yammy.service.ApiService;
import com.gotop.yammy.service.DemoService;
import com.gotop.yammy.service.FilterService;
import com.gotop.yammy.service.UserService;
import com.gotop.yammy.utils.Helper;
import com.melnykov.fab.FloatingActionButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Objects;

public class DrawerActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    protected static final String TAG = "DrawerActivity";

    private Toolbar _toolbar;
    private View _rightDrawer;
    private DrawerLayout _drawer;
    private RecyclerView.Adapter _menuAdapter;
    private OrgListAdapter _orgListAdapter;
    private Button _btnCity;
    private FrameLayout _mapContainer;
    private GoogleMap _map;
    SwipeRefreshLayout _orgListContainer;

    private FloatingActionButton _fabShowMap, _fabShowUserLocation, _fabShowOrgList;
    Animation show_fab_1;
    Animation hide_fab_1;
    private boolean _isOrgListMode = true;
    private int currentFragment = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_drawer);

        _drawer = (DrawerLayout) findViewById(R.id.mainDrawer);

        initToolbar();
        initGooleApi();
        initFloatButtons();
        initLeftDrawer();
        initOrgList();
        initRightDrawer();

        _btnCity.setVisibility(View.GONE);
        _btnCity.setText("");
        _btnCity.setOnClickListener(null);
        _fabShowMap.setVisibility(View.GONE);
        _fabShowOrgList.setVisibility(View.GONE);
        _fabShowUserLocation.setVisibility(View.GONE);

        Drawable toolbarIcon = _toolbar.getNavigationIcon();
        int iconColor = ContextCompat.getColor(this, R.color.gold);
        toolbarIcon.setColorFilter(iconColor, PorterDuff.Mode.MULTIPLY);

        bus.register(this);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    private void initRightDrawer() {
        ImageButton btnFilter = (ImageButton) findViewById(R.id.toolbar_btn_filter);
        ImageButton btnSearch = (ImageButton) findViewById(R.id.toolbar_btn_search);

        if (btnFilter != null) {
            btnFilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (_drawer.isDrawerOpen(GravityCompat.END)) {
                        _drawer.closeDrawer(GravityCompat.END);
                    } else {
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.rightDrawerContainer, new FilterFragment())
                                .commit();
                        _drawer.openDrawer(GravityCompat.END);
                    }
                }
            });
        }
        if (btnSearch != null) {
            btnSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (_drawer.isDrawerOpen(GravityCompat.END)) {
                        _drawer.closeDrawer(GravityCompat.END);
                    } else {
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.rightDrawerContainer, new SearchFragment())
                                .commit();
                        _drawer.openDrawer(GravityCompat.END);
                    }
                }
            });
        }
    }

    private void initToolbar() {
        _toolbar = (Toolbar) findViewById(R.id.toolbar);
        _toolbar.setTitle("");

        _btnCity = (Button) _toolbar.findViewById(R.id.toolbar_btn_choose_city);
        setSupportActionBar(_toolbar);
    }

    private void initOrgList() {

        _orgListContainer = (SwipeRefreshLayout) findViewById(R.id.activity_drawer_org_list_refresh_layout);
        _orgListContainer.setColorSchemeResources(R.color.gold, R.color.gold, R.color.gold);
        _orgListContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                Helper.setRefreshing(_orgListContainer, true);
                updateList();
            }
        });

        RecyclerView orgList = (RecyclerView) findViewById(R.id.fragment_org_menu_list);
        _orgListAdapter = new OrgListAdapter(this);
        orgList.setAdapter(_orgListAdapter);
        orgList.setLayoutManager(new LinearLayoutManager(this));

        final GestureDetector gestureDetector =
                new GestureDetector(DrawerActivity.this, new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        return true;
                    }
                });

        orgList.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());
                int position = rv.getChildAdapterPosition(child);

                ArrayList<OrgSearchListVO> currentOrgs = _orgListAdapter.getOrgsFiltered();
                if(gestureDetector.onTouchEvent(e) && currentOrgs != null && currentOrgs.size() > 0){

                    OrgSearchListVO orgSearchListVO = currentOrgs.get(position);

                    Helper.showLoadingDialog(DrawerActivity.this, "", "");

                    ApiService.getOrganizationByID(DrawerActivity.this, orgSearchListVO.object_id, new ApiService.ApiResponse() {
                        @Override
                        public void response(Boolean isSuccess, Object payload) {

                            Helper.dismissLoadingDialog();

                            if(isSuccess){
                                OrgVO orgVO = (OrgVO)payload;
                                Intent intent = new Intent(DrawerActivity.this, OrgInfoActivity.class);
                                OrgInfoActivity.setgetCurrentOrgVO(orgVO);
                                DrawerActivity.this.startActivity(intent);
                                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            }
                        }
                    });
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    private void initLeftDrawer() {

        RecyclerView leftMenu = (RecyclerView) findViewById(R.id.leftDrawer);
        leftMenu.setHasFixedSize(true);

        _menuAdapter = new LeftMenuDrawerAdapter(this);
        leftMenu.setAdapter(_menuAdapter);
        leftMenu.setLayoutManager(new LinearLayoutManager(this));

        ActionBarDrawerToggle leftDrawerToggle = new ActionBarDrawerToggle(this, _drawer, _toolbar, R.string.open_drawer_title, R.string.close_drawer_title) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        _drawer.addDrawerListener(leftDrawerToggle);
        leftDrawerToggle.syncState();

        final GestureDetector gestureDetector =
                new GestureDetector(DrawerActivity.this, new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        return true;
                    }
                });

        leftMenu.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                if (child != null && gestureDetector.onTouchEvent(motionEvent)) {

                    _drawer.closeDrawers();

                    int position = recyclerView.getChildLayoutPosition(child);
                    currentFragment = position;
                    switch (position) {
                        case 0:
                            showManageAccount();
                            break;
//                        case LeftMenuDrawerAdapter.FAVORITE_PLACES_FRAGMENT:
//                            openFragment(new FavoritePlacesFragment());
//                            setTitle(getString(R.string.drawer_menu_section_favorite_places));
//                            break;
//                        case LeftMenuDrawerAdapter.FAVORITE_DISHES_FRAGMENT:
//                            openFragment(new FavoriteDishesFragment());
//                            setTitle(getString(R.string.drawer_menu_section_favorite_dishes));
//                            break;
//                        case LeftMenuDrawerAdapter.PROMOTION_FRAGMENT:
//                            openFragment(new PromotionFragment());
//                            setTitle(getString(R.string.drawer_menu_section_promotion));
//                            break;
//                        case LeftMenuDrawerAdapter.INVITE_FRIENDS_FRAGMENT:
//                            openFragment(new InviteFriendsFragment());
//                            setTitle(getString(R.string.drawer_menu_section_invite_friends));
//                            break;
                        case LeftMenuDrawerAdapter.PAYMENT_CARDS_FRAGMENT:
                            CardActivity.isInvokedFromOrder = false;
                            startActivity(new Intent(getApplicationContext(), CardActivity.class));
                            overridePendingTransition(R.anim.right_in, R.anim.left_out);
                            break;
                        case LeftMenuDrawerAdapter.MANAGE_ACCOUNT_FRAGMENT:
                            showManageAccount();
                            break;
                        case LeftMenuDrawerAdapter.SIGNOUT_FRAGMENT:

                            UserService.logout();
                            LoginManager.getInstance().logOut();

                            DemoService.setIsDemoMode(false);
                            Intent initial = new Intent(getApplicationContext(), InitialActivity.class);
                            initial.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(initial);
                            break;

                        default:
                            break;
                    }
                    return true;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    public void showManageAccount(){
        startActivity(new Intent(getApplicationContext(), ManageAccountActivity.class));
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

//    private void openFragment(final Fragment fragment) {
//        getSupportFragmentManager()
//                .beginTransaction()
//                .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left)
//                .add(R.id.activity_drawer_root_layout, fragment)
//                //.replace(R.id.activity_drawer_org_list_refresh_layout, fragment)
//                .commit();
//    }

    private void initFloatButtons() {

        _fabShowMap = (FloatingActionButton) findViewById(R.id.fab_show_map);
        _fabShowUserLocation = (FloatingActionButton) findViewById(R.id.fab_show_user_location);
        _fabShowOrgList = (FloatingActionButton) findViewById(R.id.fab_show_org_list);
        _fabShowUserLocation.setVisibility(View.GONE);
        _fabShowOrgList.setVisibility(View.GONE);
        show_fab_1 = AnimationUtils.loadAnimation(this, R.anim.fab1_show);
        hide_fab_1 = AnimationUtils.loadAnimation(this, R.anim.fab1_hide);
        _fabShowMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMap();
                _isOrgListMode = true;
            }
        });
        _fabShowOrgList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showOrgList();
                _isOrgListMode = false;
            }
        });
        _fabShowUserLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(DrawerActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DrawerActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){

                    _map.setMyLocationEnabled(true);

                    Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    CameraPosition camPos = new CameraPosition.Builder()
                            .target(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()))
                            .zoom(10)
                            .build();
                    CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(camPos);
                    _map.animateCamera(camUpdate);
                }
            }
        });
        _fabShowUserLocation.setVisibility(View.GONE);
    }

    private void showMap() {

        _orgListContainer.setVisibility(View.GONE);
        _mapContainer.setVisibility(View.VISIBLE);
        populateMapWithOrgMarkers();

        _fabShowMap.setVisibility(View.GONE);

        _fabShowOrgList.setVisibility(View.VISIBLE);

        //Floating Action Button 1
        _fabShowUserLocation.setVisibility(View.VISIBLE);
       // _fabShowUserLocation.startAnimation(show_fab_1);
        //_fabShowUserLocation.setEnabled(true);
    }

    private void showOrgList() {

        _orgListContainer.setVisibility(View.VISIBLE);
        _mapContainer.setVisibility(View.GONE);

        _fabShowMap.setVisibility(View.VISIBLE);

        _fabShowOrgList.setVisibility(View.GONE);

        //_fabShowUserLocation.setEnabled(false);
        _fabShowUserLocation.setVisibility(View.GONE);
        //_fabShowUserLocation.startAnimation(hide_fab_1);
    }

    public void updateList() {

        Helper.dismissLoadingDialog();

        _btnCity.setVisibility(View.GONE);
        _btnCity.setText("");
        _btnCity.setOnClickListener(null);
        _fabShowMap.setVisibility(View.GONE);
        _fabShowOrgList.setVisibility(View.GONE);
        _fabShowUserLocation.setVisibility(View.GONE);

        if (!Helper.isConnectionAvailable(this)) {
            Helper.setRefreshing(_orgListContainer, false);
            Helper.showAlert(this, R.string.alert_no_connection_title, R.string.alert_no_connection_message);
            return;
        }

        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates locationSettingsStates = result.getLocationSettingsStates();

                switch (status.getStatusCode()){
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        if (ActivityCompat.checkSelfPermission(DrawerActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DrawerActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                            Helper.showLoadingDialog(DrawerActivity.this, getString(R.string.loading_cities), getString(R.string.please_wait));

                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                            if (mLastLocation != null) {

                                final int userCountryID = UserService.getUserVO().countryID;

                                ApiService.getCitiesByCountryID(String.valueOf(userCountryID), new ApiService.ApiResponse() {
                                    @Override
                                    public void response(Boolean isSuccess, Object payload) {

                                        if (isSuccess) {

                                            ArrayList<City> cities = (ArrayList<City>) payload;
                                            setCurrentCities(cities, mLastLocation);
                                        } else {
                                            Helper.setRefreshing(_orgListContainer, false);
                                            Helper.dismissLoadingDialog();
                                        }
                                    }
                                });

                            } else {
                                Helper.setRefreshing(_orgListContainer, false);
                                Helper.dismissLoadingDialog();
                                Toast.makeText(DrawerActivity.this, R.string.toast_location_error, Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Helper.checkPermissionLocation(DrawerActivity.this);
                        }
                        break;

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    DrawerActivity.this,
                                    Helper.MY_REQUEST_CHECK_LOCATION_SETTINGS);
                        }
                        catch (IntentSender.SendIntentException e) {
                            Helper.setRefreshing(_orgListContainer, false);
                            Helper.dismissLoadingDialog();
                            Toast.makeText(DrawerActivity.this, R.string.toast_location_error, Toast.LENGTH_LONG).show();
                        }
                        break;

                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Helper.setRefreshing(_orgListContainer, false);
                        Helper.dismissLoadingDialog();
                        Helper.showAlert(DrawerActivity.this, R.string.alert_title_location_settings_are_not_satisfied_title, R.string.alert_title_location_settings_are_not_satisfied_message);
                        break;
                }
            }
        });
    }

    private City userCity;

    private void setCurrentCities(final ArrayList<City> cities, final Location location) {

        if (cities != null) {

            userCity = null;

            final int userCityID = UserService.getUserVO().cityID;

            for (int i = 0; i < cities.size(); i++) {
                City city = cities.get(i);
                if (Objects.equals(city.id, String.valueOf(userCityID))) {
                    userCity = city;
                    break;
                }
            }

            if (userCity != null) {

                _btnCity.setVisibility(View.VISIBLE);
                _btnCity.setText(userCity.name);
                _btnCity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        CityPickerDialog cityPicker = new CityPickerDialog(DrawerActivity.this, new CityPickerDialog.CityPickerCallbacks() {
                            @Override
                            public void onCitySelected(City city) {

                                UserService.getUserVO().cityID = Integer.parseInt(city.id);
                                UserService.saveUserData();

                                showOrgList();
                                updateList();
                            }
                        }, cities, _btnCity, userCity);
                        cityPicker.show();
                    }
                });

                getOrgSearch(location, userCity);
            } else {
                Helper.setRefreshing(_orgListContainer, false);
                Helper.dismissLoadingDialog();
                Toast.makeText(this, R.string.toast_no_user_city, Toast.LENGTH_LONG).show();
            }
        } else {
            Helper.setRefreshing(_orgListContainer, false);
            Helper.dismissLoadingDialog();
            Toast.makeText(this, R.string.toast_no_cities, Toast.LENGTH_LONG).show();
        }
    }

    private void getOrgSearch(Location location, City city) {

        OrgFilterVO orgFilterVO = new OrgFilterVO();
        orgFilterVO.location = location;
        orgFilterVO.cityID = city.id;
//        orgFilterVO.cuisine = ;
//        orgFilterVO.type = ;
//        orgFilterVO.feature = ;
//        orgFilterVO.name = ;

        ApiService.getOrgSearch(orgFilterVO, new ApiService.ApiResponse() {
            @Override
            public void response(Boolean isSuccess, Object payload) {

                Helper.setRefreshing(_orgListContainer, false);
                Helper.dismissLoadingDialog();

                showOrgList();

                if (isSuccess) {
                    _orgListAdapter.setOrgList((ArrayList<OrgSearchListVO>) payload);
                } else {
                    _orgListAdapter.setOrgList(null);

                    Helper.setRefreshing(_orgListContainer, false);
                    Toast.makeText(DrawerActivity.this, R.string.toast_empty_org_data, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case Helper.MY_PERMISSIONS_REQUEST_ACCESS_COURSE_LOCATION:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    updateList();
                } else {
                    //code for deny
                    Helper.setRefreshing(_orgListContainer, false);
                    Helper.dismissLoadingDialog();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case Helper.MY_REQUEST_CHECK_LOCATION_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        updateList();
                        break;
                    case Activity.RESULT_CANCELED:
                        updateList();
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    private EventBus bus = EventBus.getDefault();

    @Subscribe
    public void onEvent(FilterService.FilterUpdate event){
        if(_orgListAdapter != null){
            _orgListAdapter.filterDataProvider();
        }
    }

    private void onUserUpdate(){
        if(_menuAdapter != null) {
            _menuAdapter.notifyDataSetChanged();
        }

        String userCityID = String.valueOf(UserService.getUserVO().cityID);
        if(userCity != null && !Objects.equals(userCity.id, userCityID)){
            updateList();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!mGoogleApiClient.isConnected()){
            mGoogleApiClient.connect();
        }
        onUserUpdate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            //mGoogleApiClient.disconnect();
        }
    }

    protected GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;

    protected synchronized void initGooleApi() {

        _mapContainer = (FrameLayout) findViewById(R.id.activity_drawer_map_layout);
        _mapContainer.setVisibility(View.GONE);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {

        updateList();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
        Toast.makeText(DrawerActivity.this, R.string.toast_google_api_services_connection_failed, Toast.LENGTH_LONG).show();
    }
    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        _map = map;
        populateMapWithOrgMarkers();
    }

    private void populateMapWithOrgMarkers(){

        if(_map == null){
            MapFragment mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(R.id.activity_drawer_map_fragment);
            mapFragment.getMapAsync(this);

            return;
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){

            if(_map != null){
                _map.clear();
                _map.setMyLocationEnabled(true);

                // Setting a custom info window adapter for the google map
                _map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    // Use default InfoWindow frame
                    @Override
                    public View getInfoContents(Marker args) {
                        // Getting view from the layout file info_window_layout
                        final View v = getLayoutInflater().inflate(R.layout.map_marker_info_window, null);

                        ArrayList<OrgSearchListVO> currentOrgs = _orgListAdapter.getOrgsFiltered();
                        if(currentOrgs != null && currentOrgs.size() > 0){
                            for (int i = 0; i < currentOrgs.size(); i++){
                                OrgSearchListVO org = currentOrgs.get(i);
                                if(Objects.equals(org.name, args.getTitle())){

                                    TextView markerLabel = (TextView)v.findViewById(R.id.map_marker_info_window_name);
                                    TextView anotherLabel = (TextView)v.findViewById(R.id.map_marker_info_window_adress);

                                    markerLabel.setText(org.name);
                                    anotherLabel.setText(org.address);
                                    break;
                                }
                            }
                        }

                        return v;
                    }

                    // Defines the contents of the InfoWindow
                    @Override
                    public View getInfoWindow(Marker args) {

                        return null;

                    }
                });
                _map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        ArrayList<OrgSearchListVO> currentOrgs = _orgListAdapter.getOrgsFiltered();
                        if(currentOrgs != null && currentOrgs.size() > 0){
                            for (int i = 0; i < currentOrgs.size(); i++){
                                OrgSearchListVO org = currentOrgs.get(i);
                                if(Objects.equals(org.name, marker.getTitle())) {

                                    Helper.showLoadingDialog(DrawerActivity.this, "", "");

                                    ApiService.getOrganizationByID(DrawerActivity.this, org.object_id, new ApiService.ApiResponse() {
                                        @Override
                                        public void response(Boolean isSuccess, Object payload) {

                                            Helper.dismissLoadingDialog();

                                            if(isSuccess){
                                                OrgVO orgVO = (OrgVO)payload;
                                                Intent intent = new Intent(DrawerActivity.this, OrgInfoActivity.class);
                                                OrgInfoActivity.setgetCurrentOrgVO(orgVO);
                                                DrawerActivity.this.startActivity(intent);
                                                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                                            }
                                        }
                                    });
                                    break;
                                }
                            }
                        }
                    }
                });

                ArrayList<OrgSearchListVO> currentOrgs =  _orgListAdapter.getOrgsFiltered();

                if(currentOrgs != null && currentOrgs.size() > 0){

                    LatLngBounds.Builder builder = new LatLngBounds.Builder();

                    for (int i = 0; i < currentOrgs.size(); i++) {

                        OrgSearchListVO orgSearchListVO = currentOrgs.get(i);
                        LatLng latLng = new LatLng(orgSearchListVO.latintude, orgSearchListVO.longitude);

                        Marker marker = _map.addMarker(new MarkerOptions()
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_location_icon))
                                .title(orgSearchListVO.name)
                                .snippet(orgSearchListVO.address)
                                .position(latLng));

                        builder.include(marker.getPosition());
                    }

                    LatLngBounds bounds = builder.build();

                    int width = getResources().getDisplayMetrics().widthPixels;
                    int height = getResources().getDisplayMetrics().heightPixels;
                    int padding = (int) (width * 0.10); // offset from edges of the map 12% of screen

                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

                    _map.animateCamera(cu);
                }
            }
        }
    }
}
