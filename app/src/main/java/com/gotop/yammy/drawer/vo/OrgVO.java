package com.gotop.yammy.drawer.vo;

import android.content.Context;
import android.util.Log;

import com.gotop.yammy.R;
import com.gotop.yammy.service.DemoService;
import com.gotop.yammy.utils.Helper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OrgVO implements Serializable{

    public String address = "";
    public String alias = "";
    public String city = "";
    public String country = "";
    public Integer createdAt = 0;
    public ArrayList<CuisineVO> cuisines = new ArrayList<CuisineVO>();
    public String current_chief = "";
    public String description = "";
    public String dinner_days = "";
    public String dinner_times = "";
    public String dress_code = "";
    public Integer header_image = 0;
    public ArrayList<Integer> images = new ArrayList<Integer>();
    public Double latitude = 0.0;
    public Double longitude = 0.0;
    public String lunch_days = "";
    public String lunch_times = "";
    public String name = "";
    public Integer object_id = 0;
    public String parking = "";
    public String parking_details = "";
    public String price = "";
    public Integer rating = 0;
    public String site_link = "";
    public String social_link_fb = "";
    public String social_link_vk = "";
    public Integer updatedAt = 0;
    public Boolean isFavorite = false;

    static public OrgVO fromJSON(Context context, JSONObject json, Boolean isDemo){

        OrgVO orgVO = new OrgVO();

        try{
            orgVO.address       = json.getString("address");
            orgVO.alias         = json.getString("alias");
            orgVO.city          = json.getString("city");
            orgVO.country       = json.getString("country");
            orgVO.createdAt     = json.getInt("createdAt");
            orgVO.current_chief = json.getString("current_chief");
            orgVO.description   = json.getString("description");
            orgVO.dinner_days   = json.getString("dinner_days");
            orgVO.dinner_times  = json.getString("dinner_times");
            orgVO.dress_code    = json.getString("dress_code");
            orgVO.latitude      = json.getDouble("latitude");
            orgVO.longitude     = json.getDouble("longitude");
            orgVO.lunch_days    = json.getString("lunch_days");
            orgVO.lunch_times   = json.getString("lunch_times");
            orgVO.name          = json.getString("name");
            orgVO.object_id     = json.getInt("object_id");
            orgVO.parking       = json.getString("parking");
            orgVO.parking_details = json.getString("parking_details");
            orgVO.price         = json.getString("price");
            orgVO.rating        = json.getInt("rating");
            orgVO.site_link     = json.getString("site_link");
            orgVO.social_link_fb = json.getString("social_link_fb");
            orgVO.social_link_vk = json.getString("social_link_vk");
            orgVO.updatedAt     = json.getInt("updatedAt");
            orgVO.isFavorite = json.getBoolean("isFavorite");

            if(isDemo){
                JSONArray demoImageNames = json.getJSONArray("demoImages");
                for (int index = 0; index < demoImageNames.length(); index++) {
                    String imageName = demoImageNames.getString(index);
                    int imageID = Helper.getDrawableIDByName(context, imageName);
                    orgVO.images.add(imageID);
                }
                orgVO.header_image  = orgVO.images.get(0);
            }
            else{
                orgVO.header_image  = json.getInt("header_image");
                orgVO.images.add(orgVO.header_image);
                JSONArray images = json.getJSONArray("images");
                for (int index = 0; index < images.length(); index++) {
                    int image = images.getInt(index);
                    orgVO.images.add(image);
                }
            }

            JSONArray cousines = json.getJSONArray("cuisine");
            if(cousines != null){
                for (int index = 0; index < cousines.length(); index++) {
                    JSONObject item = cousines.getJSONObject(index);

                    int id = item.getInt("id");
                    String name = item.getString("name");
                    CuisineVO cuisineVO = new CuisineVO();
                    cuisineVO.id = id;
                    cuisineVO.name = name;
                    orgVO.cuisines.add(cuisineVO);
                }
            }
        }
        catch (Exception e) {
            Log.e("OrgVO parse", "exception");
        }

        return orgVO;
    }
}
