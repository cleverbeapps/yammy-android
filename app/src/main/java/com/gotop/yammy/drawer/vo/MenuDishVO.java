package com.gotop.yammy.drawer.vo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MenuDishVO {

    public int weight       = 0;
    public int id           = 0;
    public int category_id  = 0;
    public Double price     = 0.0;
    public String dishDescription = "";
    public String bulk      = "";
    public String name      = "";
    public String video     = "";
    public int calorific_value = 0;
    public ArrayList<Integer> product_images_list = new ArrayList<Integer>();

    static public MenuDishVO fromJSON(JSONObject json){

        MenuDishVO menuDishVO = new MenuDishVO();
        try {
            menuDishVO.id = json.getInt("id");
            menuDishVO.dishDescription = json.getString("description");
            menuDishVO.bulk = json.getString("bulk");
            menuDishVO.name = json.getString("name");
            menuDishVO.price = json.getDouble("price");
            menuDishVO.category_id = json.getInt("category_id");
            menuDishVO.weight = json.getInt("weight");
            menuDishVO.calorific_value = json.getInt("calorific_value");
            menuDishVO.video = json.getString("video");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return menuDishVO;
    }

}
