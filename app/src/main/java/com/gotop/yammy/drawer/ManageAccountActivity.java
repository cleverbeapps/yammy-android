package com.gotop.yammy.drawer;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gotop.yammy.R;
import com.gotop.yammy.login_register.UserUpdateVO;
import com.gotop.yammy.login_register.choose_photo_diallog.ChoosePhotoDialog;
import com.gotop.yammy.login_register.city_picker.City;
import com.gotop.yammy.login_register.city_picker.CityPickerDialog;
import com.gotop.yammy.login_register.country_picker.Country;
import com.gotop.yammy.login_register.country_picker.CountryPickerDialog;
import com.gotop.yammy.service.ApiService;
import com.gotop.yammy.service.UserService;
import com.gotop.yammy.utils.Helper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class ManageAccountActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView toolbarTitle;
    private ImageButton backButton;
    private EditText inputName, inputSurname;
    private TextInputLayout inputLayoutName, inputLayoutSurname;
    TextInputEditText tfPhoneNumber;
    TextInputLayout phoneNumberInput;
    private Button btnChooseCity;
    private Button btnSave;
    Button btnAddphoto;
    CircleImageView circleProfileImageView;
    ImageView profileDefaultImage;

    Boolean isPhotoChoosed = false;

    LinearLayout layoutCountryCodeAndPhone;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_manage_account);

        toolbar = (Toolbar)findViewById(R.id.act_manage_account_toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_back_title);
        toolbarTitle.setText("Manage Account");
        backButton = (ImageButton) toolbar.findViewById(R.id.toolbar_back_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        final Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(Color.LTGRAY);
        backButton.setColorFilter(ContextCompat.getColor(ManageAccountActivity.this, R.color.gold));
        toolbarTitle.setHintTextColor(ContextCompat.getColor(ManageAccountActivity.this, R.color.gold));
        toolbar.getBackground().setAlpha(255);

        btnSave = (Button)findViewById(R.id.act_manage_account_save_button);

        inputName = (EditText)findViewById(R.id.act_manage_account_text_input_name);
        inputSurname = (EditText)findViewById(R.id.act_manage_account_text_input_surname);

        inputLayoutName = (TextInputLayout) findViewById(R.id.act_manage_account_layout_name);
        inputLayoutSurname = (TextInputLayout) findViewById(R.id.act_manage_account_layout_surname);

        tfCity = (TextInputEditText) findViewById(R.id.act_manage_account_text_input_city);

        tfPhoneNumber = (TextInputEditText) findViewById(R.id.act_manage_account_text_input_phone_number);
        phoneNumberInput = (TextInputLayout) findViewById(R.id.act_manage_account_layout_phone_number);

        inputName.addTextChangedListener(new MyTextWatcher(inputName));
        inputSurname.addTextChangedListener(new MyTextWatcher(inputSurname));
        tfPhoneNumber.addTextChangedListener(new MyTextWatcher(tfPhoneNumber));

        layoutCountryCodeAndPhone = (LinearLayout) findViewById(R.id.act_manage_account_country_code_and_phone_layout);
        imageViewFlag = (ImageView) findViewById(R.id.act_manage_account_flag_view);
        textViewCountryCode = (TextView)findViewById(R.id.act_manage_account_country_code_text);
        int userCountryID = UserService.getUserVO().countryID;
        layoutCountryCodeAndPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CountryPickerDialog countryPicker = new CountryPickerDialog(ManageAccountActivity.this,
                        new CountryPickerDialog.CountryPickerCallbacks() {
                            @Override
                            public void onCountrySelected(Country country) {
                                setCurrentCountry(country);
                            }
                        },
                        currentCountry.isoCode, true);

                countryPicker.show();
            }
        });
        btnChooseCity = (Button)findViewById(R.id.act_manage_account_btn_choose_city);
        btnChooseCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentCountryCities != null) {
                    CityPickerDialog cityPicker = new CityPickerDialog(ManageAccountActivity.this, new CityPickerDialog.CityPickerCallbacks() {
                        @Override
                        public void onCitySelected(City city) {
                            setCurrentCity(city);
                        }
                    }, currentCountryCities);
                    cityPicker.show();
                }
            }
        });
        btnAddphoto = (Button) findViewById(R.id.act_manage_account_btn_add_photo);
        btnAddphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        circleProfileImageView = (CircleImageView) findViewById(R.id.act_manage_account_profile_circle_image);
        circleProfileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        profileDefaultImage = (ImageView) findViewById(R.id.act_manage_account_profile_default_image);
        profileDefaultImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        profileDefaultImage.setVisibility(View.VISIBLE);
        profileDefaultImage.setImageResource(R.drawable.default_profile_image);

        byte[] avatarBytes = UserService.getUserVO().avatarBytes;
        if(avatarBytes != null && avatarBytes.length > 0){
            Bitmap userProfileAvatarBitmap = Helper.getImageFromBytes(avatarBytes);
            circleProfileImageView.setImageBitmap(userProfileAvatarBitmap);
            circleProfileImageView.setVisibility(View.VISIBLE);
            profileDefaultImage.setVisibility(View.INVISIBLE);
        }
        else{
            profileDefaultImage.setVisibility(View.VISIBLE);
            circleProfileImageView.setVisibility(View.INVISIBLE);
        }

        inputName.setText(UserService.getUserVO().name);
        inputSurname.setText(UserService.getUserVO().surname);
        tfPhoneNumber.setText(UserService.getUserVO().phone);


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUser();
            }
        });

        setCurrentCountry(Country.getCountryById(String.valueOf(userCountryID)));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

    Country currentCountry;
    ImageView imageViewFlag;
    TextView textViewCountryCode;
    ArrayList<City> currentCountryCities;
    City currentCity;
    TextInputEditText tfCity;


    private void setCurrentCountry(Country country){

        this.currentCountry = country;
        imageViewFlag.setImageResource(country.getResourceID());
        textViewCountryCode.setText("+" + country.dialingCode);

        if(!Helper.isConnectionAvailable(ManageAccountActivity.this)){
            Helper.showAlert(ManageAccountActivity.this, R.string.alert_no_connection_title, R.string.alert_no_connection_message);
        }
        else{
            Helper.showLoadingDialog(this, getString(R.string.loading_cities), getString(R.string.please_wait));

            ApiService.getCitiesByCountryID(country.id, new ApiService.ApiResponse() {
                @Override
                public void response(Boolean isSuccess, Object payload) {

                    Helper.dismissLoadingDialog();

                    ArrayList<City> cities = (ArrayList<City>) payload;

                    currentCountryCities = cities;

                    City userCity = null;
                    for(int i = 0; i<cities.size(); i++){
                        City city = cities.get(i);
                        if(Objects.equals(city.id, String.valueOf(UserService.getUserVO().cityID))){
                            userCity = city;
                        }
                    }
                    if(userCity == null){
                        userCity = cities.get(0);
                    }
                    setCurrentCity(userCity);
                }
            });
        }
    }
    private void setCurrentCity(City city){
        currentCity = city;
        tfCity.setText(city.name);
    }

    private void updateUser() {
        if (!validateName()) {
            return;
        }

        if (!validateSurname()) {
            return;
        }

        if(!validatePhoneNumber()){
            return;
        }

        if(currentCountry == null){
            Helper.showAlert(this, R.string.registration_no_country_title, R.string.registration_no_country_message);
            return;
        }
        if(currentCity == null){
            Helper.showAlert(this, R.string.registration_no_city_title, R.string.registration_no_city_message);
            return;
        }

        if(!Helper.isConnectionAvailable(ManageAccountActivity.this)){
            Helper.showAlert(ManageAccountActivity.this, R.string.alert_no_connection_title, R.string.alert_no_connection_message);
        }
        else{
            Helper.showLoadingDialog(this, getString(R.string.updating_user), getString(R.string.please_wait));

            UserUpdateVO userUpdateVO = new UserUpdateVO();

            if(isPhotoChoosed){
                Bitmap profileBitmap = Helper.getBitmapFromImageView(circleProfileImageView);
                userUpdateVO.profileImageBitmap = profileBitmap;
            }
            userUpdateVO.name = inputName.getText().toString();
            userUpdateVO.surname = inputSurname.getText().toString();
            userUpdateVO.cityId = currentCity.id;
            userUpdateVO.countryId = currentCountry.id;
            userUpdateVO.phone = tfPhoneNumber.getText().toString();

            ApiService.userUpdate(this, userUpdateVO  , new ApiService.ApiResponse() {
                @Override
                public void response(Boolean isSuccess, Object payload) {

                    Helper.dismissLoadingDialog();

                    Toast.makeText(getApplicationContext(), R.string.msg_changes_saved, Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
            });
        }
    }

    private boolean validateName() {
        if (inputName.getText().toString().trim().isEmpty()) {
            inputLayoutName.setError(getString(R.string.err_msg_name));
            requestFocus(inputName);
            btnSave.setEnabled(false);
            return false;
        } else {
            inputLayoutName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateSurname() {
        if (inputSurname.getText().toString().trim().isEmpty()) {
            inputLayoutSurname.setError(getString(R.string.err_msg_surname));
            requestFocus(inputSurname);
            btnSave.setEnabled(false);
            return false;
        } else {
            inputLayoutSurname.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePhoneNumber() {
        if (!Helper.isValidPhone(tfPhoneNumber.getText().toString())) {
            phoneNumberInput.setErrorEnabled(true);
            phoneNumberInput.setError(getString(R.string.wrong_phone_number));
            requestFocus(phoneNumberInput);
            btnSave.setEnabled(false);
            return false;
        } else {
            phoneNumberInput.setError(null);
            phoneNumberInput.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {

            Boolean isValid = false;

            switch (view.getId()) {
                case R.id.act_manage_account_text_input_name:
                    isValid = validateName();
                    break;
                case R.id.act_manage_account_text_input_surname:
                    isValid = validateSurname();
                    break;
                case R.id.act_manage_account_text_input_phone_number:
                    isValid = validatePhoneNumber();
                    break;
            }

            btnSave.setEnabled(isValid);
        }
    }



    private ChoosePhotoDialog.ChoosePhotoDialogItem choosePhotoDialogItem;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case Helper.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(choosePhotoDialogItem.id == ChoosePhotoDialog.TAKE_PHOTO)
                        cameraIntent();
                    else if(choosePhotoDialogItem.id == ChoosePhotoDialog.CHOOSE_FROM_LIBRARY)
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void selectImage() {

        ChoosePhotoDialog choosePhotoPicker = new ChoosePhotoDialog(this, new ChoosePhotoDialog.ChoosePhotoDialogCallback() {
            @Override
            public void onSelect(ChoosePhotoDialog.ChoosePhotoDialogItem item) {

                choosePhotoDialogItem = item;
                boolean result = Helper.checkPermissionReadExternalStorage(ManageAccountActivity.this);

                switch(item.id){
                    case ChoosePhotoDialog.TAKE_PHOTO:
                        if(result)
                            cameraIntent();
                        break;
                    case ChoosePhotoDialog.CHOOSE_FROM_LIBRARY:
                        if(result)
                            galleryIntent();
                        break;
                }
            }
        });

        choosePhotoPicker.show();
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        circleProfileImageView.setImageBitmap(thumbnail);

        isPhotoChoosed = true;
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(this.getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        profileDefaultImage.setVisibility(View.INVISIBLE);
        circleProfileImageView.setVisibility(View.VISIBLE);
        circleProfileImageView.setImageBitmap(bm);

        isPhotoChoosed = true;
    }
}
