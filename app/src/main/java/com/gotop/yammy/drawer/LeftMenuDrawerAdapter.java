package com.gotop.yammy.drawer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.service.UserService;
import com.gotop.yammy.utils.Helper;
import com.gotop.yammy.utils.IconsTextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class LeftMenuDrawerAdapter extends RecyclerView.Adapter<LeftMenuDrawerAdapter.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private String mNavTitles[];
    private String mIcons[];

    private DrawerActivity activity;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.left_drawer_menu_row,parent,false);
            ViewHolder vhItem = new ViewHolder(v,viewType, activity);
            return vhItem;
        }
        else if(viewType == TYPE_HEADER) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header,parent,false);
            ViewHolder vhHeader = new ViewHolder(v,viewType, activity);

            return vhHeader;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if(holder.Holderid == 1) {
            holder.textView.setText(mNavTitles[position - 1]);
            holder.imageView.setText(mIcons[position -1]);
        }
        else{
            byte[] avatarBytes = UserService.getUserVO().avatarBytes;
            if(avatarBytes != null && avatarBytes.length > 0){
                Bitmap userProfileAvatarBitmap = Helper.getImageFromBytes(avatarBytes);
                holder.circleImageView.setImageBitmap(userProfileAvatarBitmap);
            }
            else{
                holder.circleImageView.setImageResource(R.drawable.default_profile_image);
            }

            String userName = UserService.getUserVO().name + " " + UserService.getUserVO().surname;
            String userEmail = UserService.getUserVO().email;
            holder.Name.setText(userName);
            holder.email.setText(userEmail);
        }
    }

    @Override
    public int getItemCount() {
        return mNavTitles.length + 1;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        int Holderid;

        TextView textView;
        IconsTextView imageView;
        CircleImageView circleImageView;
        TextView Name;
        TextView email;

        DrawerActivity drawerActivity;

        public ViewHolder(View itemView, int ViewType, final DrawerActivity drawerActivity) {
            super(itemView);
            this.drawerActivity = drawerActivity;

            if(ViewType == TYPE_ITEM) {
                textView = (TextView) itemView.findViewById(R.id.rowText);
                imageView = (IconsTextView) itemView.findViewById(R.id.rowIcon);
                Holderid = 1;
                itemView.setBackgroundColor(Color.WHITE);
            }
            else{
                Name = (TextView) itemView.findViewById(R.id.name);
                email = (TextView) itemView.findViewById(R.id.email);
                circleImageView = (CircleImageView) itemView.findViewById(R.id.circleView);
                Holderid = 0;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public final static int FAVORITE_PLACES_FRAGMENT = 1;
    public final static int FAVORITE_DISHES_FRAGMENT = 2;
    public final static int PROMOTION_FRAGMENT = 3;
    public final static int INVITE_FRIENDS_FRAGMENT = 4;
    public final static int PAYMENT_CARDS_FRAGMENT = 5;
    public final static int MANAGE_ACCOUNT_FRAGMENT = 6;
    public final static int SIGNOUT_FRAGMENT = 7;

    public LeftMenuDrawerAdapter(DrawerActivity activity){

        ArrayList<LeftMenuDrawerItem> dataList = new ArrayList<LeftMenuDrawerItem>();
        dataList.add(new LeftMenuDrawerAdapter.LeftMenuDrawerItem(activity.getString(R.string.drawer_menu_section_favorite_places), String.valueOf((char) 0xe912)));
        dataList.add(new LeftMenuDrawerAdapter.LeftMenuDrawerItem(activity.getString(R.string.drawer_menu_section_favorite_dishes), String.valueOf((char) 0xe90d)));
        dataList.add(new LeftMenuDrawerAdapter.LeftMenuDrawerItem(activity.getString(R.string.drawer_menu_section_promotion),       String.valueOf((char) 0xe916)));
        dataList.add(new LeftMenuDrawerAdapter.LeftMenuDrawerItem(activity.getString(R.string.drawer_menu_section_invite_friends), String.valueOf((char) 0xe918)));
        dataList.add(new LeftMenuDrawerAdapter.LeftMenuDrawerItem(activity.getString(R.string.drawer_menu_section_payment_cards), String.valueOf((char) 0xe91c)));
        dataList.add(new LeftMenuDrawerAdapter.LeftMenuDrawerItem(activity.getString(R.string.drawer_menu_section_manage_account), String.valueOf((char) 0xe911)));
        dataList.add(new LeftMenuDrawerAdapter.LeftMenuDrawerItem(activity.getString(R.string.drawer_menu_section_sign_out), String.valueOf((char) 0xe907)));

        mNavTitles = new String[dataList.size()];
        mIcons = new String[dataList.size()];

        for (int i = 0; i < dataList.size(); i++){
            mNavTitles[i] = dataList.get(i).name;
            mIcons[i] = dataList.get(i).icon;
        }
        this.activity = activity;
    }

    public static class LeftMenuDrawerItem {

        public String name;
        public String icon;

        public LeftMenuDrawerItem(String name, String icon){
            this.name = name;
            this.icon = icon;
        }
    }
}
