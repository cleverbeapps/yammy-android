package com.gotop.yammy.drawer.org_list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.drawer.vo.CuisineVO;
import com.gotop.yammy.service.ApiService;
import com.gotop.yammy.service.FilterService;
import com.greenfrvr.hashtagview.HashtagView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class OrgListAdapter extends RecyclerView.Adapter<OrgListAdapter.OrgListCell> {

    private Context mContext;
    ArrayList<OrgSearchListVO> dataProvider = new ArrayList<OrgSearchListVO>();
    ArrayList<OrgSearchListVO> filteredDataProvider = new ArrayList<OrgSearchListVO>();

    public OrgListAdapter(Context context){

        mContext = context;
    }

    public void setOrgList(ArrayList<OrgSearchListVO> orgSearchList){

        dataProvider.clear();

        if(orgSearchList != null){
            this.dataProvider.addAll(orgSearchList);
        }

        filterDataProvider();
    }

    public void filterDataProvider(){

        filteredDataProvider.clear();
        
        for (int i = 0; i < dataProvider.size(); i++) {

            OrgSearchListVO orgSearchListVO = dataProvider.get(i);

            // filter by cuisines
            ArrayList<CuisineVO> cuisineFilter = FilterService.getCuisineFilter();
            Boolean cuisinesDone = false;
            if(cuisineFilter.size() > 0){
                for (int i2 = 0; i2 < cuisineFilter.size() && !cuisinesDone; i2++) {
                    CuisineVO filterCuisine = cuisineFilter.get(i2);
                    for (int i3 = 0; i3 < orgSearchListVO.cuisines.size(); i3++) {
                        CuisineVO orgCuisine = orgSearchListVO.cuisines.get(i3);
                        if(filterCuisine.id == orgCuisine.id){
                            filteredDataProvider.add(orgSearchListVO);
                            cuisinesDone = true;
                            break;
                        }
                    }
                }
            }
            else {
                filteredDataProvider.add(orgSearchListVO);
            }


        }

        // sort
        int filterSorting = FilterService.get_currentSort();
        if(filterSorting == FilterService.SORTED_BY_NAME){
            Collections.sort(filteredDataProvider, new Comparator<OrgSearchListVO>() {
                @Override
                public int compare(OrgSearchListVO org1, OrgSearchListVO org2) {
                    return org1.name.toLowerCase().compareTo(org2.name.toLowerCase());
                }
            });
        } else if(filterSorting == FilterService.SORTED_BY_PRICE){
                Collections.sort(filteredDataProvider, new Comparator<OrgSearchListVO>() {
                    @Override
                    public int compare(OrgSearchListVO org1, OrgSearchListVO org2) {
                        String price1 = String.valueOf(org1.price);
                        String price2 = String.valueOf(org2.price);
                        return price1.compareTo(price2);
                    }
                });
        } else if(filterSorting == FilterService.SORTED_BY_LOCATION){
            Collections.sort(filteredDataProvider, new Comparator<OrgSearchListVO>() {
                @Override
                public int compare(OrgSearchListVO org1, OrgSearchListVO org2) {
                    String distance1 = String.valueOf(org1.distance);
                    String distance2 = String.valueOf(org2.distance);
                    return distance1.compareTo(distance2);
                }
            });
        }

        notifyDataSetChanged();
    }

    public ArrayList<OrgSearchListVO> getOrgsFiltered(){
        return filteredDataProvider;
    }

    @Override
    public OrgListCell onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.org_list_row,parent,false);
        OrgListCell vhItem = new OrgListCell(v,viewType);
        return vhItem;
    }

    @Override
    public void onBindViewHolder(final OrgListCell holder, int position) {

        OrgSearchListVO currentOrg = filteredDataProvider.get(position);

        holder.rate.setText(String.valueOf(currentOrg.rating));
        holder.distance.setText(String.format("%.2f", currentOrg.distance / 1000) + " km");
        holder.name.setText(String.valueOf(currentOrg.name));
        holder.adress.setText(String.valueOf(currentOrg.address));

        holder.tagView.removeAllViews();
        ArrayList<String> cuisines = new ArrayList<>();
        for (int i = 0; i < currentOrg.cuisines.size(); i++) {
            CuisineVO cuisine = currentOrg.cuisines.get(i);
            cuisines.add(cuisine.name);
        }
        holder.tagView.setData(cuisines);

        holder.activityIndicator.setVisibility(View.VISIBLE);

        String imageURL = ApiService.getOrgImageURL(currentOrg.header_image);

        Picasso.with(mContext)
                .load(imageURL)
                .placeholder(R.drawable.org_cell_bg)
                .fit()
                .tag(mContext)
                .centerCrop()
                .into(holder.imgBg, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        holder.activityIndicator.setVisibility(View.INVISIBLE);
                    }
                    @Override
                    public void onError() {

                    }
                });
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return filteredDataProvider.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public static class OrgListCell extends RecyclerView.ViewHolder{

        ImageView imgBg;
        ProgressBar activityIndicator;
        TextView rate;
        TextView distance;
        TextView name;
        TextView adress;
        LinearLayout distanceLayout;
        LinearLayout rateLayout;
        HashtagView tagView;

        public OrgListCell(View itemView,int ViewType) {
            super(itemView);

            imgBg = (ImageView) itemView.findViewById(R.id.org_list_row_bg);
            activityIndicator = (ProgressBar) itemView.findViewById(R.id.org_list_row_progress_bar);
            rate = (TextView) itemView.findViewById(R.id.ogr_list_row_rate);
            distance = (TextView) itemView.findViewById(R.id.ogr_list_row_distance);
            name = (TextView) itemView.findViewById(R.id.ogr_list_row_name);
            adress = (TextView) itemView.findViewById(R.id.ogr_list_row_adress);
            distanceLayout = (LinearLayout) itemView.findViewById(R.id.org_list_row_distance_layout);
            rateLayout = (LinearLayout) itemView.findViewById(R.id.org_list_row_rate_layout);

            distanceLayout.getBackground().setAlpha(100); // 20%  255 = 100%
            rateLayout.getBackground().setAlpha(100); // 20%

            tagView = (HashtagView) itemView.findViewById(R.id.org_list_row_tag_view);
        }
    }
}
