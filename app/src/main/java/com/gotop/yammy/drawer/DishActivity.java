package com.gotop.yammy.drawer;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.drawer.vo.DishDescriptionVO;
import com.gotop.yammy.drawer.vo.MenuDishVO;
import com.gotop.yammy.photo_browser.PhotoGridViewActivity;
import com.gotop.yammy.service.ApiService;
import com.gotop.yammy.service.DemoService;
import com.gotop.yammy.service.OrderService;
import com.gotop.yammy.utils.Helper;
import com.gotop.yammy.utils.IconsTextView;
import com.gotop.yammy.utils.ParallaxScrollView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DishActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageButton backButton;
    private ImageButton addDishButton;
    private HeaderAdapter headerAdapter;
    private ViewPager headerPager;
    private ParallaxScrollView scrollView;
    private RelativeLayout ingridientsLayout;
    private RelativeLayout priceNameLayout;
    private RelativeLayout commentLayout;
    private RelativeLayout dishCountLayout;
    private RelativeLayout addDishLayout;
    private LinearLayout descriptionLayout;
    private Button btnOrder;
    private Button btnCallWaiter;
    private TextView priceText;
    private TextView nameText;
    private TextView descriptionText;
    private TextView descriptionTitleText;
    private TextView toolbarTitle;
    private TextView dishCountText;

    private IconsTextView btnLike;
    private IconsTextView btnShare;
    private IconsTextView ingridientIcon;
    private IconsTextView commentIcon;
    private IconsTextView ingridientArrow;
    private IconsTextView commentArrow;

    private Boolean isToolbarVisible = false;

    static public DishDescriptionVO currentDishDescription;
    static public MenuDishVO currentMenuDishVO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish);

        final int statusBarHeight = Helper.getStatusBarHeight(this);

        toolbar = (Toolbar)findViewById(R.id.dish_toolbar);
        backButton = (ImageButton) toolbar.findViewById(R.id.toolbar_back_btn);

        // Set the padding to match the Status Bar height
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) toolbar.getLayoutParams();
        lp.setMargins(0, statusBarHeight, 0, 0);

        toolbarTitle = (TextView)toolbar.findViewById(R.id.toolbar_back_title);
        toolbarTitle.setText(currentDishDescription.name);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnLike = (IconsTextView)toolbar.findViewById(R.id.toolbar_org_like_btn);
        btnShare = (IconsTextView)toolbar.findViewById(R.id.toolbar_org_share_btn);
        btnShare.setText(String.valueOf((char) 0xe918));

        ingridientsLayout = (RelativeLayout)findViewById(R.id.act_dish_ingridients_layout);
        commentLayout = (RelativeLayout)findViewById(R.id.act_dish_comment_layout);
        dishCountLayout = (RelativeLayout)findViewById(R.id.act_dish_order_count_layout);
        addDishLayout = (RelativeLayout)findViewById(R.id.act_dish_add_dish_layout);
        priceNameLayout = (RelativeLayout)findViewById(R.id.act_dish_price_name_layout);
        btnOrder = (Button) findViewById(R.id.act_dish_btn_order);
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DishActivity.this, OrderActivity.class);
                startActivity(intent);
            }
        });

        btnCallWaiter = (Button) findViewById(R.id.act_dish_btn_call_waiter);
        priceText = (TextView) findViewById(R.id.act_dish_price);
        nameText = (TextView) findViewById(R.id.act_dish_name);
        dishCountText = (TextView) findViewById(R.id.act_dish_order_count_text);
        descriptionLayout = (LinearLayout) findViewById(R.id.act_dish_description_layout);
        descriptionText = (TextView) findViewById(R.id.act_dish_description);
        descriptionTitleText = (TextView) findViewById(R.id.act_dish_description_title);
        ingridientIcon = (IconsTextView)findViewById(R.id.act_dish_ingridients_icon);
        commentIcon = (IconsTextView)findViewById(R.id.act_dish_comments_icon);
        ingridientArrow = (IconsTextView)findViewById(R.id.act_dish_ingridients_arrow);
        commentArrow = (IconsTextView)findViewById(R.id.act_dish_comment_arrow);
        addDishButton = (ImageButton) findViewById(R.id.act_dish_add_dish_btn);

        ingridientIcon.setText(String.valueOf((char) 0xe91e));
        commentIcon.setText(String.valueOf((char) 0xe91d));
        ingridientArrow.setText(String.valueOf((char) 0xe902));
        commentArrow.setText(String.valueOf((char) 0xe902));

        //descriptionText.setText(currentDishDescription.description);
        String price = "$" + String.format("%.2f", currentDishDescription.price);
        priceText.setText(price);
        nameText.setText(currentDishDescription.name);

        ingridientsLayout.setBackgroundColor(Color.WHITE);
        commentLayout.setBackgroundColor(Color.WHITE);
        priceNameLayout.setBackgroundColor(Color.parseColor("#EFEEEB"));
        descriptionText.setBackgroundColor(Color.WHITE);
        descriptionTitleText.setBackgroundColor(Color.WHITE);
        descriptionLayout.setBackgroundColor(Color.WHITE);

        final Window window = this.getWindow();
        window.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        headerPager = (ViewPager) findViewById(R.id.act_dish_header);
        headerAdapter = new HeaderAdapter(getSupportFragmentManager());
        headerAdapter.images.addAll(currentDishDescription.images);
        headerPager.setAdapter(headerAdapter);

        scrollView = (ParallaxScrollView)findViewById(R.id.act_dish_paralax_scroll);
        scrollView.onScrollCallback = new ParallaxScrollView.ParallaxScrollViewOnScroll() {
            @Override
            public void onScroll(int l, int t, int oldl, int oldt) {

                int headerPagerHeight = headerPager.getHeight();
                int toolbarHeight = toolbar.getHeight();

                int height = statusBarHeight + toolbarHeight;
                int pos = headerPagerHeight - height;
                isToolbarVisible = pos <= t;
                updateToolbar();
            }
        };

        btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DemoService.getIsDemoMode()){

                }
                else{
                    if(!Helper.isConnectionAvailable(DishActivity.this)){
                        Helper.showAlert(DishActivity.this, R.string.alert_no_connection_title, R.string.alert_no_connection_message);
                    }
                    else{
                        Helper.showLoadingDialog(DishActivity.this, "", "");

                        if(!currentDishDescription.isFavorite){
                            ApiService.setDishFavorite(DishActivity.this, currentMenuDishVO.id, new ApiService.ApiResponse() {
                                @Override
                                public void response(Boolean isSuccess, Object payload) {
                                    Helper.dismissLoadingDialog();
                                    if(isSuccess){
                                        currentDishDescription.isFavorite = true;
                                        updateToolbar();
                                    }
                                }
                            });
                        }
                        else{
                            ApiService.removeDishFavorite(DishActivity.this, currentMenuDishVO.id, new ApiService.ApiResponse() {
                                @Override
                                public void response(Boolean isSuccess, Object payload) {
                                    Helper.dismissLoadingDialog();
                                    if(isSuccess){
                                        currentDishDescription.isFavorite = false;
                                        updateToolbar();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });

        addDishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OrderService.addDish(currentMenuDishVO);
                updateDishCount();
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!DemoService.getIsDemoMode()) {
                    if(headerAdapter != null){
                        HeaderPage headerPage = headerAdapter.getCurrentPage();
                        if(headerPage != null && headerPage.imageView != null){
                            Helper.shareImage(DishActivity.this, headerPage.imageView);
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        updateDishCount();
        updateToolbar();
    }

    private void updateDishCount(){

        int orderCount = OrderService.getOrderCount();
        int currentOrderDishCount = OrderService.getDishCount(currentMenuDishVO);
        if(orderCount > 0){

            Double currentOrderPrice = OrderService.getOrderPrice();
            btnOrder.setVisibility(View.VISIBLE);
            btnOrder.setText(getString(R.string.btn_proceed_order_label) + " $" + String.format("%.2f", currentOrderPrice));

            if(currentOrderDishCount > 0) {
                dishCountLayout.setVisibility(View.VISIBLE);
                dishCountText.setText(String.valueOf(currentOrderDishCount));
            }
            else{
                dishCountLayout.setVisibility(View.INVISIBLE);
            }
        }
        else{
            dishCountLayout.setVisibility(View.INVISIBLE);
            btnOrder.setVisibility(View.GONE);
        }
    }

    private void updateToolbar(){

        final Window window = this.getWindow();

        if(currentDishDescription.isFavorite){
            btnLike.setText(String.valueOf((char) 0xe914));
        }
        else {
            btnLike.setText(String.valueOf((char) 0xe90d));
        }

        if(isToolbarVisible){
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.LTGRAY);
            backButton.setColorFilter(ContextCompat.getColor(DishActivity.this, R.color.gold));
            toolbar.getBackground().setAlpha(255);    // // 20%  255 = 100%
            btnLike.setTextColor(ContextCompat.getColor(DishActivity.this, R.color.gold));
            btnShare.setTextColor(ContextCompat.getColor(DishActivity.this, R.color.gold));
            toolbarTitle.setTextColor(ContextCompat.getColor(DishActivity.this, R.color.gold));
        }
        else{
            backButton.setColorFilter(ContextCompat.getColor(DishActivity.this, R.color.white));
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            toolbar.getBackground().setAlpha(0);    // // 20%  255 = 100%
            btnLike.setTextColor(ContextCompat.getColor(DishActivity.this, R.color.white));
            btnShare.setTextColor(ContextCompat.getColor(DishActivity.this, R.color.white));
            toolbarTitle.setTextColor(ContextCompat.getColor(DishActivity.this, R.color.white));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public static class HeaderAdapter extends FragmentPagerAdapter {

        public ArrayList<Integer> images = new ArrayList<Integer>();

        public HeaderAdapter(FragmentManager fm) {
            super(fm);
        }

        private HeaderPage mCurrentFragment;

        public HeaderPage getCurrentPage() {
            return mCurrentFragment;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            if (getCurrentPage() != object) {
                mCurrentFragment = ((HeaderPage) object);
            }
            super.setPrimaryItem(container, position, object);
        }

        @Override
        public Fragment getItem(int i) {

            Fragment fragment = new HeaderPage();
            Bundle args = new Bundle();
            args.putInt("id", images.get(i));
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return images.size();
        }
    }


    public static class HeaderPage extends Fragment {

        private ImageView imageView;
        private ProgressBar progressView;


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            Bundle args = getArguments();
            int imageID = args.getInt("id");

            View rootView = inflater.inflate(R.layout.header_image_fragment, container, false);
            imageView = (ImageView) rootView.findViewById(R.id.header_image_fr_image);
            progressView = (ProgressBar) rootView.findViewById(R.id.header_image_fr_progress);

            progressView.setVisibility(View.VISIBLE);

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(currentDishDescription.images.size() > 0){

                        if(DemoService.getIsDemoMode()){
                            Intent intent = new Intent(getContext(), PhotoGridViewActivity.class);
                            PhotoGridViewActivity.photosVO = new PhotoGridViewActivity.PhotosIDsVO(currentDishDescription.name, currentDishDescription.images);
                            startActivity(intent);
                        }
                        else{
                            Intent intent = new Intent(getContext(), PhotoGridViewActivity.class);
                            ArrayList<String> imagesURLs = ApiService.getDisheImagesUrlsFromIDs(currentDishDescription.images);
                            PhotoGridViewActivity.photosVO = new PhotoGridViewActivity.PhotosURLsVO(currentDishDescription.name, imagesURLs);
                            startActivity(intent);
                        }
                    }


                }
            });

            if(DemoService.getIsDemoMode()){
                Picasso.with(getActivity())
                        .load(imageID)
                        .placeholder(R.drawable.bg_dish)
                        .fit()
                        .tag(getActivity())
                        .centerCrop()
                        .into(imageView, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                progressView.setVisibility(View.INVISIBLE);
                            }
                            @Override
                            public void onError() {

                            }
                        });
            }
            else{
                String imageURL = ApiService.getDishImageURL(imageID);
                Picasso.with(getActivity())
                        .load(imageURL)
                        .placeholder(R.drawable.bg_dish)
                        .fit()
                        .tag(getActivity())
                        .centerCrop()
                        .into(imageView, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                progressView.setVisibility(View.INVISIBLE);
                            }
                            @Override
                            public void onError() {

                            }
                        });
            }

            return rootView;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();

            Picasso.with(getActivity()).cancelRequest(imageView);
        }
    }
}
