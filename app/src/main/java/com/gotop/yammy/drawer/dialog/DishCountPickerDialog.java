package com.gotop.yammy.drawer.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

import com.gotop.yammy.R;

public class DishCountPickerDialog extends AppCompatDialog {


    public interface DishCountPickerDialogCallback {
        void onSelect(int dishCount);
    }

    DishCountPickerDialogCallback dishCountPickerDialogCallback;
    int currentDishCount;

    public DishCountPickerDialog(Context context, int currentDishCount, DishCountPickerDialogCallback dishCountPickerDialogCallback) {
        super(context);
        this.currentDishCount = currentDishCount;
        this.dishCountPickerDialogCallback = dishCountPickerDialogCallback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dish_count_picker_dialog);

        final NumberPicker picker = (NumberPicker)findViewById(R.id.dish_count_picker_number_picker);
        picker.setMinValue(0);// restricted number to minimum value i.e 1
        picker.setMaxValue(10);// restricked number to maximum value i.e. 31
        picker.setValue(this.currentDishCount);
        //picker.setWrapSelectorWheel(true);

        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener()
        {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal)
            {

            }
        });

        Button setBtn = (Button) findViewById(R.id.dish_count_picker_apply_button);
        setBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dishCountPickerDialogCallback.onSelect(picker.getValue());
                dismiss();
            }
        });
    }

}
