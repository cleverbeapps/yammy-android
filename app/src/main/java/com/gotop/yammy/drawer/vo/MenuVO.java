package com.gotop.yammy.drawer.vo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MenuVO {

    public ArrayList<MenuCategoryVO> categories = new ArrayList<MenuCategoryVO>();

    static public MenuVO fromJSON(JSONArray jsonArray){

        MenuVO menuVO = new MenuVO();

        try {
            if(jsonArray != null){
                for (int index = 0; index < jsonArray.length(); index++) {
                    JSONObject categoryJSON = jsonArray.getJSONObject(index);
                    menuVO.categories.add(MenuCategoryVO.fromJSON(categoryJSON));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return menuVO;
    }
}
