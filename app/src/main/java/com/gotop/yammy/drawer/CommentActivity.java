package com.gotop.yammy.drawer;

import android.graphics.Color;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.service.OrderService;

public class CommentActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView toolbarTitle;
    private ImageButton backButton;

    private TextInputLayout commentTextLayout;
    private TextInputEditText commentText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        toolbar = (Toolbar)findViewById(R.id.comment_toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_back_title);
        toolbarTitle.setText("Request Comment");
        backButton = (ImageButton) toolbar.findViewById(R.id.toolbar_back_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        final Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(Color.LTGRAY);
        backButton.setColorFilter(ContextCompat.getColor(CommentActivity.this, R.color.gold));
        toolbarTitle.setHintTextColor(ContextCompat.getColor(CommentActivity.this, R.color.gold));
        toolbar.getBackground().setAlpha(255);    // // 20%  255 = 100%

        commentTextLayout = (TextInputLayout) findViewById(R.id.comment_input_layout);
        commentText = (TextInputEditText)findViewById(R.id.comment_input_layout_text);
        commentText.setText(OrderService.requestComment);
        requestFocus(commentTextLayout);
    }

    @Override
    public void onBackPressed() {

        OrderService.requestComment = commentText.getText().toString();

        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }
}
}
