package com.gotop.yammy.drawer.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.service.OrderService;
import com.gotop.yammy.utils.AnimCheckBox;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Objects;


public class OrderSuccessDialog extends AppCompatDialog {

    AnimCheckBox animCheckBox;

    public interface OrderSuccessDialogCallback {
        void onClose();
    }

    OrderSuccessDialogCallback orderSuccessDialogCallback;

    public OrderSuccessDialog(Context context, OrderSuccessDialogCallback orderSuccessDialogCallback) {
        super(context);
        this.orderSuccessDialogCallback = orderSuccessDialogCallback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.order_success_layout);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ViewCompat.setElevation(getWindow().getDecorView(), 10);

        animCheckBox = (AnimCheckBox)findViewById(R.id.order_success_animcheckbox);
        TextView orgName = (TextView) findViewById(R.id.order_success_org_name_text);
        TextView requestDate = (TextView) findViewById(R.id.order_success_date_text);
        TextView requestTime = (TextView) findViewById(R.id.order_success_time_text);
        TextView orgAddress = (TextView) findViewById(R.id.order_success_adress_text);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.order_success_linear_layout);

        orgName.setText(OrderService.getRequestOrg().name);

        DateFormat dateFormat = SimpleDateFormat.getDateInstance();
        String requestDateText = dateFormat.format(OrderService.getRequestDate());
        requestDate.setText(requestDateText);

        if(Objects.equals(OrderService.getRequestOrderType(), OrderService.ORDER_TYPE_RESTAURANT)){
            requestTime.setText("Table for " + OrderService.getRequestPeopleCount() + ", " + OrderService.getRequestEarliestDateTime().getInlineString() + " - " + OrderService.getRequestLatestDateTime().getInlineString());
        }
        else if(Objects.equals(OrderService.getRequestOrderType(), OrderService.ORDER_TYPE_YOURSELF)){
            requestTime.setText("Yourself, " + OrderService.getRequestEarliestDateTime().getInlineString() + " - " + OrderService.getRequestLatestDateTime().getInlineString());
        }
        else if(Objects.equals(OrderService.getRequestOrderType(), OrderService.ORDER_TYPE_DELIVERY)){
            requestTime.setText(requestDateText + "Delivery, " + OrderService.getRequestEarliestDateTime().getInlineString() + " - " + OrderService.getRequestLatestDateTime().getInlineString());
        }

        orgAddress.setText(OrderService.getRequestOrg().address);


        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                orderSuccessDialogCallback.onClose();
            }
        });

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        animCheckBox.setChecked(true, true);
    }

    @Override
    public void onBackPressed() {
        dismiss();
    }
}
