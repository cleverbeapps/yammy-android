package com.gotop.yammy.drawer;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.drawer.vo.CuisineVO;
import com.gotop.yammy.drawer.vo.MenuVO;
import com.gotop.yammy.drawer.vo.OrgVO;
import com.gotop.yammy.photo_browser.PhotoGridViewActivity;
import com.gotop.yammy.service.ApiService;
import com.gotop.yammy.service.DemoService;
import com.gotop.yammy.service.OrderService;
import com.gotop.yammy.utils.Helper;
import com.gotop.yammy.utils.IconsTextView;
import com.gotop.yammy.utils.ParallaxScrollView;
import com.greenfrvr.hashtagview.HashtagView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OrgInfoActivity extends AppCompatActivity{

    private Toolbar toolbar;
    private ImageButton backButton;
    private HeaderAdapter headerAdapter;
    private ViewPager headerPager;
    private ParallaxScrollView scrollView;
    private RelativeLayout menuLayout;
    private RelativeLayout addressLayout;
    private RelativeLayout hoursLayout;
    private RelativeLayout contactsLayout;
    private RelativeLayout descriptionLayout;
    private Button btnOrder;
    private Button btnCheckin;
    private IconsTextView btnLike;
    private IconsTextView btnShare;
    private TextView rateText;
    private TextView nameText;
    private TextView descriptionText;
    private TextView addressText;
    private HashtagView tagsView;
    private IconsTextView menuIcon;
    private IconsTextView addressIcon;
    private IconsTextView hoursIcon;
    private IconsTextView contactsIcon;

    private IconsTextView menuArrow;
    private IconsTextView addressArrow;
    private IconsTextView hoursArrow;
    private IconsTextView contactsArrow;


    static private OrgVO _currentOrgVO;
    static public OrgVO getCurrentOrgVO(){
        return _currentOrgVO;
    }
    static public void setgetCurrentOrgVO(OrgVO orgVO){
        _currentOrgVO = orgVO;
        OrderService.setRequestOrg(orgVO);
    }

    private Boolean isToolbarVisible = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_org_info);

        final int statusBarHeight = Helper.getStatusBarHeight(this);

        toolbar = (Toolbar)findViewById(R.id.org_info_toolbar);
        toolbar.getBackground().setAlpha(0);    // // 20%  255 = 100%
        backButton = (ImageButton) toolbar.findViewById(R.id.toolbar_back_btn);

        // Set the padding to match the Status Bar height
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) toolbar.getLayoutParams();
        lp.setMargins(0, statusBarHeight, 0, 0);

        TextView toolbarTitle = (TextView)toolbar.findViewById(R.id.toolbar_back_title);
        toolbarTitle.setVisibility(View.GONE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnLike = (IconsTextView)toolbar.findViewById(R.id.toolbar_org_like_btn);
        btnShare = (IconsTextView)toolbar.findViewById(R.id.toolbar_org_share_btn);

        btnShare.setText(String.valueOf((char) 0xe918));

        menuLayout = (RelativeLayout)findViewById(R.id.activity_org_info_menu_layout);
        addressLayout = (RelativeLayout)findViewById(R.id.activity_org_adress_layout);
        addressText = (TextView) findViewById(R.id.activity_org_info_adress_btn);
        hoursLayout = (RelativeLayout)findViewById(R.id.activity_org_hours_layout);
        contactsLayout = (RelativeLayout)findViewById(R.id.activity_org_contacts_layout);
        descriptionLayout = (RelativeLayout)findViewById(R.id.activity_org_description_layout);
        btnCheckin = (Button) findViewById(R.id.act_org_info_btn_check_in);
        rateText = (TextView) findViewById(R.id.act_org_info_rate);
        nameText = (TextView) findViewById(R.id.act_org_info_name);
        descriptionText = (TextView) findViewById(R.id.act_org_info_description);
        tagsView = (HashtagView) findViewById(R.id.act_org_info_tag_view);
        menuIcon = (IconsTextView)findViewById(R.id.activity_org_info_menu_icon);
        addressIcon = (IconsTextView)findViewById(R.id.activity_org_info_address_icon);
        hoursIcon = (IconsTextView)findViewById(R.id.activity_org_info_hours_icon);
        contactsIcon = (IconsTextView)findViewById(R.id.activity_org_info_contacts_icon);
        menuArrow = (IconsTextView)findViewById(R.id.activity_org_info_menu_arrow);
        addressArrow = (IconsTextView)findViewById(R.id.activity_org_info_address_arrow);
        hoursArrow = (IconsTextView)findViewById(R.id.activity_org_info_hours_arrow);
        contactsArrow = (IconsTextView)findViewById(R.id.activity_org_info_contacts_arrow);

        btnOrder = (Button) findViewById(R.id.act_org_info_btn_order);
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrgInfoActivity.this, RequestActivity.class);
                RequestActivity.isInvokedFromOrgInfoActivity = true;
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            }
        });

        menuIcon.setText(String.valueOf((char) 0xe913));
        addressIcon.setText(String.valueOf((char) 0xe912));
        hoursIcon.setText(String.valueOf((char) 0xe909));
        contactsIcon.setText(String.valueOf((char) 0xe91b));

        menuArrow.setText(String.valueOf((char) 0xe902));
        addressArrow.setText(String.valueOf((char) 0xe902));
        hoursArrow.setText(String.valueOf((char) 0xe902));
        contactsArrow.setText(String.valueOf((char) 0xe902));

        descriptionText.setText(getCurrentOrgVO().description);
        addressText.setText(getCurrentOrgVO().address);
        rateText.setText(String.valueOf(getCurrentOrgVO().rating));
        nameText.setText(getCurrentOrgVO().name);

        tagsView.removeAllViews();
        ArrayList<String> cuisines = new ArrayList<>();
        for (int i = 0; i < getCurrentOrgVO().cuisines.size(); i++) {
            CuisineVO cuisine = getCurrentOrgVO().cuisines.get(i);
            cuisines.add(cuisine.name);
        }
        tagsView.setData(cuisines);

        menuLayout.setBackgroundColor(Color.WHITE);
        addressLayout.setBackgroundColor(Color.WHITE);
        hoursLayout.setBackgroundColor(Color.WHITE);
        contactsLayout.setBackgroundColor(Color.WHITE);
        contactsLayout.setBackgroundColor(Color.WHITE);
        descriptionLayout.setBackgroundColor(Color.WHITE);

        final Window window = this.getWindow();
        window.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// clear FLAG_TRANSLUCENT_STATUS flag:
//        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
       // window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//// finally change the color
        //window.setStatusBarColor(Color.TRANSPARENT);

        headerPager = (ViewPager) findViewById(R.id.act_org_info_header);
        headerAdapter = new HeaderAdapter(getSupportFragmentManager());
        headerAdapter.images.addAll(getCurrentOrgVO().images);
        headerPager.setAdapter(headerAdapter);

        scrollView = (ParallaxScrollView)findViewById(R.id.act_org_info_paralax_scroll);
        scrollView.onScrollCallback = new ParallaxScrollView.ParallaxScrollViewOnScroll() {
            @Override
            public void onScroll(int l, int t, int oldl, int oldt) {

                int headerPagerHeight = headerPager.getHeight();
                int toolbarHeight = toolbar.getHeight();

                int height = statusBarHeight + toolbarHeight;
                int pos = headerPagerHeight - height;
                isToolbarVisible = pos <= t;
                updateToolbar();
            }
        };

        menuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DemoService.getIsDemoMode()){
                    MenuVO menuVO = (MenuVO) DemoService.getDemoVO(OrgInfoActivity.this, DemoService.DEMO_MENU_VO_PATH);
                    Intent intent = new Intent(OrgInfoActivity.this, OrgMenuActivity.class);
                    OrgMenuActivity.currentMenuVO = menuVO;
                    OrgInfoActivity.this.startActivity(intent);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
                else{
                    if(!Helper.isConnectionAvailable(OrgInfoActivity.this)){
                        Helper.showAlert(OrgInfoActivity.this, R.string.alert_no_connection_title, R.string.alert_no_connection_message);
                    }
                    else{
                        Helper.showLoadingDialog(OrgInfoActivity.this, "", "");

                        ApiService.getMenuByOrgID(OrgInfoActivity.getCurrentOrgVO().object_id, new ApiService.ApiResponse() {
                            @Override
                            public void response(Boolean isSuccess, Object payload) {
                                Helper.dismissLoadingDialog();
                                if(isSuccess){
                                    MenuVO menuVO = (MenuVO)payload;
                                    Intent intent = new Intent(OrgInfoActivity.this, OrgMenuActivity.class);
                                    OrgMenuActivity.currentMenuVO = menuVO;
                                    OrgInfoActivity.this.startActivity(intent);
                                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                                }
                            }
                        });
                    }
                }
            }
        });

        btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DemoService.getIsDemoMode()){

                }
                else{
                    if(!Helper.isConnectionAvailable(OrgInfoActivity.this)){
                        Helper.showAlert(OrgInfoActivity.this, R.string.alert_no_connection_title, R.string.alert_no_connection_message);
                    }
                    else{
                        Helper.showLoadingDialog(OrgInfoActivity.this, "", "");

                        if(getCurrentOrgVO().isFavorite == false){
                            ApiService.setOrgFavorite(OrgInfoActivity.this, getCurrentOrgVO().object_id, new ApiService.ApiResponse() {
                                @Override
                                public void response(Boolean isSuccess, Object payload) {
                                    Helper.dismissLoadingDialog();
                                    if(isSuccess){
                                        getCurrentOrgVO().isFavorite = true;
                                        updateToolbar();
                                    }
                                }
                            });
                        }
                        else{
                            ApiService.removeOrgFavorite(OrgInfoActivity.this, getCurrentOrgVO().object_id, new ApiService.ApiResponse() {
                                @Override
                                public void response(Boolean isSuccess, Object payload) {
                                    Helper.dismissLoadingDialog();
                                    if(isSuccess){
                                        getCurrentOrgVO().isFavorite = false;
                                        updateToolbar();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!DemoService.getIsDemoMode()){
                    if(headerAdapter != null){
                        HeaderPage headerPage = headerAdapter.getCurrentPage();
                        if(headerPage != null && headerPage.imageView != null){
                            Helper.shareImage(OrgInfoActivity.this, headerPage.imageView);
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        updateToolbar();
    }

    private void updateToolbar(){

        final Window window = this.getWindow();

        if(getCurrentOrgVO().isFavorite){
            btnLike.setText(String.valueOf((char) 0xe914));
        }
        else {
            btnLike.setText(String.valueOf((char) 0xe90d));
        }

        if(isToolbarVisible){
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.LTGRAY);
            backButton.setColorFilter(ContextCompat.getColor(OrgInfoActivity.this, R.color.gold));
            toolbar.getBackground().setAlpha(255);    // // 20%  255 = 100%
            btnLike.setTextColor(ContextCompat.getColor(OrgInfoActivity.this, R.color.gold));
            btnShare.setTextColor(ContextCompat.getColor(OrgInfoActivity.this, R.color.gold));
        }
        else{
            backButton.setColorFilter(ContextCompat.getColor(OrgInfoActivity.this, R.color.white));
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            toolbar.getBackground().setAlpha(0);    // // 20%  255 = 100%
            btnLike.setTextColor(ContextCompat.getColor(OrgInfoActivity.this, R.color.white));
            btnShare.setTextColor(ContextCompat.getColor(OrgInfoActivity.this, R.color.white));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public static class HeaderAdapter extends FragmentPagerAdapter {

        public ArrayList<Integer> images = new ArrayList<Integer>();

        public HeaderAdapter(FragmentManager fm) {
            super(fm);
        }

        private HeaderPage mCurrentFragment;

        public HeaderPage getCurrentPage() {
            return mCurrentFragment;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            if (getCurrentPage() != object) {
                mCurrentFragment = ((HeaderPage) object);
            }
            super.setPrimaryItem(container, position, object);
        }

        @Override
        public Fragment getItem(int i) {

            HeaderPage headerPage = new HeaderPage();
            Bundle args = new Bundle();
            args.putInt("id", images.get(i));
            headerPage.setArguments(args);
            return headerPage;
        }

        @Override
        public int getCount() {
            return images.size();
        }
    }


    public static class HeaderPage extends Fragment {

        public ImageView imageView;
        public ProgressBar progressView;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            Bundle args = getArguments();
            int imageID = args.getInt("id");

            View rootView = inflater.inflate(R.layout.header_image_fragment, container, false);
            imageView = (ImageView) rootView.findViewById(R.id.header_image_fr_image);
            progressView = (ProgressBar) rootView.findViewById(R.id.header_image_fr_progress);

            progressView.setVisibility(View.VISIBLE);

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(getCurrentOrgVO().images.size() > 0){

                        if(DemoService.getIsDemoMode()){
                            Intent intent = new Intent(getContext(), PhotoGridViewActivity.class);
                            PhotoGridViewActivity.photosVO = new PhotoGridViewActivity.PhotosIDsVO(getCurrentOrgVO().name, getCurrentOrgVO().images);
                            startActivity(intent);
                        }
                        else{
                            Intent intent = new Intent(getContext(), PhotoGridViewActivity.class);
                            ArrayList<String> imagesURLs = ApiService.getOrgImagesUrlsFromIDs(getCurrentOrgVO().images);
                            PhotoGridViewActivity.photosVO = new PhotoGridViewActivity.PhotosURLsVO(getCurrentOrgVO().name, imagesURLs);
                            startActivity(intent);
                        }

                    }
                }
            });

            if(DemoService.getIsDemoMode()){
                Picasso.with(getActivity())
                        .load(imageID)
                        .placeholder(R.drawable.org_cell_bg)
                        .fit()
                        .tag(getActivity())
                        .centerCrop()
                        .into(imageView, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                progressView.setVisibility(View.INVISIBLE);
                            }
                            @Override
                            public void onError() {

                            }
                        });
            }
            else{
                String imageURL = ApiService.getOrgImageURL(imageID);
                Picasso.with(getActivity())
                        .load(imageURL)
                        .placeholder(R.drawable.org_cell_bg)
                        .fit()
                        .tag(getActivity())
                        .centerCrop()
                        .into(imageView, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                progressView.setVisibility(View.INVISIBLE);
                            }
                            @Override
                            public void onError() {

                            }
                        });
            }

            return rootView;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();

            Picasso.with(getActivity()).cancelRequest(imageView);
        }
    }
}
