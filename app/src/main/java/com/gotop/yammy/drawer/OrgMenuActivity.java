package com.gotop.yammy.drawer;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.drawer.dialog.DishCountPickerDialog;
import com.gotop.yammy.drawer.vo.DishDescriptionVO;
import com.gotop.yammy.drawer.vo.MenuCategoryVO;
import com.gotop.yammy.drawer.vo.MenuDishVO;
import com.gotop.yammy.drawer.vo.MenuVO;
import com.gotop.yammy.service.ApiService;
import com.gotop.yammy.service.DemoService;
import com.gotop.yammy.service.OrderService;
import com.gotop.yammy.utils.Helper;
import com.gotop.yammy.utils.IconsTextView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class OrgMenuActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageButton backButton;
    private IconsTextView btnLike;
    private IconsTextView btnShare;
    private Button btnProccedOrder;
    private Button btnCallWaiter;
    private RelativeLayout btnOrderDishCountLayout;
    private RelativeLayout btnOrderCallLayout;
    private TextView dishCountText;
    private TextView orgNameText;
    TabLayout tabs;
    ViewPager viewPager;

    static public MenuVO currentMenuVO;

    private EventBus bus = EventBus.getDefault();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_org_menu);

        final Window window = this.getWindow();
        window.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = (ViewPager) findViewById(R.id.act_org_menu_pager);
        viewPager.setAdapter(new MenuCategoryPagerAdapter(getSupportFragmentManager(),
                OrgMenuActivity.this, currentMenuVO.categories, this));
        tabs = (TabLayout) findViewById(R.id.act_org_menu_tabs);
        tabs.setupWithViewPager(viewPager);


        toolbar = (Toolbar)findViewById(R.id.org_menu_toolbar);
        toolbar.getBackground().setAlpha(0);    // // 20%  255 = 100%

        backButton = (ImageButton) toolbar.findViewById(R.id.toolbar_back_btn);
        btnProccedOrder = (Button) findViewById(R.id.act_org_menu_btn_order);
        btnProccedOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrgMenuActivity.this, OrderActivity.class);
                startActivity(intent);
            }
        });

        btnCallWaiter = (Button) findViewById(R.id.act_org_menu_call_waiter);
        btnOrderDishCountLayout = (RelativeLayout) findViewById(R.id.act_org_menu_order_count_layout);
        btnOrderCallLayout = (RelativeLayout) findViewById(R.id.act_org_menu_buttons_layout);
        dishCountText = (TextView) findViewById(R.id.org_menu_dish_count_text);
        dishCountText.setText("0");
        orgNameText = (TextView) findViewById(R.id.act_org_menu_name);
        orgNameText.setText(OrgInfoActivity.getCurrentOrgVO().name);

        // Set the padding to match the Status Bar height
        final int statusBarHeight = Helper.getStatusBarHeight(this);

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) toolbar.getLayoutParams();
        lp.setMargins(0, statusBarHeight, 0, 0);

        TextView toolbarTitle = (TextView)toolbar.findViewById(R.id.toolbar_back_title);
        if(toolbarTitle != null){
            toolbarTitle.setText(OrgInfoActivity.getCurrentOrgVO().name);
        }
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView title = (TextView)findViewById(R.id.toolbar_back_title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.btn_menu_label);

        btnLike = (IconsTextView)toolbar.findViewById(R.id.toolbar_org_like_btn);
        btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DemoService.getIsDemoMode()){

                }
                else{
                    if(!Helper.isConnectionAvailable(OrgMenuActivity.this)){
                        Helper.showAlert(OrgMenuActivity.this, R.string.alert_no_connection_title, R.string.alert_no_connection_message);
                    }
                    else{
                        Helper.showLoadingDialog(OrgMenuActivity.this, "", "");

                        if(OrgInfoActivity.getCurrentOrgVO().isFavorite == false){
                            ApiService.setOrgFavorite(OrgMenuActivity.this, OrgInfoActivity.getCurrentOrgVO().object_id, new ApiService.ApiResponse() {
                                @Override
                                public void response(Boolean isSuccess, Object payload) {
                                    Helper.dismissLoadingDialog();
                                    if(isSuccess){
                                        OrgInfoActivity.getCurrentOrgVO().isFavorite = true;
                                        updateToolbar();
                                    }
                                }
                            });
                        }
                        else{
                            ApiService.removeOrgFavorite(OrgMenuActivity.this, OrgInfoActivity.getCurrentOrgVO().object_id, new ApiService.ApiResponse() {
                                @Override
                                public void response(Boolean isSuccess, Object payload) {
                                    Helper.dismissLoadingDialog();
                                    if(isSuccess){
                                        OrgInfoActivity.getCurrentOrgVO().isFavorite = false;
                                        updateToolbar();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });

        final ImageView headerImage = (ImageView) findViewById(R.id.act_org_menu_header_image);
        final ProgressBar progressBar = (ProgressBar)findViewById(R.id.act_org_menu_header_image_progress);

        btnShare = (IconsTextView)toolbar.findViewById(R.id.toolbar_org_share_btn);
        btnShare.setText(String.valueOf((char) 0xe918));
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!DemoService.getIsDemoMode()) {
                    if(headerImage != null){
                        Helper.shareImage(OrgMenuActivity.this, headerImage);
                    }
                }
            }
        });

        if(DemoService.getIsDemoMode()){
            int image = OrgInfoActivity.getCurrentOrgVO().header_image;
            Picasso.with(this)
                    .load(image)
                    .placeholder(R.drawable.org_cell_bg)
                    .fit()
                    .centerCrop()
                    .into(headerImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        @Override
                        public void onError() {

                        }
                    });
        }
        else{
            String imageURL = ApiService.getOrgImageURL(OrgInfoActivity.getCurrentOrgVO().header_image);
            Picasso.with(this)
                    .load(imageURL)
                    .placeholder(R.drawable.org_cell_bg)
                    .fit()
                    .centerCrop()
                    .into(headerImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        @Override
                        public void onError() {

                        }
                    });
        }

        initSearch();
        updateToolbar();

        bus.register(this);
    }

    private void initSearch(){
        final EditText searchEditText = (EditText) findViewById(R.id.act_org_menu_search_edit_text);
        searchEditText.setVisibility(View.GONE);
        final Button searchCancelBtn = (Button) findViewById(R.id.act_org_search_cancel_btn);
        searchCancelBtn.setVisibility(View.GONE);
        final RecyclerView seatchList = (RecyclerView) findViewById(R.id.act_org_search_result_list);
        seatchList.setVisibility(View.GONE);
        final SearchAdapter searchAdapter = new SearchAdapter(this);
        seatchList.setAdapter(searchAdapter);
        seatchList.setLayoutManager(new LinearLayoutManager(this));

        searchCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchEditText.setText("");
                tabs.setVisibility(View.VISIBLE);
                viewPager.setVisibility(View.VISIBLE);
                searchCancelBtn.setVisibility(View.GONE);
                searchEditText.setVisibility(View.GONE);
                seatchList.setVisibility(View.GONE);
            }
        });
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                searchAdapter.updateSearch(searchEditText.getText().toString());
            }
        });

        IconsTextView btnSearch = (IconsTextView)findViewById(R.id.activity_org_menu_search_icon);
        btnSearch.setText(String.valueOf((char) 0xe91f));
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(searchEditText.getVisibility() == View.GONE){
                    searchCancelBtn.setVisibility(View.VISIBLE);
                    searchEditText.setVisibility(View.VISIBLE);
                    seatchList.setVisibility(View.VISIBLE);
                    tabs.setVisibility(View.GONE);
                    viewPager.setVisibility(View.GONE);

                    requestFocus(searchEditText);
                }
                else{
                    tabs.setVisibility(View.VISIBLE);
                    viewPager.setVisibility(View.VISIBLE);
                    searchCancelBtn.setVisibility(View.GONE);
                    searchEditText.setVisibility(View.GONE);
                    seatchList.setVisibility(View.GONE);
                }
            }
        });
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Subscribe
    public void onEvent(OrderService.OrderUpdate event){
        updateMenu();
    }

    @Override
    protected void onStart() {
        super.onStart();

        updateMenu();
        updateToolbar();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    private void updateMenu(){
        int orderCount = OrderService.getOrderCount();
        Double orderPrice = OrderService.getOrderPrice();
        if(orderCount > 0){
            btnOrderCallLayout.setVisibility(View.VISIBLE);
            btnProccedOrder.setText(getString(R.string.btn_proceed_order_label) + " $" + String.format("%.2f", orderPrice));
            btnOrderDishCountLayout.setVisibility(View.VISIBLE);
            dishCountText.setText(String.valueOf(orderCount));
        }
        else{
            btnOrderCallLayout.setVisibility(View.GONE);
            btnOrderDishCountLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

    private void updateToolbar(){

        if(OrgInfoActivity.getCurrentOrgVO().isFavorite){
            btnLike.setText(String.valueOf((char) 0xe914));
        }
        else {
            btnLike.setText(String.valueOf((char) 0xe90d));
        }

        backButton.setColorFilter(ContextCompat.getColor(OrgMenuActivity.this, R.color.white));
        toolbar.getBackground().setAlpha(0);    // // 20%  255 = 100%
        btnLike.setTextColor(ContextCompat.getColor(OrgMenuActivity.this, R.color.white));
        btnShare.setTextColor(ContextCompat.getColor(OrgMenuActivity.this, R.color.white));
    }

    public void showDish(final MenuDishVO menuDishVO){

        if(DemoService.getIsDemoMode()){
            Intent intent = new Intent(OrgMenuActivity.this, DishActivity.class);
            DishActivity.currentDishDescription = (DishDescriptionVO)DemoService.getDemoVO(OrgMenuActivity.this, DemoService.DEMO_DISH_DESCRIPTION_VO_PATH);
            DishActivity.currentMenuDishVO = menuDishVO;
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
        else{
            Helper.showLoadingDialog(this, "", "");

            ApiService.getDishByID(this, menuDishVO.id, new ApiService.ApiResponse() {
                @Override
                public void response(Boolean isSuccess, Object payload) {

                    Helper.dismissLoadingDialog();

                    if(isSuccess){
                        Intent intent = new Intent(OrgMenuActivity.this, DishActivity.class);
                        DishActivity.currentDishDescription = (DishDescriptionVO)payload;
                        DishActivity.currentMenuDishVO = menuDishVO;
                        startActivity(intent);
                        overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    }
                }
            });
        }
    }


    public static class MenuCategoryPageFragment extends Fragment {

        public MenuCategoryVO menuCategoryVO;
        public OrgMenuActivity activity;

        public static MenuCategoryPageFragment newInstance(MenuCategoryVO menuCategoryVO, OrgMenuActivity activity) {

            MenuCategoryPageFragment fragment = new MenuCategoryPageFragment();
            fragment.menuCategoryVO = menuCategoryVO;
            fragment.activity = activity;
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // TODO dish list
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.fragment_menu_category_page, container, false);

            RecyclerView dishList = (RecyclerView) view.findViewById(R.id.fragment_category_menu_page_list);
            final MenuDishListAdapter dishListAdapter = new MenuDishListAdapter(getContext());
            dishListAdapter.activity = activity;
            dishListAdapter.dataProvider = menuCategoryVO.products;
            dishList.setAdapter(dishListAdapter);
            dishList.setLayoutManager(new LinearLayoutManager(getContext()));

            return view;
        }
    }

    public static class MenuCategoryPagerAdapter extends FragmentPagerAdapter {

        private ArrayList<MenuCategoryVO> categories;
        private Context context;
        private OrgMenuActivity activity;

        public MenuCategoryPagerAdapter(FragmentManager fm, Context context, ArrayList<MenuCategoryVO> categories, OrgMenuActivity activity) {
            super(fm);
            this.context = context;
            this.categories = categories;
            this.activity = activity;
        }

        @Override
        public int getCount() {
            return categories.size();
        }

        @Override
        public Fragment getItem(int position) {
            return MenuCategoryPageFragment.newInstance(categories.get(position), activity);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            MenuCategoryVO menuCategoryVO = categories.get(position);
            return menuCategoryVO.category;
        }
    }

    public static class MenuDishListAdapter extends RecyclerView.Adapter<DishListCell>{

        public OrgMenuActivity activity;
        private Context mContext;
        public ArrayList<MenuDishVO> dataProvider = new ArrayList<MenuDishVO>();

        public MenuDishListAdapter(Context context){

            mContext = context;
        }

        @Override
        public DishListCell onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_list_row,parent,false);
            DishListCell dishCell = new DishListCell(v,viewType);
            dishCell.activity = activity;

            return dishCell;
        }

        @Override
        public void onBindViewHolder(final DishListCell holder, int position) {

            final MenuDishVO currentDish = dataProvider.get(position);

            holder.lblDishName.setText(String.valueOf(currentDish.name));
            holder.lblDishDescription.setText(String.valueOf(currentDish.dishDescription));
            holder.lblPrice.setText("$" + String.format("%.2f", currentDish.price));

            holder.btnAddDish.setVisibility(View.VISIBLE);
            holder.btnDeleteDish.setVisibility(View.INVISIBLE);

            holder.setCurrentDish(currentDish);
        }
        @Override
        public int getItemCount() {
            return dataProvider.size();
        }
        @Override
        public long getItemId(int position) {
            return 0;
        }
        @Override
        public int getItemViewType(int position) {
            return 0;
        }
    }


    public static class SearchAdapter extends RecyclerView.Adapter<DishListCell>{

        public OrgMenuActivity activity;
        public ArrayList<MenuDishVO> dataProvider = new ArrayList<MenuDishVO>();

        public SearchAdapter(OrgMenuActivity context){
            activity = context;
        }

        @Override
        public DishListCell onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_list_row,parent,false);
            DishListCell dishCell = new DishListCell(v,viewType);
            dishCell.activity = activity;

            return dishCell;
        }

        public void updateSearch(String serachText){

            dataProvider.clear();
            if(serachText.length() > 0){
                MenuVO menuVO = OrgMenuActivity.currentMenuVO;
                for (int i = 0; i < menuVO.categories.size(); i++) {
                    MenuCategoryVO menuCategoryVO = menuVO.categories.get(i);
                    for (int y = 0; y < menuCategoryVO.products.size(); y++) {
                        MenuDishVO menuDishVO = menuCategoryVO.products.get(y);
                        if(menuDishVO.name.contains(serachText)){
                            dataProvider.add(menuDishVO);
                        }
                    }
                }
                Collections.sort(dataProvider, new Comparator<MenuDishVO>() {
                    @Override
                    public int compare(MenuDishVO d1, MenuDishVO d2) {
                        String name1 = String.valueOf(d1.name.toLowerCase());
                        String name2 = String.valueOf(d2.name.toLowerCase());
                        return name1.compareTo(name2);
                    }
                });
            }

            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(final DishListCell holder, int position) {

            final MenuDishVO currentDish = dataProvider.get(position);

            holder.lblDishName.setText(String.valueOf(currentDish.name));
            holder.lblDishDescription.setText(String.valueOf(currentDish.dishDescription));
            holder.lblPrice.setText("$" + String.format("%.2f", currentDish.price));

            holder.btnAddDish.setVisibility(View.VISIBLE);
            holder.btnDeleteDish.setVisibility(View.INVISIBLE);

            holder.setCurrentDish(currentDish);
        }
        @Override
        public int getItemCount() {
            return dataProvider.size();
        }
        @Override
        public long getItemId(int position) {
            return 0;
        }
        @Override
        public int getItemViewType(int position) {
            return 0;
        }
    }

    public static class DishListCell extends RecyclerView.ViewHolder{

        ImageButton btnAddDish, btnDeleteDish, btnArrow;
        TextView lblDishName, lblDishDescription, lblPrice, lblCount;
        RelativeLayout countLayout;
        Button btnRow;

        public OrgMenuActivity activity;

        MenuDishVO currentDish = null;

        private EventBus bus = EventBus.getDefault();

        public DishListCell(View itemView,int ViewType) {
            super(itemView);

            btnAddDish = (ImageButton) itemView.findViewById(R.id.dish_list_row_btn_add_dish);
            btnDeleteDish = (ImageButton)itemView.findViewById(R.id.btn_delete_dish);
            btnArrow = (ImageButton)itemView.findViewById(R.id.btn_arrow_right);
            lblDishName = (TextView)itemView.findViewById(R.id.text_dish_name);
            lblDishDescription = (TextView)itemView.findViewById(R.id.text_dish_description);
            lblPrice = (TextView)itemView.findViewById(R.id.text_dish_price);
            lblCount = (TextView)itemView.findViewById(R.id.dish_list_row_count_text);
            countLayout = (RelativeLayout) itemView.findViewById(R.id.dish_list_row_count_layout);
            btnRow = (Button) itemView.findViewById(R.id.dish_list_row_layout_btn);

            btnAddDish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(currentDish != null)
                    {
                        OrderService.addDish(currentDish);
                    }
                }
            });
            btnRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.showDish(currentDish);
                }
            });

            final GestureDetector mGestureDetector = new GestureDetector(activity, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public void onLongPress(MotionEvent e) {
                    showDishCountPicker();
                }
            });
            btnRow.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return mGestureDetector.onTouchEvent(event);
                }
            });
        }

        public void setCurrentDish(MenuDishVO menuDishVO){
            this.currentDish = menuDishVO;
            updateCount();
            if(!bus.isRegistered(this)){
                bus.register(this);
            }
        }

        public void unsubscribe(){
            bus.unregister(this);
        }

        @Subscribe
        public void onEvent(OrderService.OrderUpdate event){
            updateCount();
        }

        private void updateCount(){

            if(currentDish != null){
                int currentDishCount = OrderService.getDishCount(currentDish);
                lblCount.setText(String.valueOf(currentDishCount));
                countLayout.setVisibility(currentDishCount > 0 ? View.VISIBLE : View.INVISIBLE);
            }
        }

        private void showDishCountPicker(){
            if(currentDish != null){
                int currentDishCount = OrderService.getDishCount(currentDish);
                DishCountPickerDialog dishCountPickerDialog = new DishCountPickerDialog(activity, currentDishCount, new DishCountPickerDialog.DishCountPickerDialogCallback() {
                    @Override
                    public void onSelect(int dishCount) {
                        OrderService.setCountForDish(currentDish, dishCount);
                    }
                });
                dishCountPickerDialog.show();
            }
        }
    }
}
