package com.gotop.yammy.drawer.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

import com.gotop.yammy.R;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class OrderTipDialog extends AppCompatDialog {


    public interface OrderTipDialogCallback {
        void onSelect(int tip);
    }

    OrderTipDialogCallback orderTipDialogCallback;
    int currentTip;

    public OrderTipDialog(Context context, int currentTip, OrderTipDialogCallback orderTipDialogCallback) {
        super(context);
        this.currentTip = currentTip;
        this.orderTipDialogCallback = orderTipDialogCallback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.order_tip_dialog);

        final NumberPicker picker = (NumberPicker)findViewById(R.id.tip_count_picker_number_picker);
        picker.setFormatter(new NumberPicker.Formatter() {

            @Override
            public String format(int value) {
                return String.valueOf(value) + "%";
            }
        });
        try {
            Method method = picker.getClass().getDeclaredMethod("changeValueByOne", boolean.class);
            method.setAccessible(true);
            method.invoke(picker, true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        picker.setMinValue(0);// restricted number to minimum value i.e 1
        picker.setMaxValue(100);// restricked number to maximum value i.e. 31
        picker.setValue(this.currentTip);
        picker.setWrapSelectorWheel(false);
        picker.invalidate();

        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener()
        {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal)
            {

            }
        });

        Button setBtn = (Button) findViewById(R.id.tip_count_picker_apply_button);
        setBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderTipDialogCallback.onSelect(picker.getValue());
                dismiss();
            }
        });
    }

}
