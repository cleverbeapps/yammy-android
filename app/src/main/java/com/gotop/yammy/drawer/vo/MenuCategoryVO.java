package com.gotop.yammy.drawer.vo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MenuCategoryVO {

    public String category = "";
    public int id = 0;
    public int position = 0;
    public ArrayList<MenuDishVO> products = new ArrayList<MenuDishVO>();

    static public MenuCategoryVO fromJSON(JSONObject json){

        MenuCategoryVO categoryVO = new MenuCategoryVO();

        try {
            categoryVO.id = json.getInt("id");
            categoryVO.category = json.getString("category");
            categoryVO.position = json.getInt("position");

            JSONArray dishes = json.getJSONArray("products");
            if(dishes != null){
                for (int index = 0; index < dishes.length(); index++) {
                    JSONObject dishJSON = dishes.getJSONObject(index);
                    categoryVO.products.add(MenuDishVO.fromJSON(dishJSON));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return categoryVO;
    }
}
