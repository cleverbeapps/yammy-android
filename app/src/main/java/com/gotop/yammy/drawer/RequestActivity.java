package com.gotop.yammy.drawer;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.drawer.vo.MenuVO;
import com.gotop.yammy.service.ApiService;
import com.gotop.yammy.service.DemoService;
import com.gotop.yammy.service.OrderService;
import com.gotop.yammy.utils.Helper;
import com.gotop.yammy.utils.IconsTextView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RequestActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private TextView toolbarTitle;
    private ImageButton backButton;
    private Button btnGoToMenu;
    private Button btnInviteFriend;
    private TextView orgNameText;

    private LinearLayout yourselfLayout;
    private IconsTextView yourselfIcon;
    private TextView yourselfText;

    private LinearLayout deliveryLayout;
    private IconsTextView deliveryIcon;
    private TextView deliveryText;

    private LinearLayout restaurantLayout;
    private IconsTextView restaurantIcon;
    private TextView restaurantText;

    private View act_request_line_2;

    private RelativeLayout peopleSpinner;
    private TextView peopleTitle;
    private TextView peopleFooter;
    private ImageButton leftPeopleBtn;
    private ImageButton rightPeopleBtn;
    private ViewPager peoplePager;
    private PeopleAdapter peoplePagerAdapter;

    private RelativeLayout dateSpinner;
    private ImageButton leftDateBtn;
    private ImageButton rightDateBtn;
    private TextView dateTitle;
    private TextView dateFooter;
    private ViewPager datePager;
    private DateAdapter datePagerAdapter;

    private RelativeLayout earliestSpinner;
    private ImageButton leftEarliestBtn;
    private ImageButton rightEarliestBtn;
    private TextView earliestTitle;
    private TextView earliesFooter;
    private ViewPager earliestPager;
    private EarliestTimeAdapter earliestPagerAdapter;

    private RelativeLayout latestSpinner;
    private ImageButton leftLatestBtn;
    private ImageButton rightLatestBtn;
    private TextView latestTitle;
    private TextView latestFooter;
    private ViewPager latestPager;
    private LatestTimeAdapter latestPagerAdapter;

    private IconsTextView orderTableIcon;

    private EventBus bus = EventBus.getDefault();

    static public Boolean isInvokedFromOrgInfoActivity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);

        toolbar = (Toolbar)findViewById(R.id.request_toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_back_title);
        toolbarTitle.setText("Request");
        backButton = (ImageButton) toolbar.findViewById(R.id.toolbar_back_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        final Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(Color.LTGRAY);
        backButton.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gold));
        toolbarTitle.setHintTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gold));
        toolbar.getBackground().setAlpha(255);    // // 20%  255 = 100%

        final ImageView headerImage = (ImageView) findViewById(R.id.act_request_header_image);
        final ProgressBar progressBar = (ProgressBar)findViewById(R.id.act_request_header_image_progress);

        restaurantLayout = (LinearLayout)findViewById(R.id.act_request_restaurant_layout);
        restaurantIcon = (IconsTextView) findViewById(R.id.act_request_restaurant_icon);
        restaurantIcon.setText(String.valueOf((char) 0xe917));
        restaurantText = (TextView) findViewById(R.id.act_restaurant_text);
        restaurantLayout.setOnClickListener(this);

        act_request_line_2 = (View) findViewById(R.id.act_request_line_2);

        yourselfLayout = (LinearLayout)findViewById(R.id.act_request_yourself_layout);
        yourselfIcon = (IconsTextView) findViewById(R.id.act_request_yourself_icon);
        yourselfIcon.setText(String.valueOf((char) 0xe91a));
        yourselfText = (TextView) findViewById(R.id.act_request_yourself_text);
        yourselfLayout.setOnClickListener(this);

        deliveryLayout = (LinearLayout)findViewById(R.id.act_request_delivery_layout);
        deliveryIcon = (IconsTextView) findViewById(R.id.act_request_delivery_icon);
        deliveryIcon.setText(String.valueOf((char) 0xe90c));
        deliveryText = (TextView) findViewById(R.id.act_delivery_text);
        deliveryLayout.setOnClickListener(this);


        btnGoToMenu = (Button) findViewById(R.id.act_request_go_to_menu_btn);

        if(isInvokedFromOrgInfoActivity){
            btnGoToMenu.setOnClickListener(this);
            btnGoToMenu.setVisibility(View.VISIBLE);
        } else{
            btnGoToMenu.setOnClickListener(null);
            btnGoToMenu.setVisibility(View.GONE);
        }


        btnInviteFriend = (Button) findViewById(R.id.act_request_invite_friend_btn);
        orgNameText = (TextView) findViewById(R.id.act_request_org_name);
        orgNameText.setText(OrgInfoActivity.getCurrentOrgVO().name);

        peopleSpinner = (RelativeLayout)findViewById(R.id.request_people_spinner);
        leftPeopleBtn = (ImageButton) peopleSpinner.findViewById(R.id.request_spinner_left_arrow_btn);
        rightPeopleBtn = (ImageButton) peopleSpinner.findViewById(R.id.request_spinner_right_arrow_btn);
        peopleTitle = (TextView) peopleSpinner.findViewById(R.id.request_spinner_title_text);
        peopleFooter = (TextView) peopleSpinner.findViewById(R.id.request_spinner_bottom_text);
        peoplePager = (ViewPager) peopleSpinner.findViewById(R.id.request_spinner_pager);
        peoplePager.setId(1);
        peopleTitle.setText("People");
        peopleFooter.setVisibility(View.GONE);
        peoplePagerAdapter = new PeopleAdapter(getSupportFragmentManager());
        peoplePager.setAdapter(peoplePagerAdapter);


        dateSpinner = (RelativeLayout)findViewById(R.id.request_date_spinner);
        leftDateBtn = (ImageButton) dateSpinner.findViewById(R.id.request_spinner_left_arrow_btn);
        rightDateBtn = (ImageButton) dateSpinner.findViewById(R.id.request_spinner_right_arrow_btn);
        dateTitle = (TextView) dateSpinner.findViewById(R.id.request_spinner_title_text);
        dateFooter = (TextView) dateSpinner.findViewById(R.id.request_spinner_bottom_text);
        dateTitle.setText("Date");
        dateFooter.setVisibility(View.GONE);
        datePager = (ViewPager) dateSpinner.findViewById(R.id.request_spinner_pager);
        datePager.setId(2);
        datePagerAdapter = new DateAdapter(getSupportFragmentManager());
        datePager.setAdapter(datePagerAdapter);


        earliestSpinner = (RelativeLayout)findViewById(R.id.request_earliest_spinner);
        leftEarliestBtn = (ImageButton) earliestSpinner.findViewById(R.id.request_spinner_left_arrow_btn);
        rightEarliestBtn = (ImageButton) earliestSpinner.findViewById(R.id.request_spinner_right_arrow_btn);
        earliestTitle = (TextView) earliestSpinner.findViewById(R.id.request_spinner_title_text);
        earliesFooter = (TextView) earliestSpinner.findViewById(R.id.request_spinner_bottom_text);
        earliestTitle.setVisibility(View.VISIBLE);
        earliestTitle.setText("Earliest");
        earliesFooter.setVisibility(View.GONE);
        earliestPager = (ViewPager) earliestSpinner.findViewById(R.id.request_spinner_pager);
        earliestPager.setId(3);
        earliestPagerAdapter = new EarliestTimeAdapter(getSupportFragmentManager());
        earliestPager.setAdapter(earliestPagerAdapter);


        latestSpinner = (RelativeLayout)findViewById(R.id.request_latest_spinner);
        leftLatestBtn = (ImageButton) latestSpinner.findViewById(R.id.request_spinner_left_arrow_btn);
        rightLatestBtn = (ImageButton) latestSpinner.findViewById(R.id.request_spinner_right_arrow_btn);
        latestTitle = (TextView) latestSpinner.findViewById(R.id.request_spinner_title_text);
        latestFooter = (TextView) latestSpinner.findViewById(R.id.request_spinner_bottom_text);
        latestTitle.setVisibility(View.VISIBLE);
        latestTitle.setText("Latest");
        latestFooter.setVisibility(View.GONE);
        latestPager = (ViewPager) latestSpinner.findViewById(R.id.request_spinner_pager);
        latestPager.setId(4);
        latestPagerAdapter = new LatestTimeAdapter(getSupportFragmentManager());
        latestPager.setAdapter(latestPagerAdapter);


        peoplePager.setCurrentItem(OrderService.getRequestPeopleCount() - 1);
        peoplePager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                ArrayList<Integer> peopleData = OrderService.getPeopleData();
                OrderService.setRequestPeopleCount(peopleData.get(position));
                updatePeopleSpinnerButtons();
            }
        });


        datePager.setCurrentItem(OrderService.dateData.indexOf(OrderService.getRequestDate()));
        datePager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                OrderService.setRequestDate(OrderService.dateData.get(position));
                updateDateSpinnerButtons();
            }
        });

        earliestPager.setCurrentItem(OrderService.timeData.indexOf(OrderService.getRequestEarliestDateTime()));
        earliestPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                OrderService.setRequestEarliestDateTime(OrderService.timeData.get(position));
                int nextPos = position + 1;
                latestPager.setCurrentItem(nextPos, true);

                updateEarliestSpinnerButtons();
            }
        });

        latestPager.setCurrentItem(OrderService.timeData.indexOf(OrderService.getRequestLatestDateTime()));
        latestPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                OrderService.setRequestLatestDateTime(OrderService.timeData.get(position));

                int prevPos = position - 1;
                earliestPager.setCurrentItem(prevPos, true);

                updateLatestSpinnerButtons();
            }
        });

        updatePeopleSpinnerButtons();
        updateDateSpinnerButtons();
        updateEarliestSpinnerButtons();
        updateLatestSpinnerButtons();

        bus.register(this);

        if(DemoService.getIsDemoMode()){
            int image = OrgInfoActivity.getCurrentOrgVO().header_image;
            Picasso.with(this)
                    .load(image)
                    .placeholder(R.drawable.org_cell_bg)
                    .fit()
                    .centerCrop()
                    .into(headerImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        @Override
                        public void onError() {

                        }
                    });
        }
        else{
            String imageURL = ApiService.getOrgImageURL(OrgInfoActivity.getCurrentOrgVO().header_image);
            Picasso.with(this)
                    .load(imageURL)
                    .placeholder(R.drawable.org_cell_bg)
                    .fit()
                    .centerCrop()
                    .into(headerImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        @Override
                        public void onError() {

                        }
                    });
        }
    }

    private void updatePeopleSpinnerButtons(){
        leftPeopleBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gold));
        leftPeopleBtn.setOnClickListener(this);
        rightPeopleBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gold));
        rightPeopleBtn.setOnClickListener(this);

        if(peoplePager.getCurrentItem() < 1){
            leftPeopleBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            leftPeopleBtn.setOnClickListener(null);
        }
        if(peoplePager.getCurrentItem() >= (OrderService.getPeopleData().size() - 1)){
            rightPeopleBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            rightPeopleBtn.setOnClickListener(null);
        }
    }

    private void updateDateSpinnerButtons(){
        leftDateBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gold));
        leftDateBtn.setOnClickListener(this);
        rightDateBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gold));
        rightDateBtn.setOnClickListener(this);

        if(datePager.getCurrentItem() < 1){
            leftDateBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            leftDateBtn.setOnClickListener(null);
        }
        if(datePager.getCurrentItem() >= (OrderService.dateData.size() - 1)){
            rightDateBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            rightDateBtn.setOnClickListener(null);
        }
    }

    private void updateEarliestSpinnerButtons(){
        leftEarliestBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gold));
        leftEarliestBtn.setOnClickListener(this);
        rightEarliestBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gold));
        rightEarliestBtn.setOnClickListener(this);

        if(earliestPager.getCurrentItem() < 1){
            leftEarliestBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            leftEarliestBtn.setOnClickListener(null);
        }
        if(earliestPager.getCurrentItem() >= (OrderService.timeData.size() - 2)){
            rightEarliestBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            rightEarliestBtn.setOnClickListener(null);
        }
    }

    private void updateLatestSpinnerButtons(){
        leftLatestBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gold));
        leftLatestBtn.setOnClickListener(this);
        rightLatestBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gold));
        rightLatestBtn.setOnClickListener(this);

        if(latestPager.getCurrentItem() < 2){
            leftLatestBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            leftLatestBtn.setOnClickListener(null);
        }
        if(latestPager.getCurrentItem() >= (OrderService.timeData.size() - 1)){
            rightLatestBtn.setColorFilter(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            rightLatestBtn.setOnClickListener(null);
        }
    }

    @Override
    public void onClick(final View v)
    {
        if(isInvokedFromOrgInfoActivity){
            if(v == btnGoToMenu){
                if(DemoService.getIsDemoMode()){
                    MenuVO menuVO = (MenuVO)DemoService.getDemoVO(this, DemoService.DEMO_MENU_VO_PATH);
                    Intent intent = new Intent(RequestActivity.this, OrgMenuActivity.class);
                    OrgMenuActivity.currentMenuVO = menuVO;
                    RequestActivity.this.startActivity(intent);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    RequestActivity.this.finish();
                }
                else{
                    if(!Helper.isConnectionAvailable(RequestActivity.this)){
                        Helper.showAlert(RequestActivity.this, R.string.alert_no_connection_title, R.string.alert_no_connection_message);
                    }
                    else{
                        Helper.showLoadingDialog(RequestActivity.this, "", "");

                        ApiService.getMenuByOrgID(OrgInfoActivity.getCurrentOrgVO().object_id, new ApiService.ApiResponse() {
                            @Override
                            public void response(Boolean isSuccess, Object payload) {
                                Helper.dismissLoadingDialog();
                                if(isSuccess){
                                    MenuVO menuVO = (MenuVO)payload;
                                    Intent intent = new Intent(RequestActivity.this, OrgMenuActivity.class);
                                    OrgMenuActivity.currentMenuVO = menuVO;
                                    RequestActivity.this.startActivity(intent);
                                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                                    RequestActivity.this.finish();
                                }
                            }
                        });
                    }
                }

                return;
            }
        }

        if(v == restaurantLayout){
            OrderService.setRequestOrderType(OrderService.ORDER_TYPE_RESTAURANT);
            updateRequest();
        } else if(v == yourselfLayout){
            OrderService.setRequestOrderType(OrderService.ORDER_TYPE_YOURSELF);
            updateRequest();
        } else if(v == deliveryLayout){
            OrderService.setRequestOrderType(OrderService.ORDER_TYPE_DELIVERY);
            updateRequest();
        }

        if(v == leftPeopleBtn){
            peoplePager.setCurrentItem(peoplePager.getCurrentItem() - 1, true);
        } else if(v == rightPeopleBtn){
            peoplePager.setCurrentItem(peoplePager.getCurrentItem() + 1, true);
        }

        if(v == leftDateBtn){
            datePager.setCurrentItem(datePager.getCurrentItem() - 1, true);
        } else if(v == rightDateBtn){
            datePager.setCurrentItem(datePager.getCurrentItem() + 1, true);
        }

        if(v == leftEarliestBtn){
            earliestPager.setCurrentItem(earliestPager.getCurrentItem() - 1, true);
        } else if(v == rightEarliestBtn){
            earliestPager.setCurrentItem(earliestPager.getCurrentItem() + 1, true);
        }

        if(v == leftLatestBtn){
            latestPager.setCurrentItem(latestPager.getCurrentItem() - 1, true);
        } else if(v == rightLatestBtn){
            latestPager.setCurrentItem(latestPager.getCurrentItem() + 1, true);
        }
    }

    @Subscribe
    public void onEvent(OrderService.OrderUpdate event){
        updateRequest();
    }

    @Override
    protected void onStart() {
        super.onStart();

        updateRequest();
    }

    private void updateRequest(){

        if(OrderService.getRequestOrderType() == OrderService.ORDER_TYPE_RESTAURANT){
            restaurantIcon.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gold));
            restaurantText.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gold));
            yourselfIcon.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            yourselfText.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            deliveryIcon.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            deliveryText.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gray));

            peopleSpinner.setVisibility(View.VISIBLE);
            act_request_line_2.setVisibility(View.VISIBLE);
            btnInviteFriend.setVisibility(View.VISIBLE);
        }
        else if(OrderService.getRequestOrderType() == OrderService.ORDER_TYPE_YOURSELF){
            restaurantIcon.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            restaurantText.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            yourselfIcon.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gold));
            yourselfText.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gold));
            deliveryIcon.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            deliveryText.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gray));

            peopleSpinner.setVisibility(View.GONE);
            act_request_line_2.setVisibility(View.GONE);
            btnInviteFriend.setVisibility(View.GONE);
        }
        else{
            restaurantIcon.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            restaurantText.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            yourselfIcon.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            yourselfText.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gray));
            deliveryIcon.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gold));
            deliveryText.setTextColor(ContextCompat.getColor(RequestActivity.this, R.color.gold));

            peopleSpinner.setVisibility(View.GONE);
            act_request_line_2.setVisibility(View.GONE);
            btnInviteFriend.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }




    public static class PeopleAdapter extends FragmentStatePagerAdapter {
        public PeopleAdapter(FragmentManager fm) {super(fm);}
        @Override
        public Fragment getItem(int i) {
            SpinnerPage fragment = new SpinnerPage();
            ArrayList<Integer> peopleData = OrderService.getPeopleData();
            fragment.text = String.valueOf(peopleData.get(i));
            return fragment;
        }
        @Override
        public int getCount() {
            return OrderService.getPeopleData().size();
        }
        @Override
        public int getItemPosition(Object object) {return POSITION_NONE;}
    }

    public static class DateAdapter extends FragmentStatePagerAdapter {
        public DateAdapter(FragmentManager fm) {super(fm);}
        @Override
        public Fragment getItem(int i) {
            SpinnerPage fragment = new SpinnerPage();
            Date date = OrderService.dateData.get(i);
            DateFormat dateFormat = SimpleDateFormat.getDateInstance();
            fragment.text = dateFormat.format(date);
            return fragment;
        }
        @Override
        public int getCount() {return OrderService.dateData.size();}
        @Override
        public int getItemPosition(Object object) {return POSITION_NONE;}
    }

    public static class EarliestTimeAdapter extends FragmentStatePagerAdapter {
        public EarliestTimeAdapter(FragmentManager fm) {super(fm);}
        @Override
        public Fragment getItem(int i) {
            SpinnerPage fragment = new SpinnerPage();
            OrderService.DateTime dateTime = OrderService.timeData.get(i);
            fragment.text = dateTime.getTwoLineString();
            return fragment;
        }
        @Override
        public int getCount() {return OrderService.timeData.size();}
        @Override
        public int getItemPosition(Object object) {return POSITION_NONE;}
    }

    public static class LatestTimeAdapter extends FragmentStatePagerAdapter {
        public LatestTimeAdapter(FragmentManager fm) {super(fm);}
        @Override
        public Fragment getItem(int i) {
            SpinnerPage fragment = new SpinnerPage();
            OrderService.DateTime dateTime = OrderService.timeData.get(i);
            fragment.text = dateTime.getTwoLineString();
            return fragment;
        }
        @Override
        public int getCount() {return OrderService.timeData.size();}
        @Override
        public int getItemPosition(Object object) {return POSITION_NONE;}
    }

    public static class SpinnerPage extends Fragment {

        public String text = "";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.request_spinner_page, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.request_spinner_page_text);
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);

            return rootView;
        }

    }

}
