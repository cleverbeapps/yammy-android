package com.gotop.yammy.drawer.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.drawer.vo.OrgTypeVO;
import com.gotop.yammy.service.FilterService;

import java.util.ArrayList;
import java.util.List;

public class FilterOrgTypesDialog extends AppCompatDialog {

    private RecyclerView listview;
    FilterOrgTypesListAdapter adapter;
    List<OrgTypeVO> orgTypeVOs;

    public FilterOrgTypesDialog(Context context, List<OrgTypeVO> orgTypeVOs) {
        super(context);
        this.orgTypeVOs = orgTypeVOs;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.filter_dialog);
        getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        ViewCompat.setElevation(getWindow().getDecorView(), 3);
        listview = (RecyclerView) findViewById(R.id.filter_dialog_list);

        TextView title = (TextView)findViewById(R.id.filter_dialog_title);
        title.setText("Place Types:");

        List<OrgTypeVO> prevSelectedOrgTypes = FilterService.getOrgTypeFilter();

        adapter = new FilterOrgTypesListAdapter(getContext(), orgTypeVOs, prevSelectedOrgTypes);
        listview.setLayoutManager(new LinearLayoutManager(getContext()));
        listview.setAdapter(adapter);

        final GestureDetector gestureDetector =
                new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        return true;
                    }
                });

        listview.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());
                int position = rv.getChildAdapterPosition(child);
                if(gestureDetector.onTouchEvent(e)){
                    adapter.selectOrgType(position);
                }
                return false;
            }
            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {}
            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
        });

        Button applyButton = (Button)findViewById(R.id.filter_dialog_list_apply_btn);
        if(applyButton != null){
            applyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(adapter != null){
                        FilterService.set_orgTypeFilter(adapter.selectedOrgTypes);
                    }
                    else{
                        FilterService.set_orgTypeFilter(null);
                    }
                    dismiss();
                }
            });
        }
    }



    public static class FilterOrgTypesListAdapter extends RecyclerView.Adapter<FilterOrgTypesListAdapter.OrgTypeCell> {
        private Context mContext;
        List<OrgTypeVO> dataProvider = new ArrayList<OrgTypeVO>();
        List<OrgTypeVO> selectedOrgTypes = new ArrayList<OrgTypeVO>();

        public FilterOrgTypesListAdapter(Context context, List<OrgTypeVO> dataProvider, List<OrgTypeVO> selectedOrgTypes){

            mContext = context;
            this.dataProvider = dataProvider;
            this.selectedOrgTypes.addAll(selectedOrgTypes);
        }

        public void selectOrgType(int position){
            if(position == 0){
                this.selectedOrgTypes.clear();
                notifyDataSetChanged();
            }
            else {
                OrgTypeVO OrgTypeVO = dataProvider.get(position - 1);
                for (int i = 0; i < selectedOrgTypes.size(); i++) {
                    OrgTypeVO selectedOrgType = selectedOrgTypes.get(i);
                    if(OrgTypeVO.id == selectedOrgType.id){
                        selectedOrgTypes.remove(i);
                        notifyDataSetChanged();
                        return;
                    }
                }
                selectedOrgTypes.add(OrgTypeVO);
                notifyDataSetChanged();
            }
        }

        @Override
        public OrgTypeCell onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_dialog_row,parent,false);
            OrgTypeCell vhItem = new OrgTypeCell(v,viewType);
            return vhItem;
        }

        @Override
        public void onBindViewHolder(final OrgTypeCell holder, int position) {

            if(holder.type == OrgTypeCell.ANY_TYPE){
                holder.textView.setText("Any Place Type");
                if(selectedOrgTypes.size() == 0){
                    holder.textView.setTextColor(ContextCompat.getColor(mContext, R.color.gold));
                    holder.imageView.setColorFilter(ContextCompat.getColor(mContext, R.color.dark_blue));
                    holder.imageView.setVisibility(View.VISIBLE);
                }
                else{
                    holder.textView.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                    holder.imageView.setVisibility(View.INVISIBLE);
                }
            }
            else{
                OrgTypeVO currentOrgType = dataProvider.get(position - 1);
                holder.textView.setText(currentOrgType.name);
                for (int i = 0; i < selectedOrgTypes.size(); i++) {
                    OrgTypeVO orgType = selectedOrgTypes.get(i);
                    if(orgType.id == currentOrgType.id){
                        holder.textView.setTextColor(ContextCompat.getColor(mContext, R.color.gold));
                        holder.imageView.setColorFilter(ContextCompat.getColor(mContext, R.color.dark_blue));
                        holder.imageView.setVisibility(View.VISIBLE);
                        return;
                    }
                }
                holder.textView.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                holder.imageView.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
            super.onDetachedFromRecyclerView(recyclerView);
        }

        @Override
        public int getItemCount() {
            return dataProvider.size() + 1;
        }

        @Override
        public int getItemViewType(int position) {
            if(position == 0){
                return OrgTypeCell.ANY_TYPE;
            }
            return OrgTypeCell.PLACE_TYPE;
        }

        public static class OrgTypeCell extends RecyclerView.ViewHolder{

            static public int ANY_TYPE = 0;
            static public int PLACE_TYPE = 1;

            public int type;

            TextView textView;
            ImageView imageView;

            public OrgTypeCell(View itemView, int type) {
                super(itemView);

                this.type = type;
                textView = (TextView) itemView.findViewById(R.id.filter_dialog_row_text);
                imageView = (ImageView) itemView.findViewById(R.id.filter_dialog_row_done_icon);
            }
        }
    }
}
