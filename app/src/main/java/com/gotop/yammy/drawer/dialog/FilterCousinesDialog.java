package com.gotop.yammy.drawer.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.drawer.vo.CuisineVO;
import com.gotop.yammy.service.FilterService;

import java.util.ArrayList;
import java.util.List;

public class FilterCousinesDialog extends AppCompatDialog {

    private RecyclerView listview;
    FilterCuisineListAdapter adapter;
    List<CuisineVO> cuisineVOs;

    public FilterCousinesDialog(Context context, List<CuisineVO> cuisineVOs) {
        super(context);
        this.cuisineVOs = cuisineVOs;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.filter_dialog);
        getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        ViewCompat.setElevation(getWindow().getDecorView(), 3);
        listview = (RecyclerView) findViewById(R.id.filter_dialog_list);

        TextView title = (TextView)findViewById(R.id.filter_dialog_title);
        title.setText("Cuisines:");

        List<CuisineVO> prevSelectedCuisines = FilterService.getCuisineFilter();

        adapter = new FilterCuisineListAdapter(getContext(), cuisineVOs, prevSelectedCuisines);
        listview.setLayoutManager(new LinearLayoutManager(getContext()));
        listview.setAdapter(adapter);

        final GestureDetector gestureDetector =
                new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        return true;
                    }
                });

        listview.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());
                int position = rv.getChildAdapterPosition(child);
                if(gestureDetector.onTouchEvent(e)){
                    adapter.selectCuisineAtPosition(position);
                }
                return false;
            }
            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {}
            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
        });

        Button applyButton = (Button)findViewById(R.id.filter_dialog_list_apply_btn);
        if(applyButton != null){
            applyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(adapter != null){
                        FilterService.set_cuisineFilter(adapter.selectedCuisines);
                    }
                    else{
                        FilterService.set_cuisineFilter(null);
                    }
                    dismiss();
                }
            });
        }
    }



    public static class FilterCuisineListAdapter extends RecyclerView.Adapter<FilterCuisineListAdapter.CuisineCell> {
        private Context mContext;
        List<CuisineVO> dataProvider = new ArrayList<CuisineVO>();
        List<CuisineVO> selectedCuisines = new ArrayList<CuisineVO>();

        public FilterCuisineListAdapter(Context context, List<CuisineVO> dataProvider, List<CuisineVO> selectedCuisines){

            mContext = context;
            this.dataProvider = dataProvider;
            this.selectedCuisines.addAll(selectedCuisines);
        }

        public void selectCuisineAtPosition(int position){
            if(position == 0){
                this.selectedCuisines.clear();
                notifyDataSetChanged();
            }
            else {
                CuisineVO cuisineVO = dataProvider.get(position - 1);
                for (int i = 0; i < selectedCuisines.size(); i++) {
                    CuisineVO selectedCuisine = selectedCuisines.get(i);
                    if(cuisineVO.id == selectedCuisine.id){
                        selectedCuisines.remove(i);
                        notifyDataSetChanged();
                        return;
                    }
                }
                selectedCuisines.add(cuisineVO);
                notifyDataSetChanged();
            }
        }

        @Override
        public CuisineCell onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_dialog_row,parent,false);
            CuisineCell vhItem = new CuisineCell(v,viewType);
            return vhItem;
        }

        @Override
        public void onBindViewHolder(final CuisineCell holder, int position) {

            if(holder.type == CuisineCell.ANY_CUISINE){
                holder.textView.setText("Any Cuisine");
                if(selectedCuisines.size() == 0){
                    holder.textView.setTextColor(ContextCompat.getColor(mContext, R.color.gold));
                    holder.imageView.setColorFilter(ContextCompat.getColor(mContext, R.color.dark_blue));
                    holder.imageView.setVisibility(View.VISIBLE);
                }
                else{
                    holder.textView.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                    holder.imageView.setVisibility(View.INVISIBLE);
                }
            }
            else{
                CuisineVO currentCuisine = dataProvider.get(position - 1);
                holder.textView.setText(currentCuisine.name);
                for (int i = 0; i < selectedCuisines.size(); i++) {
                    CuisineVO cuisine = selectedCuisines.get(i);
                    if(cuisine.id == currentCuisine.id){
                        holder.textView.setTextColor(ContextCompat.getColor(mContext, R.color.gold));
                        holder.imageView.setColorFilter(ContextCompat.getColor(mContext, R.color.dark_blue));
                        holder.imageView.setVisibility(View.VISIBLE);
                        return;
                    }
                }
                holder.textView.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                holder.imageView.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
            super.onDetachedFromRecyclerView(recyclerView);
        }

        @Override
        public int getItemCount() {
            return dataProvider.size() + 1;
        }

        @Override
        public int getItemViewType(int position) {
            if(position == 0){
                return CuisineCell.ANY_CUISINE;
            }
            return CuisineCell.CUISINE;
        }

        public static class CuisineCell extends RecyclerView.ViewHolder{

            static public int ANY_CUISINE = 0;
            static public int CUISINE = 1;

            public int type;

            TextView textView;
            ImageView imageView;

            public CuisineCell(View itemView, int type) {
                super(itemView);

                this.type = type;
                textView = (TextView) itemView.findViewById(R.id.filter_dialog_row_text);
                imageView = (ImageView) itemView.findViewById(R.id.filter_dialog_row_done_icon);
            }
        }
    }
}
