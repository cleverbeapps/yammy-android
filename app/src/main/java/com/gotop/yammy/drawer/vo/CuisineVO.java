package com.gotop.yammy.drawer.vo;

import com.orm.SugarRecord;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CuisineVO {

    public int id = 0;
    public String name = "";

    public CuisineVO(){}

    static public CuisineVO fromJSON(JSONObject json){

        CuisineVO cuisineVO = new CuisineVO();
        try {
            cuisineVO.id = json.getInt("id");
            cuisineVO.name = json.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cuisineVO;
    }

//    static public List<CuisineVO> getAllCuisines(){
//        List<CuisineVO> allCuisines = SugarRecord.listAll(CuisineVO.class);
//        return allCuisines;
//    }
//
//    static public void saveAllCuisines(List<CuisineVO> cuisineVOs){
//
//        //SugarRecord.deleteAll(CuisineVO.class);
//        if(cuisineVOs != null){
//            for (int i = 0; i < cuisineVOs.size(); i++) {
//                CuisineVO cuisineVO = cuisineVOs.get(i);
//                cuisineVO.save();
//            }
//        }
//    }
}
