package com.gotop.yammy.drawer;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gotop.yammy.InitialActivity;
import com.gotop.yammy.R;
import com.gotop.yammy.drawer.dialog.OrderSuccessDialog;
import com.gotop.yammy.service.ApiService;
import com.gotop.yammy.service.DemoService;
import com.gotop.yammy.service.OrderService;
import com.gotop.yammy.service.UserService;
import com.gotop.yammy.utils.Helper;
import com.gotop.yammy.utils.IconsTextView;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ReviewRequestActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView toolbarTitle;
    private ImageButton backButton;
    private Button btnSendRequest;
    private Button btnCheckIn;
    private TextView orgNameText;

    private IconsTextView iconRequestType;
    private TextView iconRequestTypeText;

    private TextView dateText;
    private TextView timeText;
    private TextInputEditText commentText;
    private TextInputLayout commentTextLayout;

    private TextView subtotalCountText;
    private TextView totalCountText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_request);

        toolbar = (Toolbar)findViewById(R.id.review_request_toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_back_title);
        toolbarTitle.setText("Review Your Request");
        backButton = (ImageButton) toolbar.findViewById(R.id.toolbar_back_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        final Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(Color.LTGRAY);
        backButton.setColorFilter(ContextCompat.getColor(ReviewRequestActivity.this, R.color.gold));
        toolbarTitle.setHintTextColor(ContextCompat.getColor(ReviewRequestActivity.this, R.color.gold));
        toolbar.getBackground().setAlpha(255);    // // 20%  255 = 100%

        final ImageView headerImage = (ImageView) findViewById(R.id.review_request_header_image);
        final ProgressBar progressBar = (ProgressBar)findViewById(R.id.review_request_header_image_progress);

        btnSendRequest = (Button) findViewById(R.id.review_request_send_request_btn);
        btnCheckIn = (Button) findViewById(R.id.review_request_check_in_btn);

        iconRequestType = (IconsTextView)findViewById(R.id.review_request_type_icon);
        iconRequestTypeText = (TextView) findViewById(R.id.review_request_type_text);
        dateText = (TextView) findViewById(R.id.review_request_type_date_text);
        timeText = (TextView) findViewById(R.id.review_request_type_time_text);
        commentText = (TextInputEditText) findViewById(R.id.review_request_type_comment_layout_text);
        commentTextLayout = (TextInputLayout) findViewById(R.id.review_request_type_comment_layout);
        subtotalCountText = (TextView) findViewById(R.id.review_request_order_subtotal_count);
        totalCountText = (TextView) findViewById(R.id.review_request_order_total_count);
        orgNameText = (TextView) findViewById(R.id.review_request_org_name);

        commentTextLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestFocus(commentText);
            }
        });

        btnSendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int validationCode = OrderService.getValidationResult();
                if(!DemoService.getIsDemoMode()){
                    if(validationCode == OrderService.VALID){

                        UserService.UserCreditCardVO userCreditCardVO = UserService.getUserCreditCardVO();
                        if(userCreditCardVO != null){

                            // TODO ACTUAL PAYMENT HERE
                            OrderSuccessDialog orderSuccessDialog = new OrderSuccessDialog(ReviewRequestActivity.this, new OrderSuccessDialog.OrderSuccessDialogCallback() {
                                @Override
                                public void onClose() {
                                    startActivity(new Intent(ReviewRequestActivity.this, DrawerActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                }
                            });
                            orderSuccessDialog.show();
                        }
                        else{
                            CardActivity.isInvokedFromOrder = true;
                            startActivity(new Intent(getApplicationContext(), CardActivity.class));
                            overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        }
                    }
                    else{
                        String invalidDescription = OrderService.getValidationDescription(validationCode);
                        Helper.showAlert(ReviewRequestActivity.this, "Invalid Order", invalidDescription);
                    }
                }
                else{
                    // DEMO MODE
                    OrderSuccessDialog orderSuccessDialog = new OrderSuccessDialog(ReviewRequestActivity.this, new OrderSuccessDialog.OrderSuccessDialogCallback() {
                        @Override
                        public void onClose() {
                            startActivity(new Intent(ReviewRequestActivity.this, InitialActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        }
                    });
                    orderSuccessDialog.show();
                }
            }
        });

        if(DemoService.getIsDemoMode()){
            int image = OrgInfoActivity.getCurrentOrgVO().header_image;
            Picasso.with(this)
                    .load(image)
                    .placeholder(R.drawable.org_cell_bg)
                    .fit()
                    .centerCrop()
                    .into(headerImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        @Override
                        public void onError() {

                        }
                    });
        }
        else{
            String imageURL = ApiService.getOrgImageURL(OrgInfoActivity.getCurrentOrgVO().header_image);
            Picasso.with(this)
                    .load(imageURL)
                    .placeholder(R.drawable.org_cell_bg)
                    .fit()
                    .centerCrop()
                    .into(headerImage, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        @Override
                        public void onError() {

                        }
                    });
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

    @Override
    protected void onStart() {
        super.onStart();

        orgNameText.setText(OrderService.getRequestOrg().name);

        Double orderPrice = OrderService.getOrderPrice();
        Double orderPriceWithTip = OrderService.getOrderPricePlusTip(UserService.getUserVO().orderTip);

        subtotalCountText.setText("$" + String.format("%.2f", orderPrice));
        totalCountText.setText("$" + String.format("%.2f", orderPriceWithTip));

        commentText.setText(OrderService.requestComment);

        timeText.setText(OrderService.getRequestEarliestDateTime().getTwoLineString() + " - " + OrderService.getRequestLatestDateTime().getInlineString());

        DateFormat dateFormat = SimpleDateFormat.getDateInstance();
        String requestDateText = dateFormat.format(OrderService.getRequestDate());

        if(OrderService.getRequestOrderType() == OrderService.ORDER_TYPE_RESTAURANT){
            iconRequestType.setText(String.valueOf((char) 0xe917));
            iconRequestTypeText.setText("Restaurant");

            dateText.setText("Table for "+OrderService.getRequestPeopleCount()+", "+requestDateText);
        }
        else if(OrderService.getRequestOrderType() == OrderService.ORDER_TYPE_YOURSELF){
            iconRequestType.setText(String.valueOf((char) 0xe91a));
            iconRequestTypeText.setText("Yourself");

            dateText.setText(requestDateText);
        }
        else if(OrderService.getRequestOrderType() == OrderService.ORDER_TYPE_DELIVERY){
            iconRequestType.setText(String.valueOf((char) 0xe90c));
            iconRequestTypeText.setText("Delivery");

            dateText.setText(requestDateText);
        }
    }
}
