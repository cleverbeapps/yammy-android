package com.gotop.yammy.drawer;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.drawer.dialog.OrderSuccessDialog;
import com.gotop.yammy.drawer.dialog.OrderTipDialog;
import com.gotop.yammy.service.OrderService;
import com.gotop.yammy.service.UserService;
import com.gotop.yammy.utils.AnimCheckBox;
import com.gotop.yammy.utils.Helper;
import com.gotop.yammy.utils.credit_card.library.CreditCard;
import com.gotop.yammy.utils.credit_card.library.CreditCardForm;

public class CardActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView toolbarTitle;
    private ImageButton backButton;
    private Button btnSaveOrPay;
    private CreditCardForm creditCardForm;

    private LinearLayout tipLayout;
    private TextView tipText;
    private TextView paymentAmount;
    private TextView paymentAmountTotal;
    private AnimCheckBox rememberMeCheckbox;

    static public Boolean isInvokedFromOrder = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        toolbar = (Toolbar)findViewById(R.id.card_toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_back_title);
        toolbarTitle.setText("Payment Details");
        backButton = (ImageButton) toolbar.findViewById(R.id.toolbar_back_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        final Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(Color.LTGRAY);
        backButton.setColorFilter(ContextCompat.getColor(CardActivity.this, R.color.gold));
        toolbarTitle.setHintTextColor(ContextCompat.getColor(CardActivity.this, R.color.gold));
        toolbar.getBackground().setAlpha(255);

        btnSaveOrPay = (Button)findViewById(R.id.credit_card_pay_or_save_btn);
        creditCardForm = (CreditCardForm) findViewById(R.id.credit_card_edit_text);
        tipLayout = (LinearLayout) findViewById(R.id.credit_card_preset_tip_layout);
        tipText = (TextView) findViewById(R.id.card_preset_tip_text);
        paymentAmount = (TextView) findViewById(R.id.credit_card_payment_amount_count_text);
        paymentAmountTotal = (TextView) findViewById(R.id.credit_card_payment_total_count_text);
        rememberMeCheckbox = (AnimCheckBox) findViewById(R.id.card_remember_me_checkbox);

        UserService.UserCreditCardVO userCreditCardVO = UserService.getUserCreditCardVO();
        if(userCreditCardVO != null){
            creditCardForm.setCardNumber(userCreditCardVO.cardNumber, false);
            creditCardForm.setExpDate(userCreditCardVO.expDate, false);
            creditCardForm.setSecurityCode(userCreditCardVO.securityCode, true);
        }

        if(isInvokedFromOrder){
            updateOrderPrice();
        }
        else{
            TextView paymentAmountText = (TextView)findViewById(R.id.credit_card_payment_amount_text);
            TextView paymentAmountTotalText = (TextView)findViewById(R.id.credit_card_payment_total_text);
            TextView rememberMeText = (TextView)findViewById(R.id.card_remember_me_text);
            paymentAmountText.setVisibility(View.GONE);
            paymentAmountTotalText.setVisibility(View.GONE);
            rememberMeText.setVisibility(View.GONE);
            paymentAmount.setVisibility(View.GONE);
            paymentAmountTotal.setVisibility(View.GONE);
            rememberMeCheckbox.setVisibility(View.GONE);
            btnSaveOrPay.setText("SAVE CARD");
        }

        String tip = String.valueOf(UserService.getUserVO().orderTip) + "%";
        tipText.setText(tip);
        tipLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentUserTip = UserService.getUserVO().orderTip;
                OrderTipDialog orderTipDialog = new OrderTipDialog(CardActivity.this, currentUserTip, new OrderTipDialog.OrderTipDialogCallback() {
                    @Override
                    public void onSelect(int newTip) {
                        UserService.getUserVO().orderTip = newTip;
                        UserService.saveUserData();
                        updateOrderPrice();
                        String newTipString = String.valueOf(newTip) + "%";
                        tipText.setText(newTipString);
                    }
                });
                orderTipDialog.show();
            }
        });

        btnSaveOrPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(creditCardForm.isCreditCardValid())
                {
                    if(isInvokedFromOrder){
                        if(rememberMeCheckbox.isChecked()){
                            CreditCard creditCard = creditCardForm.getCreditCard();
                            UserService.saveCreditCard(creditCard.getCardNumber(), creditCard.getExpDate(), creditCard.getSecurityCode());
                        }

                        // TODO ACTUAL PAYMENT HERE
                        OrderSuccessDialog orderSuccessDialog = new OrderSuccessDialog(CardActivity.this, new OrderSuccessDialog.OrderSuccessDialogCallback() {
                            @Override
                            public void onClose() {
                                startActivity(new Intent(CardActivity.this, DrawerActivity.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            }
                        });
                        orderSuccessDialog.show();
                    }
                    else{
                        CreditCard creditCard = creditCardForm.getCreditCard();
                        UserService.saveCreditCard(creditCard.getCardNumber(), creditCard.getExpDate(), creditCard.getSecurityCode());
                        onBackPressed();
                    }
                }
                else
                {
                    Helper.showAlert(CardActivity.this, "Invalid Card Credentials", "Please, enter correct credit card credentials!");
                }
            }
        });
    }

    private void updateOrderPrice(){
        Double orderPrice = OrderService.getOrderPrice();
        paymentAmount.setText("$" + String.format("%.2f", orderPrice));
        Double orderPriceWithTip = OrderService.getOrderPricePlusTip(UserService.getUserVO().orderTip);
        paymentAmountTotal.setText("$" + String.format("%.2f", orderPriceWithTip));
        rememberMeCheckbox.setChecked(false);
        btnSaveOrPay.setText("PAY");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }
}
