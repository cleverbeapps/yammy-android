package com.gotop.yammy.drawer.vo;

import com.orm.SugarRecord;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class OrgTypeVO{

    public String name;
    public int id;

    public OrgTypeVO(){}

    static public OrgTypeVO fromJSON(JSONObject json){

        OrgTypeVO orgTypeVO = new OrgTypeVO();
        try {
            orgTypeVO.id = json.getInt("id");
            orgTypeVO.name = json.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return orgTypeVO;
    }

//    static public List<OrgTypeVO> getAllOrgTypes(){
//        List<OrgTypeVO> allOrgTypes = SugarRecord.listAll(OrgTypeVO.class);
//        return allOrgTypes;
//    }
//
//    static public void saveAllOrgTypes(List<OrgTypeVO> orgTypeVOs){
//
//        SugarRecord.deleteAll(OrgTypeVO.class);
//        if(orgTypeVOs != null){
//            for (int i = 0; i < orgTypeVOs.size(); i++) {
//                OrgTypeVO orgTypeVO = orgTypeVOs.get(i);
//                orgTypeVO.save();
//            }
//        }
//    }
}
