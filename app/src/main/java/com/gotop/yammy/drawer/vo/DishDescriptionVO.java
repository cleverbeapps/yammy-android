package com.gotop.yammy.drawer.vo;

import android.content.Context;

import com.gotop.yammy.utils.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DishDescriptionVO {

    public JSONArray allergens;
    public JSONArray ingridients;
    public JSONArray types;

    public String bulk = "";
    public int calorific = 0;
    public String description = "";
    public int id = 0;
    public String name = "";
    public Double price = 0.0;
    public String video = "";
    public int weight = 0;
    public Boolean isFavorite = false;
    public ArrayList<Integer> images = new ArrayList<Integer>();

    static public DishDescriptionVO fromJSON(Context context, JSONObject json, Boolean isDemo){

        DishDescriptionVO dishDescriptionVO = new DishDescriptionVO();
        try {
            dishDescriptionVO.allergens = json.getJSONArray("allergens");
            dishDescriptionVO.ingridients = json.getJSONArray("ingridients");
            dishDescriptionVO.types = json.getJSONArray("types");

            dishDescriptionVO.bulk = json.getString("bulk");
            dishDescriptionVO.description = json.getString("description");
            dishDescriptionVO.calorific = json.getInt("calorific");
            dishDescriptionVO.id = json.getInt("id");
            dishDescriptionVO.name = json.getString("name");
            dishDescriptionVO.price = json.getDouble("price");
            dishDescriptionVO.isFavorite = json.getBoolean("isFavorite");

            if(!isDemo){
                JSONArray images = json.getJSONArray("images");
                for (int index = 0; index < images.length(); index++) {
                    int image = images.getInt(index);
                    dishDescriptionVO.images.add(image);
                }
            }
            else{
                JSONArray demoImageNames = json.getJSONArray("demoImages");
                for (int index = 0; index < demoImageNames.length(); index++) {
                    String imageName = demoImageNames.getString(index);
                    int imageID = Helper.getDrawableIDByName(context, imageName);
                    dishDescriptionVO.images.add(imageID);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return dishDescriptionVO;
    }
}
