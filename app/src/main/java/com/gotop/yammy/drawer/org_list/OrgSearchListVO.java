package com.gotop.yammy.drawer.org_list;

import com.gotop.yammy.drawer.vo.CuisineVO;
import com.gotop.yammy.drawer.vo.OrgTypeVO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OrgSearchListVO {

    public String address = "";
    public int createdAt = 0;
    public List<CuisineVO> cuisines = new ArrayList<CuisineVO>();
    public Double distance = 0.0;
    public int header_image = 0;
    public Double latintude = 0.0;
    public Double longitude = 0.0;
    public String name = "";
    public int object_id = 0;
    public int price = 0;
    public int rating = 0;
    public int updatedAt = 0;
    public List<OrgTypeVO> types = new ArrayList<OrgTypeVO>();

    static public OrgSearchListVO fromJSON(JSONObject jsonObject){

        OrgSearchListVO orgSearchList = new OrgSearchListVO();

        try {
            orgSearchList.address = jsonObject.getString("address");
            orgSearchList.createdAt = jsonObject.getInt("createdAt");
            orgSearchList.header_image = jsonObject.getInt("header_image");
            orgSearchList.name = jsonObject.getString("name");
            orgSearchList.object_id = jsonObject.getInt("object_id");
            orgSearchList.rating = jsonObject.getInt("rating");
            orgSearchList.updatedAt = jsonObject.getInt("updatedAt");

            orgSearchList.latintude = jsonObject.getDouble("latitude");
            orgSearchList.longitude = jsonObject.getDouble("longitude");
            orgSearchList.distance = jsonObject.getDouble("distance");

            JSONArray cousines = jsonObject.getJSONArray("cuisine");
            if(cousines != null){
                for (int index = 0; index < cousines.length(); index++) {
                    JSONObject item = cousines.getJSONObject(index);

                    int id = item.getInt("id");
                    String name = item.getString("name");
                    CuisineVO cuisineVO = new CuisineVO();
                    cuisineVO.id = id;
                    cuisineVO.name = name;
                    orgSearchList.cuisines.add(cuisineVO);
                }
            }

            JSONArray orgTypes = jsonObject.getJSONArray("type");
            if(orgTypes != null){
                for (int index = 0; index < orgTypes.length(); index++) {
                    JSONObject item = orgTypes.getJSONObject(index);

                    int id = item.getInt("id");
                    String name = item.getString("name");
                    OrgTypeVO orgType = new OrgTypeVO();
                    orgType.id = id;
                    orgType.name = name;
                    orgSearchList.types.add(orgType);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return orgSearchList;

//
//        if let orgTypes = json["type"].array{
//        for type in orgTypes{
//        let orgType = OrgTypeVO()
//        orgType.id = type["id"].intValue
//        orgType.name = type["name"].stringValue
//        orgSearchListVO.types.append(orgType)
//        }
//        }
//
//        return orgSearchListVO


    }
}
