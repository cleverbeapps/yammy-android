package com.gotop.yammy.service;


import com.gotop.yammy.drawer.vo.MenuDishVO;
import com.gotop.yammy.drawer.vo.OrgVO;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class OrderService {

    private static EventBus bus = EventBus.getDefault();
    public static class OrderUpdate{}


    final static public String ORDER_TYPE_RESTAURANT = "ORDER_TYPE_RESTAURANT";
    final static public String ORDER_TYPE_YOURSELF = "ORDER_TYPE_YOURSELF";
    final static public String ORDER_TYPE_DELIVERY = "ORDER_TYPE_DELIVERY";

    final static public String DAY_TIME_MERIDIUM_TYPE_AM = "AM";
    final static public String DAY_TIME_MERIDIUM_TYPE_PM = "PM";


    // INITIALIZE

    static public void init(){
        order = new HashMap<MenuDishVO,Integer>();
        peopleData = new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
        dateData = OrderService.createDateData();
        timeData = OrderService.createDateTimeData();

        requestPeopleCount = OrderService.getPeopleData().get(0);
        requestDate = OrderService.dateData.get(0);
        requestEarliestDateTime = OrderService.timeData.get(0);
        requestLatestDateTime = OrderService.timeData.get(1);
        requestOrderType = ORDER_TYPE_RESTAURANT;
        requestComment = "";
    }

    static private void reset(){
        order.clear();
        requestPeopleCount = OrderService.getPeopleData().get(0);
        requestDate = OrderService.dateData.get(0);
        requestEarliestDateTime = OrderService.timeData.get(0);
        requestLatestDateTime = OrderService.timeData.get(1);
        requestOrderType = ORDER_TYPE_RESTAURANT;
        requestComment = "";
    }

    public static class DateTime{
        private int hour;
        private int minutes;
        private String dateTimeMeridiumType;
        public DateTime(int hour, int minutes, String dateTimeMeridiumType){
            this.hour = hour;
            this.minutes = minutes;
            this.dateTimeMeridiumType = dateTimeMeridiumType;
        }
        public String getTwoLineString(){
            return this.hour + ":" + (this.minutes == 0 ? "00" : this.minutes) + "\n" + this.dateTimeMeridiumType;
        }
        public String getInlineString(){
            return this.hour + ":" + (this.minutes == 0 ? "00" : this.minutes) + " " + this.dateTimeMeridiumType;
        }
        public int getRealHour(){
            int realHour = this.hour;
            if(Objects.equals(this.dateTimeMeridiumType, DAY_TIME_MERIDIUM_TYPE_PM)){
                realHour = this.hour + 12;
            }
            return realHour;
        }
    }

    // ORDER VALIDATION

    final static public int VALID = 0;
    final static public int INVALID_DATE = 1;
    final static public int INVALID_ORG = 2;
    final static public int INVALID_PEOPLE_COUNT = 3;
    final static public int INVALID_DISH_COUNT = 4;
    static public int getValidationResult(){
        if(requestOrderType == ORDER_TYPE_RESTAURANT){
            if(getRequestPeopleCount() < 1){
                return INVALID_PEOPLE_COUNT;

            }
        }
        if(requestOrg == null){
            return INVALID_ORG;
        }
        if(requestDate == null || getRequestEarliestDateTime() == null || getRequestLatestDateTime() == null){
            return INVALID_DATE;
        }

        Calendar calendar = Calendar.getInstance();
        Date currentDate = calendar.getTime();

        calendar.setTime(requestDate);
        calendar.set(Calendar.HOUR, getRequestEarliestDateTime().getRealHour());
        calendar.set(Calendar.MINUTE, getRequestEarliestDateTime().minutes);
        Date requestEarliestDate = calendar.getTime();

        if(requestEarliestDate.compareTo(currentDate) < 0){
            return INVALID_DATE;
        }

        if(order.size() < 1){
            return INVALID_DISH_COUNT;
        }

        return VALID;
    }
    static public String getValidationDescription(int orderValidationResult){
        switch (orderValidationResult){
            case INVALID_DATE:
                return "Order start date should be greater than current date.";
            case INVALID_ORG:
                return "Invalid restaurant.";
            case INVALID_DISH_COUNT:
                return "Should be at least one dish in your order.";
            case INVALID_PEOPLE_COUNT:
                return "Should be at least 1 people in order for restaurant.";
        }
        return "Order is valid.";
    }

    // CURRENT REQUEST

    static public String requestComment = "";

    static private String requestOrderType = ORDER_TYPE_RESTAURANT;
    static public String getRequestOrderType(){
        return requestOrderType;
    }
    static public void setRequestOrderType(String orderType){
        requestOrderType = orderType;
        //bus.post(new OrderUpdate());
    }

    static private int requestPeopleCount;
    static public int getRequestPeopleCount(){
        return requestPeopleCount;
    }
    static public void setRequestPeopleCount(int peopleCount){
        int first = OrderService.getPeopleData().get(0);
        int last = OrderService.getPeopleData().get(OrderService.getPeopleData().size() - 1);
        requestPeopleCount = peopleCount;
        if(peopleCount > last){
            requestPeopleCount = last;
        }
        else if (peopleCount < first){
            requestPeopleCount = first;
        }
        //bus.post(new OrderUpdate());
    }
    static private Date requestDate;
    static public void setRequestDate(Date date) {
        requestDate = date;
        //bus.post(new OrderUpdate());
    }
    static public Date getRequestDate(){
        return requestDate;
    }
    static private DateTime requestEarliestDateTime;
    static public void setRequestEarliestDateTime(DateTime dateTime){
        requestEarliestDateTime = dateTime;
        //bus.post(new OrderUpdate());
    }
    static public DateTime getRequestEarliestDateTime(){
        return requestEarliestDateTime;
    }
    static private DateTime requestLatestDateTime;
    static public void setRequestLatestDateTime(DateTime dateTime){
        requestLatestDateTime = dateTime;
        //bus.post(new OrderUpdate());
    }
    static public DateTime getRequestLatestDateTime(){
        return requestLatestDateTime;
    }


    // REQUEST DATA

    static private OrgVO requestOrg;
    static public OrgVO getRequestOrg(){
        return requestOrg;
    }
    static public void setRequestOrg(OrgVO orgVO){

        reset();

        requestOrg = orgVO;

        Calendar currentCalendar = Calendar.getInstance();
        Date currentDateCalendar = currentCalendar.getTime();   // current time

        for (int i=0; i<OrderService.timeData.size(); i++){
            DateTime timeDate = OrderService.timeData.get(i);

            int timeDataHour = timeDate.getRealHour();
            int timeDataMin = timeDate.minutes;

            Calendar dateCalendar = Calendar.getInstance();
            dateCalendar.set(Calendar.HOUR, timeDataHour);
            dateCalendar.set(Calendar.MINUTE, timeDataMin);
            Date timeDateCalendar = dateCalendar.getTime();

            if(timeDateCalendar.compareTo(currentDateCalendar) > 0){
                int earliestIndex = OrderService.timeData.indexOf(timeDate);

                requestEarliestDateTime = OrderService.timeData.get(earliestIndex);

                if ((OrderService.timeData.size() - 1) >= earliestIndex + 1){
                    requestLatestDateTime = OrderService.timeData.get(earliestIndex + 1);
                }
                else{
                    requestLatestDateTime = OrderService.timeData.get(earliestIndex);
                }

                    bus.post(new OrderUpdate());
                return;
            }
        }
    }

    private static ArrayList<Integer> peopleData;
    static public ArrayList<Integer> getPeopleData(){
        return peopleData;
    }
    private static ArrayList<Date> createDateData(){
        ArrayList<Date> dateData = new ArrayList<Date>();
        Calendar calendar = Calendar.getInstance();
        for (int i=0; i<20; i++)
        {
            dateData.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }
        return dateData;
    }
    static public ArrayList<Date> dateData;
    private static ArrayList<DateTime> createDateTimeData(){
        ArrayList<DateTime> timeData = new ArrayList<DateTime>();
        for (int i=0; i<=24; i++)
        {
            String dayTimeMeridiumType = DAY_TIME_MERIDIUM_TYPE_AM;
            if(i>12){
                dayTimeMeridiumType = DAY_TIME_MERIDIUM_TYPE_PM;
            }
            int hour = i;
            if(hour>12){
                hour = hour - 12;
            }
            timeData.add(new DateTime(hour, 0, dayTimeMeridiumType));
            if(i!=12 && i!=24){
                timeData.add(new DateTime(hour, 30, dayTimeMeridiumType));
            }
        }
        return timeData;
    }
    static public ArrayList<DateTime> timeData;



    static private HashMap<MenuDishVO,Integer> order;
    static public HashMap<MenuDishVO,Integer> getOrder(){

        HashMap<MenuDishVO,Integer> orderCopy = new HashMap<MenuDishVO,Integer>();
        orderCopy = (HashMap<MenuDishVO,Integer>)order.clone();

        return orderCopy;
    }

    static public int getOrderCount(){

        int allCount = 0;
        for(Map.Entry<MenuDishVO, Integer> entry : order.entrySet()){
            MenuDishVO dishVO = entry.getKey();
            Integer count = entry.getValue();
            if(count > 0){
                allCount += count;
            }
        }

        return allCount;
    }

    static public Double getOrderPrice(){

        Double allPrice = 0.0;
        for(Map.Entry<MenuDishVO, Integer> entry : order.entrySet()){
            MenuDishVO dishVO = entry.getKey();
            Integer count = entry.getValue();
            if(count > 0){
                Double dishesPrice = dishVO.price * count;
                allPrice += dishesPrice;
            }
        }

        return allPrice;
    }

    static public Double getOrderPricePlusTip(int tip){

        Double currentOrderPrice = getOrderPrice();
        if(currentOrderPrice > 0){
            Double total = currentOrderPrice + currentOrderPrice / 100 * tip;
            return total;
        }
        return 0.0;
    }

    static public void addDish(MenuDishVO dish){

        for(Map.Entry<MenuDishVO, Integer> entry : order.entrySet()){
            MenuDishVO dishVO = entry.getKey();
            Integer count = entry.getValue();
            if(dish.id == dishVO.id){

                Integer currentCount = order.get(dishVO);
                order.put(dishVO, currentCount + 1);
                bus.post(new OrderUpdate());
                return;
            }
        }

        order.put(dish, 1);
        bus.post(new OrderUpdate());
    }

    static public void removeDish(MenuDishVO dish){

        MenuDishVO currentDishKey = null;

        for(Map.Entry<MenuDishVO, Integer> entry : order.entrySet()){
            MenuDishVO dishVO = entry.getKey();
            if(dish.id == dishVO.id){

                currentDishKey = dishVO;
                Integer currentCount = order.get(currentDishKey);
                if(currentCount > 1){
                    order.put(currentDishKey, currentCount - 1);
                    bus.post(new OrderUpdate());
                    return;
                }
            }
        }

        if(currentDishKey != null){
            order.remove(currentDishKey);
            bus.post(new OrderUpdate());
        }
    }

    static public void setCountForDish(MenuDishVO dish, Integer count){
        for(Map.Entry<MenuDishVO, Integer> entry : order.entrySet()){
            MenuDishVO dishVO = entry.getKey();
            if(dish.id == dishVO.id){
                order.remove(dishVO);
            }
        }

        order.put(dish, count);
        if(count < 1){
            order.remove(dish);
        }
        bus.post(new OrderUpdate());
    }

    static public int getDishCount(MenuDishVO dish){
        for(Map.Entry<MenuDishVO, Integer> entry : order.entrySet()){
            MenuDishVO dishVO = entry.getKey();
            Integer count = entry.getValue();
            if(dish.id == dishVO.id){
                return count;
            }
        }
        return 0;
    }
}
