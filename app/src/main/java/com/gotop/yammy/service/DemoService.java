package com.gotop.yammy.service;


import android.content.Context;

import com.gotop.yammy.drawer.vo.DishDescriptionVO;
import com.gotop.yammy.drawer.vo.MenuDishVO;
import com.gotop.yammy.drawer.vo.MenuVO;
import com.gotop.yammy.drawer.vo.OrgVO;
import com.gotop.yammy.utils.Helper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class DemoService {

    static final public String DEMO_RESTAURANT_VO_PATH = "res/raw/demo_restaurant_vo.json";
    static final public String DEMO_MENU_VO_PATH  = "res/raw/demo_menu_vo.json";
    static final public String DEMO_DISH_DESCRIPTION_VO_PATH  = "res/raw/demo_dish_description_vo.json";

    private static List<MenuDishVO> menuDishVODemoList;

    public static Boolean getIsDemoMode() {
        return _isDemoMode;
    }

    public static void setIsDemoMode(Boolean _isDemoMode) {
        DemoService._isDemoMode = _isDemoMode;
    }

    private static Boolean _isDemoMode;

    static public Object getDemoVO(Context context, String filePath){


        switch (filePath){
            case DemoService.DEMO_RESTAURANT_VO_PATH:
                return OrgVO.fromJSON(context, Helper.loadJSON(filePath), true);
            case DemoService.DEMO_MENU_VO_PATH:
                return MenuVO.fromJSON(Helper.loadJSONArray(filePath));
            case DemoService.DEMO_DISH_DESCRIPTION_VO_PATH:
                return DishDescriptionVO.fromJSON(context, Helper.loadJSON(filePath), true);
        }

        return null;
    }
}
