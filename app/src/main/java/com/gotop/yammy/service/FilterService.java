package com.gotop.yammy.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.gotop.yammy.drawer.vo.CuisineVO;
import com.gotop.yammy.drawer.vo.OrgTypeVO;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class FilterService{

    final public static int SORTED_BY_PRICE = 0;
    final public static int SORTED_BY_LOCATION = 1;
    final public static int SORTED_BY_NAME = 2;

    final public static int CUISINE_FILTER_DIALOG = 3;
    final public static int PLACE_TYPE_FILTER_DIALOG = 4;
    final public static int DISH_CATEGORY_FILTER_DIALOG = 5;
    final public static int LOCATION_FILTER_DIALOG = 6;

    static private EventBus bus = EventBus.getDefault();

    static private int _currentSort = SORTED_BY_NAME;
    static public int get_currentSort(){
        return _currentSort;
    }
    static public void set_currentSort(int sort){
        if(_currentSort != sort){
            _currentSort = sort;
            bus.post(new FilterUpdate());
        }
    }

    static private ArrayList<CuisineVO> _cuisineFilter = new ArrayList<CuisineVO>();
    static public ArrayList<CuisineVO> getCuisineFilter(){
        return _cuisineFilter;
    }
    static public void set_cuisineFilter(List<CuisineVO> filter){
        _cuisineFilter.clear();
        if(filter != null){
            _cuisineFilter.addAll(filter);
        }
        bus.post(new FilterUpdate());
    }

    static private ArrayList<OrgTypeVO> _orgTypeFilter = new ArrayList<OrgTypeVO>();
    static public ArrayList<OrgTypeVO> getOrgTypeFilter(){
        return _orgTypeFilter;
    }
    static public void set_orgTypeFilter(List<OrgTypeVO> filter){
        _orgTypeFilter.clear();
        if(filter != null){
            _orgTypeFilter.addAll(filter);
        }
        bus.post(new FilterUpdate());
    }

    public static class FilterUpdate{

    }
}
