package com.gotop.yammy.service;

import com.orm.SugarRecord;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

public class UserService {

    private static EventBus bus = EventBus.getDefault();
    public static class UserUpdate{}

    public static UserVO getUserVO() {
        return userVO;
    }

    public static void saveUserData(JSONObject jsonData) {
        userVO.updateInfo(jsonData);
        userVO.update();

        bus.post(new UserUpdate());
    }

    public static void saveUserData() {
        userVO.update();

        bus.post(new UserUpdate());
    }

    public static void logout() {
        SugarRecord.deleteAll(UserVO.class);
    }

    private static UserVO userVO;

    public static void init(){
        userVO = SugarRecord.first(UserVO.class);
        if(userVO == null){
            userVO = new UserVO();
            userVO.save();
        }
    }

    public static UserCreditCardVO getUserCreditCardVO() {
        UserCreditCardVO userCreditCardVO = SugarRecord.first(UserCreditCardVO.class);
        return userCreditCardVO;
    }
    public static void saveCreditCard(String cardNumber, String expDate, String securityCode){
        UserCreditCardVO userCreditCardVO = SugarRecord.first(UserCreditCardVO.class);
        if(userCreditCardVO == null){
            userCreditCardVO = new UserCreditCardVO();
        }

        userCreditCardVO.cardNumber = cardNumber;
        userCreditCardVO.expDate = expDate;
        userCreditCardVO.securityCode = securityCode;
        userCreditCardVO.save();
    }

    public static class UserCreditCardVO extends SugarRecord {
        public String cardNumber;
        public String expDate;
        public String securityCode;

        public UserCreditCardVO(){}   // нужно для SugarORM
    }
    public static class UserVO extends SugarRecord {

        public UserVO(){}   // нужно для SugarORM

        public static class UserDefaultValues{
            static public int cityID      = 1;  // Харьков
            static public int countryID   = 1;  // Украина
            static public int gender      = 1; // man
            static public int statusId    = 1; // active
        }

        public String login       = "";
        public String password    = "";

        public String name          = "";
        public String surname       = "";
        public String email         = "";
        public String phone         = "";
        public String dateOfBirth   = "";
        public String role          = "";

        public String avatar        = "";
        public byte[] avatarBytes   = null;


        public String authKeyID     = "";
        public int id               = 0;
        public int createdAt        = 0;
        public int updatedAt        = 0;
        public int gender           = UserDefaultValues.gender;    // 1 - man, 0 - woman
        public int cityID           = UserDefaultValues.cityID;
        public int countryID        = UserDefaultValues.countryID;
        public int statusId         = UserDefaultValues.statusId;     //1 - active, 0 - nonactive

        public int orderTip         = 20;

        public void updateInfo(JSONObject json){
            try {
                this.name         = json.getString("name");
                this.surname      = json.getString("surname");
                this.email        = json.getString("email");
                this.phone        = json.getString("phone");
                this.dateOfBirth  = json.getString("date_of_birth");

                this.authKeyID    = json.getString("auth_key");
                this.createdAt    = json.getInt("created_at");
                this.updatedAt    = json.getInt("updated_at");
                this.id           = json.getInt("id");
                this.role         = json.getString("role");
                this.statusId     = json.getInt("status_id");

                this.gender    = json.getInt("gender");
                this.cityID    = json.optInt("city_id", UserDefaultValues.cityID);
                this.countryID = json.optInt("country_id", UserDefaultValues.countryID);
                this.avatar    = json.getString("avatar");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
