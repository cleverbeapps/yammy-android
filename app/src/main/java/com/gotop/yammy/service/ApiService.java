package com.gotop.yammy.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Base64;
import android.widget.ImageView;

import com.gotop.yammy.drawer.org_list.OrgSearchListVO;
import com.gotop.yammy.drawer.vo.CuisineVO;
import com.gotop.yammy.drawer.vo.DishDescriptionVO;
import com.gotop.yammy.drawer.vo.MenuVO;
import com.gotop.yammy.drawer.vo.OrgFilterVO;
import com.gotop.yammy.drawer.vo.OrgTypeVO;
import com.gotop.yammy.drawer.vo.OrgVO;
import com.gotop.yammy.login_register.RegisterVO;
import com.gotop.yammy.login_register.UserUpdateVO;
import com.gotop.yammy.login_register.city_picker.City;
import com.gotop.yammy.utils.Helper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.client.methods.RequestBuilder;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.entity.mime.content.FileBody;

public class ApiService {

    public static final String USER = "fbLogin_972499646161978";
    public static final String PASS = "fbPass_972499646161978";
    public static final String USER_FACEBOOK_LOGIN_PREFIX = "fbLogin_";
    public static final String USER_FACEBOOK_PASSWORD_PREFIX = "fbPass_";
    private static final String log_tag = "ApiService LOG:";

    private static final String BASE_URL = "http://api.carsguru.com.ua/v1";
    private static final String SITE_BASE_URL = "http://carsguru.com.ua/";
    private static AsyncHttpClient service;

    public interface ApiResponse {void response(Boolean isSuccess, Object payload);}

    public static void init(){
        service = new AsyncHttpClient();
    }

    private static void setSessionKeyByAuthKey(AsyncHttpClient service){
        String key = UserService.getUserVO().authKeyID + ":";
        String encoded = new String(Base64.encode(key.getBytes(), Base64.NO_WRAP));
        service.addHeader("Authorization", "Basic "+encoded);
    }

    public static String getOrgImageURL(int imageID){
        return BASE_URL + "/image/" + imageID;
    }
    public static String getDishImageURL(int imageID){
        return BASE_URL + "/image/productimage/" + imageID;
    }
    public static ArrayList<String> getDisheImagesUrlsFromIDs(ArrayList<Integer> ids){
        ArrayList<String> urls = new ArrayList<String>();
        for (int index = 0; index < ids.size(); index++) {
            int id = ids.get(index);
            String url = getDishImageURL(id);
            urls.add(url);
        }
        return urls;
    }
    public static ArrayList<String> getOrgImagesUrlsFromIDs(ArrayList<Integer> ids){
        ArrayList<String> urls = new ArrayList<String>();
        for (int index = 0; index < ids.size(); index++) {
            int id = ids.get(index);
            String url = getOrgImageURL(id);
            urls.add(url);
        }
        return urls;
    }

    public static void login(final String login, final String password, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("login",true);

        String url = BASE_URL + "/login";
        RequestParams params = new RequestParams();
        params.put("username", login);
        params.put("password", password);

        RequestHandle loginRequest = service.post(url, params, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(errorResponse);
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                UserService.getUserVO().login = login;
                UserService.getUserVO().password = password;
                UserService.saveUserData(response);

                setSessionKeyByAuthKey(service);

                apiResponse.response(true, null);
            }
        });
        loginRequest.setTag("login");
    }

    public static void signup(final Context context, final RegisterVO registerVO, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("signup",true);

        String url = BASE_URL + "/user";

        RequestParams params = new RequestParams();
        params.put("username", registerVO.login);
        params.put("password", registerVO.pass);
        params.put("repassword", registerVO.pass);
        params.put("email", registerVO.email);
        params.put("name", registerVO.name);
        params.put("surname", registerVO.surname);
        params.put("gender", registerVO.gender);
        params.put("country_id", registerVO.countryId);
        params.put("city_id", registerVO.cityId);
        params.put("phone", registerVO.phone);
        params.put("date_of_birth", registerVO.dateOfBirth);

        RequestHandle signupRequest = service.post(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                apiResponse.response(false, responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onCancel() {
                apiResponse.response(false, null);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                UserService.getUserVO().login = registerVO.login;
                UserService.getUserVO().password = registerVO.pass;
                UserService.saveUserData(response);

                setSessionKeyByAuthKey(service);

                if(registerVO.profileImageBitmap != null){
                    uploadAvatar(context, registerVO.profileImageBitmap, new ApiResponse() {
                        @Override
                        public void response(Boolean isSuccess, Object payload) {

                            apiResponse.response(true, null);

                        }
                    });
                }
                else{
                    apiResponse.response(true, null);
                }
            }
        });
        signupRequest.setTag("signup");
    }

    public static void userUpdate(final Context context, final UserUpdateVO userUpdateVO, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("userUpdate",true);

        String url = BASE_URL + "/user";

        RequestParams params = new RequestParams();
        params.put("name", userUpdateVO.name);
        params.put("surname", userUpdateVO.surname);
        params.put("phone", userUpdateVO.phone);
        params.put("gender", Integer.valueOf(userUpdateVO.gender));
        params.put("country_id", Integer.valueOf(userUpdateVO.countryId));
        params.put("city_id", Integer.valueOf(userUpdateVO.cityId));
        params.put("date_of_birth", userUpdateVO.dateOfBirth);

        RequestHandle userUpdateRequest = service.patch(url, params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                apiResponse.response(false, responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onCancel() {
                apiResponse.response(false, null);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                UserService.getUserVO().name = userUpdateVO.name;
                UserService.getUserVO().surname = userUpdateVO.surname;
                UserService.getUserVO().cityID = Integer.valueOf(userUpdateVO.cityId);
                UserService.getUserVO().countryID = Integer.valueOf(userUpdateVO.countryId);
                UserService.getUserVO().phone = userUpdateVO.phone;
                UserService.saveUserData();

                if(userUpdateVO.profileImageBitmap != null){
                    uploadAvatar(context, userUpdateVO.profileImageBitmap, new ApiResponse() {
                        @Override
                        public void response(Boolean isSuccess, Object payload) {

                            apiResponse.response(true, null);
                        }
                    });
                }
                else{
                    apiResponse.response(true, null);
                }
            }
        });
        userUpdateRequest.setTag("userUpdate");
    }

    public static void uploadAvatar(Context context, final Bitmap bitmap, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("uploadAvatar",true);

        if (bitmap != null){
            final byte[] byteArray = Helper.getBytesFromBitmap(bitmap);
            //String encodedString = Base64.encodeToString(byteArray, Base64.DEFAULT);
//            final byte[] byteArray1 = Base64.decode(byteArray, Base64.DEFAULT);
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(byteArray);

            String url = BASE_URL + "/user";

            RequestHandle uploadAvatarRequest = service.put(context, url, byteArrayEntity, "image/jpeg", new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    UserService.getUserVO().avatarBytes = byteArray;
                    UserService.saveUserData();

                    apiResponse.response(true, responseBody);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    apiResponse.response(false, responseBody);
                }
            });

            uploadAvatarRequest.setTag("uploadAvatar");
        } else {
            apiResponse.response(false, null);
        }
    }

    private static Map<String,ArrayList<City>> citiesByCountryIDRequestCookie =  new HashMap<String,ArrayList<City>>();
    public static void getCitiesByCountryID(final String countryID, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("getCitiesByCountryID",true);

        ArrayList<City> cities = citiesByCountryIDRequestCookie.get(countryID);
        if(cities != null){
            apiResponse.response(true, cities);
            return;
        }

        String url = BASE_URL + "/countries/" + countryID;

        RequestHandle getCitiesByCountryIDRequest = service.get(url, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {

                ArrayList<City> cities = new ArrayList<City>();

                for (int index = 0; index < response.length(); index++) {
                    JSONObject item = null;
                    City cityVO = new City();
                    try {
                        item = response.getJSONObject(index);
                        cityVO.id = item.getString("id");
                        cityVO.name = item.getString("name");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cities.add(cityVO);
                }

                citiesByCountryIDRequestCookie.put(countryID, cities);

                apiResponse.response(true, cities);
            }
        });
        getCitiesByCountryIDRequest.setTag("getCitiesByCountryID");
    }

    private static RequestHandle getOrgSearchRequest;
    private static AsyncHttpClient getOrgSearchClient;
    public static void getOrgSearch(final OrgFilterVO orgFilterVO, final ApiResponse apiResponse){

        if(getOrgSearchRequest != null){
            getOrgSearchRequest.cancel(true);
        }
        if(getOrgSearchClient != null){
            getOrgSearchClient.cancelAllRequests(true);
        }

        getOrgSearchClient = new AsyncHttpClient();
        setSessionKeyByAuthKey(getOrgSearchClient);

        String url = BASE_URL + "/org/search";

        RequestParams params = new RequestParams();

        Double lat = orgFilterVO.location.getLatitude();
        if(lat != 0){
            params.put("lat", lat);
        }
        Double lng = orgFilterVO.location.getLongitude();
        if(lng != 0){
            params.put("lng", lng);
        }
        String city = orgFilterVO.cityID;
        if(!TextUtils.isEmpty(city)){
            params.put("city", city);
        }
        String cuisine = orgFilterVO.cuisine;
        if(!TextUtils.isEmpty(cuisine)){
            params.put("cuisine", cuisine);
        }
        String name = orgFilterVO.name;
        if(!TextUtils.isEmpty(name)){
            params.put("name", name);
        }
        String feature = orgFilterVO.feature;
        if(!TextUtils.isEmpty(feature)){
            params.put("feature", feature);
        }
        String type = orgFilterVO.type;
        if(!TextUtils.isEmpty(type)){
            params.put("type", type);
        }

        getOrgSearchRequest = getOrgSearchClient.post(url, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if(response != null && response.length() == 0){
                    apiResponse.response(false, response);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {

                ArrayList<OrgSearchListVO> orgSearchListVO = new ArrayList<OrgSearchListVO>();

                for (int index = 0; index < response.length(); index++) {
                    JSONObject item = null;
                    OrgSearchListVO orgSearchVO = new OrgSearchListVO();
                    try {
                        item = response.getJSONObject(index);
                        orgSearchVO = OrgSearchListVO.fromJSON(item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    orgSearchListVO.add(orgSearchVO);
                }

                apiResponse.response(true, orgSearchListVO);
            }
        });
    }

    public static void getOrganizationByID(final Context context, int id, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("getOrganizationByID",true);

        String url = BASE_URL + "/org/" + id;

            RequestHandle getOrganizationByIDRequest = service.get(url, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                OrgVO orgVO = OrgVO.fromJSON(context, response, false);
                apiResponse.response(true, orgVO);
            }
        });
        getOrganizationByIDRequest.setTag("getOrganizationByID");
    }

    public static void setOrgFavorite(Context context, int id, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("setOrgFavorite",true);

        String url = BASE_URL + "/org/favorite";
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(id);

        ByteArrayEntity entity = null;
        try {
            String s = arrayList.toString();
            byte[] b = s.getBytes("UTF-8");
            entity = new ByteArrayEntity(b);
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestHandle setOrgFavoriteRequest = service.post(context, url, entity, "application/json", new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                apiResponse.response(true, response);
            }
        });
        setOrgFavoriteRequest.setTag("setOrgFavorite");
    }

    public static void setDishFavorite(Context context, int id, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("setDishFavorite",true);

        String url = BASE_URL + "/dish/favorite";
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(id);

        ByteArrayEntity entity = null;
        try {
            String s = arrayList.toString();
            byte[] b = s.getBytes("UTF-8");
            entity = new ByteArrayEntity(b);
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestHandle setDishFavoriteRequest = service.post(context, url, entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                apiResponse.response(true, response);
            }
        });
        setDishFavoriteRequest.setTag("setDishFavorite");
    }

    public static void removeOrgFavorite(final Context context, int id, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("removeOrgFavorite",true);

        String url = BASE_URL + "/org/favorite/" + id;

        RequestHandle removeOrgFavoriteRequest = service.delete(url, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                OrgVO orgVO = OrgVO.fromJSON(context, response, false);
                apiResponse.response(true, orgVO);
            }
        });
        removeOrgFavoriteRequest.setTag("removeOrgFavorite");
    }

    public static void removeDishFavorite(final Context context, int id, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("removeDishFavorite", true);

        String url = BASE_URL + "/dish/favorite/" + id;

        RequestHandle removeDishFavoriteRequest = service.delete(url, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                OrgVO orgVO = OrgVO.fromJSON(context, response, false);
                apiResponse.response(true, orgVO);
            }
        });
        removeDishFavoriteRequest.setTag("removeDishFavorite");
    }

    private static Map<Integer,MenuVO> getMenuByOrgIDRequestCookie =  new HashMap<Integer, MenuVO>();
    public static void getMenuByOrgID(final Integer orgID, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("getMenuByOrgID", true);

        MenuVO menuVO = getMenuByOrgIDRequestCookie.get(orgID);
        if(menuVO != null){
            apiResponse.response(true, menuVO);
            return;
        }

        String url = BASE_URL + "/menu/" + orgID;

        RequestHandle getMenuByOrgIDRequest = service.get(url, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {

                MenuVO menu = MenuVO.fromJSON(response);

                getMenuByOrgIDRequestCookie.put(orgID, menu);

                apiResponse.response(true, menu);
            }
        });
        getMenuByOrgIDRequest.setTag("getMenuByOrgID");
    }

    public static void getDishByID(final Context context, int id, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("getDishByID", true);

        String url = BASE_URL + "/dish/" + id;

        RequestHandle getDishByIDRequest = service.get(url, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                DishDescriptionVO dishDescriptionVO = DishDescriptionVO.fromJSON(context, response, false);
                apiResponse.response(true, dishDescriptionVO);
            }
        });
        getDishByIDRequest.setTag("getDishByID");
    }

    private static ArrayList<OrgTypeVO> orgTypeVOs = new ArrayList<OrgTypeVO>();
    public static void getOrgTypes(Context context, final ApiResponse apiResponse){

        service.cancelRequestsByTAG("getOrgTypes", true);

        if(orgTypeVOs.size() > 0){
            apiResponse.response(true, orgTypeVOs);
            return;
        }

        String url = BASE_URL + "/orgtypes";

        RequestHandle getOrgTypesRequest = service.get(context, url, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {

                for (int index = 0; index < response.length(); index++) {
                    JSONObject item = null;
                    OrgTypeVO orgTypeVO = new OrgTypeVO();
                    try {
                        item = response.getJSONObject(index);
                        orgTypeVO = OrgTypeVO.fromJSON(item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    orgTypeVOs.add(orgTypeVO);
                }

                //OrgTypeVO.saveAllOrgTypes(orgTypeVOs);
                apiResponse.response(true, orgTypeVOs);
            }
        });
        getOrgTypesRequest.setTag("getOrgTypes");
    }

    private static ArrayList<CuisineVO> cuisineVOs = new ArrayList<CuisineVO>();
    public static void getCuisines(final ApiResponse apiResponse){

        service.cancelRequestsByTAG("getCuisines", true);

        if(cuisineVOs.size() > 0){
            apiResponse.response(true, cuisineVOs);
            return;
        }

        String url = BASE_URL + "/cousines";

        RequestHandle getCuisinesRequest = service.get(url, new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                apiResponse.response(false, errorResponse);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {


                for (int index = 0; index < response.length(); index++) {
                    JSONObject item = null;
                    CuisineVO cuisineVO = new CuisineVO();
                    try {
                        item = response.getJSONObject(index);
                        cuisineVO = CuisineVO.fromJSON(item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    cuisineVOs.add(cuisineVO);
                }

                //CuisineVO.saveAllCuisines(cuisineVOs);
                apiResponse.response(true, cuisineVOs);
            }
        });
        getCuisinesRequest.setTag("getCuisines");
    }
}
