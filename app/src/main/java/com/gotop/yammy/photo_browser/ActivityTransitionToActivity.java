package com.gotop.yammy.photo_browser;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.utils.Helper;
import com.gotop.yammy.utils.IconsTextView;
import com.squareup.picasso.Picasso;

import uk.co.senab.photoview.PhotoView;

public class ActivityTransitionToActivity extends AppCompatActivity {

    static public int selectedPhotoIndex = 0;

    PhotoPagerAdapter photoPagerAdapter;
    TextView toolbarTitle;
    Toolbar toolbar;
    ImageButton backButton;
    ViewPager pager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transition_to);

        toolbar = (Toolbar)findViewById(R.id.activity_transition_to_toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_photo_browser_title);
        toolbarTitle.setText("Payment Details");
        backButton = (ImageButton) toolbar.findViewById(R.id.toolbar_close_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        final Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(Color.LTGRAY);
        backButton.setColorFilter(ContextCompat.getColor(ActivityTransitionToActivity.this, R.color.gold));
        toolbarTitle.setHintTextColor(ContextCompat.getColor(ActivityTransitionToActivity.this, R.color.gold));
        toolbar.getBackground().setAlpha(255);
        IconsTextView shareIcon = (IconsTextView)toolbar.findViewById(R.id.toolbar_photo_browser_share_icon);
        shareIcon.setText(String.valueOf((char) 0xe918));
        shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareImage();
            }
        });

        if(PhotoGridViewActivity.photosVO != null && PhotoGridViewActivity.photosVO.images() != null){

            pager = (ViewPager) findViewById(R.id.photo_browser_photo_pager);
            photoPagerAdapter = new PhotoPagerAdapter(this);
            pager.setAdapter(photoPagerAdapter);
            pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
                @Override
                public void onPageSelected(int position) {
                    updateToolbarTitle();
                }
            });
            pager.setCurrentItem(selectedPhotoIndex);
        }
    }

    private void updateToolbarTitle(){
        String total = String.valueOf(PhotoGridViewActivity.photosVO.images().size());
        String current = String.valueOf(pager.getCurrentItem() + 1);
        toolbarTitle.setText(current + " / " + total);
    }

    private void shareImage(){
        if(photoPagerAdapter != null && photoPagerAdapter.currentPhotoView != null){
            Helper.shareImage(this, photoPagerAdapter.currentPhotoView);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(photoPagerAdapter != null && PhotoGridViewActivity.photosVO != null && PhotoGridViewActivity.photosVO.images() != null){
            photoPagerAdapter.notifyDataSetChanged();
            updateToolbarTitle();
        }
    }

    static class PhotoPagerAdapter extends PagerAdapter {

        public PhotoView currentPhotoView;

        Context activity;

        public PhotoPagerAdapter(Context context){
            this.activity = context;
        }

        @Override
        public int getCount() {
            return PhotoGridViewActivity.photosVO.images().size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {

            //PhotoView photoView = (PhotoView)findViewById(R.id.photo_browser_photo_view);
            currentPhotoView = new PhotoView(container.getContext());
            //photoView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

            // Now just add PhotoView to ViewPager and return it
            container.addView(currentPhotoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            Object image = PhotoGridViewActivity.photosVO.images().get(position);

            if(image instanceof String){
                String imageString = (String)image;

                Picasso.with(activity) //
                        .load(imageString) //
                        .placeholder(R.drawable.image_placeholder) //
                        .error(R.drawable.image_placeholder) //
                        .fit() //
                        .tag(container)
                        .centerInside()
                        .into(currentPhotoView);
            }
            else {
                int imageID = (int)image;

                Picasso.with(activity) //
                        .load(imageID) //
                        .placeholder(R.drawable.image_placeholder) //
                        .error(R.drawable.image_placeholder) //
                        .fit() //
                        .tag(container)
                        .centerInside()
                        .into(currentPhotoView);
            }

            return currentPhotoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
            Picasso.with(activity).cancelTag(container);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }
}
