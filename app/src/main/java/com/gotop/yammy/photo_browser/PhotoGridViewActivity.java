package com.gotop.yammy.photo_browser;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gotop.yammy.R;
import com.gotop.yammy.drawer.org_list.OrgSearchListVO;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PhotoGridViewActivity extends AppCompatActivity {

    PhotoGridViewAdapter photoGridViewAdapter;

    static public class PhotosIDsVO implements PhotosVOInterface{
        private String title = "Title";
        private ArrayList<Integer>  imageIDs;
        public PhotosIDsVO(String title, ArrayList<Integer>  imageIDs){
            this.title = title;
            this.imageIDs = imageIDs;
        }
        public ArrayList images(){
            return imageIDs;
        }
        public String title(){
            return this.title;
        }
    }

    static public class PhotosURLsVO implements PhotosVOInterface{
        private String title = "Title";
        private ArrayList<String>  imageURLs;
        public PhotosURLsVO(String title, ArrayList<String>  imageURLs){
            this.title = title;
            this.imageURLs = imageURLs;
        }
        public ArrayList images(){
            return this.imageURLs;
        }
        public String title(){
            return this.title;
        }
    }

    public interface PhotosVOInterface {
        ArrayList images();
        String title();
    }

    static public PhotosVOInterface photosVO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_gridview_activity);

        Toolbar toolbar = (Toolbar)findViewById(R.id.photo_grid_toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_close_title);
        textView.setText(photosVO.title());

        ImageButton backButton;
        backButton = (ImageButton) toolbar.findViewById(R.id.toolbar_close_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        RecyclerView photoList = (RecyclerView) findViewById(R.id.photo_grid_list);
        photoList.setLayoutManager(new GridLayoutManager(this, 2));
        photoGridViewAdapter = new PhotoGridViewAdapter(this);
        photoList.setAdapter(photoGridViewAdapter);

        final GestureDetector gestureDetector =
                new GestureDetector(PhotoGridViewActivity   .this, new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        return true;
                    }
                });
        photoList.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                 @Override
                 public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                     View child = rv.findChildViewUnder(e.getX(), e.getY());
                     int position = rv.getChildAdapterPosition(child);

                     if(gestureDetector.onTouchEvent(e) && photosVO != null && photosVO.images() != null && photosVO.images().size() > 0){

                         if (Build.VERSION.SDK_INT < 21) {
                             Toast.makeText(PhotoGridViewActivity.this, "21 SDK + only, keep out", Toast.LENGTH_SHORT).show();
                         } else {
                             ActivityTransitionToActivity.selectedPhotoIndex = position;
                             Intent intent = new Intent(PhotoGridViewActivity.this, ActivityTransitionToActivity.class);
                             ActivityOptionsCompat options = ActivityOptionsCompat.
                                     makeSceneTransitionAnimation(PhotoGridViewActivity.this, child, "TEST");
                             startActivity(intent, options.toBundle());
                         }
                     }
                     return false;
                 }

                 @Override
                 public void onTouchEvent(RecyclerView rv, MotionEvent e) {}
                 @Override
                 public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}});
                //photoList.setOnScrollChangeListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    static public class PhotoGridViewAdapter extends RecyclerView.Adapter<ImageViewHolder> {
        private final Context context;

        public PhotoGridViewAdapter(Context context) {
            this.context = context;
        }

        @Override
        public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            ImageViewHolder holder = ImageViewHolder.inflate(parent);
            return holder;
        }

        @Override
        public void onBindViewHolder(ImageViewHolder holder, int position) {

            Object image = photosVO.images().get(position);

            if(image instanceof String){
                String imageString = (String)image;

                Picasso.with(context) //
                        .load(imageString) //
                        .placeholder(R.drawable.image_placeholder) //
                        .error(R.drawable.image_placeholder) //
                        .fit() //
                        .tag("")
                        .centerCrop()
                        .into(holder.imageView);
            }
            else {
                int imageID = (int)image;

                Picasso.with(context) //
                        .load(imageID) //
                        .placeholder(R.drawable.image_placeholder) //
                        .error(R.drawable.image_placeholder) //
                        .fit() //
                        .tag(context)
                        .centerCrop()
                        .into(holder.imageView);
            }
        }

        @Override
        public int getItemCount() {
            return PhotoGridViewActivity.photosVO.images().size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {

        public static ImageViewHolder inflate(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_image, parent, false);
            return new ImageViewHolder(view);
        }

        //public TextView mTextTitle;
        ImageView imageView;
        ProgressBar progressBar;

        public ImageViewHolder(View view) {
            super(view);
            this.imageView = (ImageView)view.findViewById(R.id.photo_browser_image);
            this.progressBar = (ProgressBar) view.findViewById(R.id.photo_browser_image_progress);

            //mTextTitle = (TextView) view.findViewById(R.id.title);
        }

//        private void bind(String title) {
//            mTextTitle.setText(title);
//        }
    }
}
