package com.gotop.yammy.login_register.country_picker;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.utils.Helper;

import java.util.List;

public class CountryPickerDialog extends AppCompatDialog {

    public interface CountryPickerCallbacks {
        void onCountrySelected(Country country);
    }

    private List<Country> countries;
    private CountryPickerCallbacks callbacks;
    private ListView listview;
    private String headingCountryCode;
    private boolean showDialingCode;

    public CountryPickerDialog(Context context, CountryPickerCallbacks callbacks,
                               @Nullable String headingCountryCode, boolean showDialingCode) {
        super(context);
        this.callbacks = callbacks;
        this.headingCountryCode = headingCountryCode;
        this.showDialingCode = showDialingCode;

        countries = Country.getAvailableCountries();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.y = 10; params.x = 10;
        getWindow().setAttributes(params);

        setContentView(R.layout.country_picker);


        ViewCompat.setElevation(getWindow().getDecorView(), 3);
        listview = (ListView) findViewById(R.id.country_picker_listview);

        setTitle("Select a Country Code");


        final int gold = ContextCompat.getColor(getContext(), R.color.gold);

        // Title
        final TextView title = (TextView)findViewById(android.support.v7.appcompat.R.id.title);
        if (title != null) {
            title.setTextColor(gold);
            Helper.setCustomTextViewFont(title, Helper.GOTHAM_PRO_REG_FONT_PATH, getContext());
        }

        CountryListAdapter adapter = new CountryListAdapter(this.getContext(), countries, showDialingCode);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                hide();

                Country country = countries.get(position);
                callbacks.onCountrySelected(country);
            }
        });

        scrollToHeadingCountry();
    }

    private void scrollToHeadingCountry() {
        if (headingCountryCode != null) {
            for (int i = 0; i < listview.getCount(); i++) {
                if (((Country) listview.getItemAtPosition(i)).isoCode.toLowerCase()
                        .equals(headingCountryCode.toLowerCase())) {
                    listview.setSelection(i);
                }
            }
        }
    }
}
