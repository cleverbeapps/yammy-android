package com.gotop.yammy.login_register.choose_photo_diallog;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;

import com.gotop.yammy.R;

import java.util.ArrayList;
import java.util.List;

public class ChoosePhotoDialog extends AppCompatDialog {

    public final static int TAKE_PHOTO = 0;
    public final static int CHOOSE_FROM_LIBRARY = 1;
    public final static int CANCEL = 2;

    public static class ChoosePhotoDialogItem{
        public int titleID;
        public int id;
        public ChoosePhotoDialogItem(int titleID, int id){
            this.titleID = titleID;
            this.id = id;
        }
    }

    public interface ChoosePhotoDialogCallback {
        void onSelect(ChoosePhotoDialogItem choosePhotoDialogItem);
    }

    private List<ChoosePhotoDialogItem> dialogItems;
    private ChoosePhotoDialogCallback callbacks;
    private ListView listview;

    public ChoosePhotoDialog(Context context, ChoosePhotoDialogCallback callbacks) {
        super(context);
        this.callbacks = callbacks;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.choose_photo_picker);

        ViewCompat.setElevation(getWindow().getDecorView(), 3);
        listview = (ListView) findViewById(R.id.choose_photo_picker_listview);

//        setTitle("Select your City");
//
//        final int gold = ContextCompat.getColor(getContext(), R.color.gold);
//
//        // Title
//        final TextView title = (TextView)findViewById(android.support.v7.appcompat.R.id.title);
//        if (title != null) {
//            title.setTextColor(gold);
//            Helper.setCustomTextViewFont(title, Helper.GOTHAM_PRO_REG_FONT_PATH, getContext());
//        }

        dialogItems = new ArrayList<ChoosePhotoDialogItem>();
        dialogItems.add(new ChoosePhotoDialogItem(R.string.take_photo, ChoosePhotoDialog.TAKE_PHOTO));
        dialogItems.add(new ChoosePhotoDialogItem(R.string.choose_photo_from_libarary, ChoosePhotoDialog.CHOOSE_FROM_LIBRARY));
        dialogItems.add(new ChoosePhotoDialogItem(R.string.cancel, ChoosePhotoDialog.CANCEL));


        ChoosePhotoListAdapter adapter = new ChoosePhotoListAdapter(this.getContext(), dialogItems);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                dismiss();

                ChoosePhotoDialogItem dialogItem = dialogItems.get(position);
                callbacks.onSelect(dialogItem);
            }
        });
    }
}
