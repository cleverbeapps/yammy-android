package com.gotop.yammy.login_register;


import android.graphics.Bitmap;

public class RegisterVO {

    public String login = "";
    public String pass = "";
    public String email = "";
    public String name = "";
    public String surname = "";
    public String phone = "";
    public String cityId = "0";
    public String countryId ="0";

    public String dateOfBirth = "";
    public String gender = "1"; // man

    public Bitmap profileImageBitmap;
}
