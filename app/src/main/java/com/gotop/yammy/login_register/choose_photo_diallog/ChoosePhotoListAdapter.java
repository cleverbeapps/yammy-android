package com.gotop.yammy.login_register.choose_photo_diallog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gotop.yammy.R;

import java.util.List;

public class ChoosePhotoListAdapter extends BaseAdapter {

    private final Context mContext;
    private static final String TAG = ChoosePhotoListAdapter.class.getSimpleName();
    private LayoutInflater inflater;
    private List<ChoosePhotoDialog.ChoosePhotoDialogItem> chooseTypes;

    public ChoosePhotoListAdapter(Context context, List<ChoosePhotoDialog.ChoosePhotoDialogItem> chooseTypes) {
        mContext = context;
        this.chooseTypes = chooseTypes;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return chooseTypes.size();
    }

    @Override
    public Object getItem(int position) {
        return chooseTypes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        Item item;
        ChoosePhotoDialog.ChoosePhotoDialogItem chooseType = chooseTypes.get(position);

        if (convertView == null) {
            item = new Item();
            itemView = inflater.inflate(R.layout.item_photo_choose, parent, false);
            item.title = (TextView) itemView.findViewById(R.id.item_photo_choose_text_view);
            itemView.setTag(item);
        } else {
            item = (Item) itemView.getTag();
        }

        item.title.setText(chooseType.titleID);

        return itemView;
    }

    private static class Item {
        public TextView title;
    }
}
