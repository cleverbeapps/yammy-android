package com.gotop.yammy.login_register.city_picker;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gotop.yammy.R;

import java.util.List;
import java.util.Objects;

public class CityListAdapter extends BaseAdapter{

    private final Context mContext;
    private static final String TAG = CityListAdapter.class.getSimpleName();
    private LayoutInflater inflater;
    private List<City> cities;
    private City currentCity;

    public CityListAdapter(Context context, List<City> cities) {
        mContext = context;
        this.cities = cities;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public CityListAdapter(Context context, List<City> cities, City currentCity) {
        mContext = context;
        this.cities = cities;
        this.currentCity = currentCity;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return cities.size();
    }

    @Override
    public Object getItem(int position) {
        return cities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        Item item;
        City city = cities.get(position);

        if (convertView == null) {
            item = new Item();
            itemView = inflater.inflate(R.layout.item_city, parent, false);
            item.name = (TextView) itemView.findViewById(R.id.item_city_name);
            itemView.setTag(item);
        } else {
            item = (Item) itemView.getTag();
        }

        item.name.setText(city.name);
        final int blackLight = ContextCompat.getColor(mContext, R.color.blackLight);
        item.name.setTextColor(blackLight);

        if(currentCity != null){
            if(Objects.equals(currentCity.id, city.id)){
                final int gold = ContextCompat.getColor(mContext, R.color.gold);
                item.name.setTextColor(gold);
            }
        }

        return itemView;
    }

    private static class Item {
        public TextView name;
    }
}
