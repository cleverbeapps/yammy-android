package com.gotop.yammy.login_register.country_picker;

import android.telephony.TelephonyManager;

import com.gotop.yammy.utils.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Objects;

public class Country {

    public String isoCode;
    public String dialingCode;

    // API
    public String id;
    public String defaultCityID;

    public int getResourceID(){
        String flagName = this.isoCode.toLowerCase(Locale.ENGLISH) + "_flag";
        int flagResID = Helper.appContext.getResources().getIdentifier(
                flagName.toLowerCase(Locale.ENGLISH), "mipmap", Helper.appContext.getPackageName());
        return flagResID;
    }

    private static ArrayList<Country> availableCountries;
    public static ArrayList<Country> getAvailableCountries(){

        if(availableCountries == null){

            JSONObject json = Helper.loadJSON("res/raw/countries_dialing_code.json");
            JSONArray jsonCountries = null;
            try {
                jsonCountries = json.getJSONArray("countries");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            availableCountries = new ArrayList<Country>();

            for (int i = 0; i < jsonCountries.length(); i++) {

                JSONObject object = null;
                try {
                    object = jsonCountries.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Country country = new Country();
                try {
                    country.isoCode = object.getString("isoCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    country.dialingCode = object.getString("dealingCode");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    country.id = object.getString("id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    country.defaultCityID = object.getString("defaultCityID");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                availableCountries.add(country);
            }

            Collections.sort(availableCountries, new Comparator<Country>() {
                @Override
                public int compare(Country country1, Country country2) {
                    return new Locale(Helper.appContext.getResources().getConfiguration().locale.getLanguage(),
                            country1.isoCode).getDisplayCountry().compareTo(
                            new Locale(Helper.appContext.getResources().getConfiguration().locale.getLanguage(),
                                    country2.isoCode).getDisplayCountry());
                }
            });
        }

        return availableCountries;
    }

    static public Country getCountryById(String id){
        ArrayList<Country> availableCountries = getAvailableCountries();
        for (int position=0; position<availableCountries.size(); position++) {
            Country country = availableCountries.get(position);
            if (Objects.equals(country.id, id))
                return country;
        }

        return getCurrentLocaleCountryFromAvailable();
    }

    static public Country getCurrentLocaleCountryFromAvailable(){

        ArrayList<Country> availableCountries = getAvailableCountries();
        String localeCountryCode = getCurrentLocaleCountryCodeIso();

        for (int position=0; position<availableCountries.size(); position++) {
            Country country = availableCountries.get(position);
            if (Objects.equals(country.isoCode, localeCountryCode))
                return country;
        }

        return availableCountries.get(0);
    }

    static private String getCurrentLocaleCountryCodeIso(){

        TelephonyManager telephonyManager  = (TelephonyManager) Helper.
                appContext.
                getSystemService(Helper.appContext.TELEPHONY_SERVICE);

        String localeCountryCode = telephonyManager.getNetworkCountryIso().toLowerCase();

        return localeCountryCode;
    }

//    static public CountryVO getUSA(){
//        CountryVO usa = new CountryVO();
//        usa.id = 2;
//        usa.name = "USA";
//        usa.code = "US";
//        usa.phoneCode = "+1";
//        usa.defaultCityID = 2;
//        return usa;
//    }
//
//    static public CountryVO getUkraine(){
//        CountryVO ukraine = new CountryVO();
//        ukraine.id = 1;
//        ukraine.name = "Ukraine";
//        ukraine.code = "UA";
//        ukraine.phoneCode = "+380";
//        ukraine.defaultCityID = 1;   // Kharkov
//        return ukraine;
//    }
}
