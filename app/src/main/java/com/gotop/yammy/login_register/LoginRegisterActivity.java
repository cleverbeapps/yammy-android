package com.gotop.yammy.login_register;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.gotop.yammy.R;
import com.gotop.yammy.drawer.DrawerActivity;
import com.gotop.yammy.login_register.choose_photo_diallog.ChoosePhotoDialog;
import com.gotop.yammy.login_register.city_picker.City;
import com.gotop.yammy.login_register.city_picker.CityPickerDialog;
import com.gotop.yammy.login_register.country_picker.Country;
import com.gotop.yammy.login_register.country_picker.CountryPickerDialog;
import com.gotop.yammy.login_register.segment_picker.SegmentedGroup;
import com.gotop.yammy.service.ApiService;
import com.gotop.yammy.service.DemoService;
import com.gotop.yammy.utils.Helper;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;

public class LoginRegisterActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener  {

    SegmentedGroup segmentedGroup;
    private CallbackManager callbackManager = CallbackManager.Factory.create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register);

        Toolbar toolbar = (Toolbar)findViewById(R.id.login_reg_toolbar);
        ImageButton backButton;
        if (toolbar != null) {
            TextView toolbarTitle = (TextView)toolbar.findViewById(R.id.toolbar_back_title);
            if(toolbarTitle != null){
                toolbarTitle.setText(R.string.title_activity_login_reg);
            }
            backButton = (ImageButton) toolbar.findViewById(R.id.toolbar_back_btn);
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }

        segmentedGroup = (SegmentedGroup) findViewById(R.id.login_reg_segment);
        segmentedGroup.setOnCheckedChangeListener(this);
        segmentedGroup.check(R.id.login_segment_button);
    }

    public void fbLogin(){
        //LoginManager.getInstance().logOut();  // для дебага

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {

                                        try {
                                            String userID = object.getString("id");
                                            String fbLogin = ApiService.USER_FACEBOOK_LOGIN_PREFIX + userID;
                                            String fbPass = ApiService.USER_FACEBOOK_PASSWORD_PREFIX + userID;
                                            tryLogin(fbLogin, fbPass);

                                        } catch (JSONException e) {
                                            showFBLoginError();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "email");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        showFBLoginError();
                    }
                });

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
    }

    public void fbRegister(){
        //LoginManager.getInstance().logOut();  // для дебага

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {

                                        try {
                                            RegisterVO registerVO = new RegisterVO();

                                            String userID = object.getString("id");
                                            String fbLogin = ApiService.USER_FACEBOOK_LOGIN_PREFIX + userID;
                                            String fbPass = ApiService.USER_FACEBOOK_PASSWORD_PREFIX + userID;
                                            String email = object.getString("email");
                                            String name = object.getString("first_name");
                                            String surname = object.getString("last_name");
                                            String gender = object.getString("gender");

                                            Country currentCountry = Country.getCurrentLocaleCountryFromAvailable();

                                            registerVO.login = fbLogin;
                                            registerVO.pass = fbPass;
                                            registerVO.name = name;
                                            registerVO.surname = surname;
                                            registerVO.email = email;
                                            registerVO.gender = gender;
                                            registerVO.phone = "";
                                            registerVO.cityId = currentCountry.defaultCityID;
                                            registerVO.countryId = currentCountry.id;

                                            tryRegister(registerVO);

                                        } catch (JSONException e) {
                                            showFBRegistrationError();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, name, first_name, last_name, picture.type(large), email, gender");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        showFBRegistrationError();
                    }
                });

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
    }

    private void showFBLoginError(){
        Toast.makeText(LoginRegisterActivity.this, R.string.toast_fb_login_error, Toast.LENGTH_LONG).show();
    }

    private void showFBRegistrationError(){
        Toast.makeText(LoginRegisterActivity.this, R.string.toast_fb_registration_error, Toast.LENGTH_LONG).show();
    }

    public void tryLogin(String login, String pass){

        Helper.showLoadingDialog(this, "Login", "Please wait...");

        ApiService.login(login, pass, new ApiService.ApiResponse() {
            @Override
            public void response(Boolean isSuccess, Object payload) {

                if(isSuccess){
                    Helper.dismissLoadingDialog();
                    showMainDrawer();
                }
                else{
                    Helper.dismissLoadingDialog();
                    Toast.makeText(LoginRegisterActivity.this, R.string.toast_wrong_login_credentials, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void tryRegister(RegisterVO registerVO){

        Helper.showLoadingDialog(this, "Registration", "Please wait...");

        ApiService.signup(this, registerVO, new ApiService.ApiResponse() {
            @Override
            public void response(Boolean isSuccess, Object payload) {

                if(isSuccess){
                    Helper.dismissLoadingDialog();
                    showMainDrawer();
                }
                else{
                    Helper.dismissLoadingDialog();
                    Toast.makeText(LoginRegisterActivity.this, R.string.toast_wrong_registration_data, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void showMainDrawer(){

        DemoService.setIsDemoMode(false);

        Intent intent = new Intent(this, DrawerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.login_segment_button:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.login_reg_container, new LoginFragment())
                        .commit();
                break;
            case R.id.register_segment_button:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.login_reg_container, new RegisterFragment())
                        .commit();
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.add_segmented:
//                addButton(segmented5);
//                break;
//            case R.id.remove_segmented:
//                removeButton(segmented5);
//                break;
//            default:
//                // Nothing to do
//        }
    }

    public static class LoginFragment extends Fragment {

        TextInputEditText tfLogin;
        TextInputEditText tfPass;
        Button btnLogin;
        Button btnLoginFB;
        Button btnForgot;
        TextInputLayout loginInput;
        TextInputLayout passInput;

        public LoginFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_login, container, false);
            loginInput = (TextInputLayout) rootView.findViewById(R.id.fr_login_login_ti_layout);
            passInput = (TextInputLayout) rootView.findViewById(R.id.fr_login_pass_ti_layout);
            tfLogin = (TextInputEditText) rootView.findViewById(R.id.fr_login_login_text_edit);
            tfPass = (TextInputEditText) rootView.findViewById(R.id.fr_login_pass_text_edit);
            btnLogin = (Button) rootView.findViewById(R.id.fr_login_btn_login);
            btnLoginFB = (Button) rootView.findViewById(R.id.fr_login_btn_fb);
            btnForgot = (Button) rootView.findViewById(R.id.fg_login_btn_forgot_pass);

            tfLogin.addTextChangedListener(new LoginTextWatcher(tfLogin));
            tfPass.addTextChangedListener(new LoginTextWatcher(tfPass));

            btnLogin.setEnabled(false);

            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loginClick();
                }
            });
            btnLoginFB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fbLoginClick();
                }
            });
            btnForgot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            return rootView;
        }

        private void fbLoginClick() {
            LoginRegisterActivity activity = (LoginRegisterActivity) getActivity();
            activity.fbLogin();
        }

        private void loginClick() {

            if(!validateLogin()){
                return;
            }
            if(!validatePass()){
                return;
            }

            String username = tfLogin.getText().toString();
            String password = tfPass.getText().toString();

            LoginRegisterActivity activity = (LoginRegisterActivity) getActivity();
            activity.tryLogin(username, password);
        }

        private boolean validateLogin() {
            if (tfLogin.getText().toString().trim().isEmpty() || !Helper.isValidPassword(tfLogin.getText().toString(), false)) {
                loginInput.setError(getString(R.string.wrong_login));
                requestFocus(loginInput);
                btnLogin.setEnabled(false);
                return false;
            } else {
                loginInput.setErrorEnabled(false);
            }

            return true;
        }

        private boolean validatePass() {
            if (tfPass.getText().toString().trim().isEmpty() || !Helper.isValidPassword(tfPass.getText().toString(), false)) {
                passInput.setError(getString(R.string.wrong_pass));
                requestFocus(passInput);
                btnLogin.setEnabled(false);
                return false;
            } else {
                passInput.setErrorEnabled(false);
            }

            return true;
        }

        private void requestFocus(View view) {
            if (view.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        }

        private class LoginTextWatcher implements TextWatcher {

            private View view;

            private LoginTextWatcher(View view) {
                this.view = view;
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void afterTextChanged(Editable editable) {

                Boolean isValid = false;

                switch (view.getId()) {
                    case R.id.fr_login_login_text_edit:
                        isValid = validateLogin();
                        break;
                    case R.id.fr_login_pass_text_edit:
                        isValid = validatePass();
                        break;
                }

                btnLogin.setEnabled(isValid);
            }
        }
    }

    public static class RegisterFragment extends Fragment {

        TextInputEditText tfLogin;
        TextInputEditText tfName;
        TextInputEditText tfSurname;
        TextInputEditText tfEmail;
        TextInputEditText tfPhoneNumber;
        TextInputEditText tfCity;
        TextInputEditText tfPass;
        TextInputLayout loginInput;
        TextInputLayout nameInput;
        TextInputLayout surnameInput;
        TextInputLayout emailInput;
        TextInputLayout phoneNumberInput;
        TextInputLayout cityInput;
        TextInputLayout passInput;

        LinearLayout layoutCountryCodeAndPhone;
        ImageView imageViewFlag;
        TextView textViewCountryCode;
        Button btnChooseCity;
        Button btnRegister;
        Button btnRegisterFB;
        Button btnAddphoto;
        ImageView profileDefaultImage;
        CircleImageView circleProfileImageView;

        Country currentCountry;
        City currentCity;
        ArrayList<City> currentCountryCities;

        public RegisterFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_register, container, false);

            tfLogin = (TextInputEditText) rootView.findViewById(R.id.fr_reg_text_input_login);
            tfName = (TextInputEditText) rootView.findViewById(R.id.fr_reg_text_input_name);
            tfSurname = (TextInputEditText) rootView.findViewById(R.id.fr_reg_text_input_surname);
            tfEmail = (TextInputEditText) rootView.findViewById(R.id.fr_reg_text_input_email);
            tfPhoneNumber = (TextInputEditText) rootView.findViewById(R.id.fr_reg_text_input_phone_number);
            tfCity = (TextInputEditText) rootView.findViewById(R.id.fr_reg_text_input_city);
            tfPass = (TextInputEditText) rootView.findViewById(R.id.fr_reg_text_input_pass);

            tfLogin.addTextChangedListener(new RegisterTextWatcher(tfLogin));
            tfName.addTextChangedListener(new RegisterTextWatcher(tfName));
            tfSurname.addTextChangedListener(new RegisterTextWatcher(tfSurname));
            tfEmail.addTextChangedListener(new RegisterTextWatcher(tfEmail));
            tfPhoneNumber.addTextChangedListener(new RegisterTextWatcher(tfPhoneNumber));
            tfCity.addTextChangedListener(new RegisterTextWatcher(tfCity));
            tfPass.addTextChangedListener(new RegisterTextWatcher(tfPass));

            loginInput = (TextInputLayout) rootView.findViewById(R.id.fr_reg_layout_login);
            nameInput = (TextInputLayout) rootView.findViewById(R.id.fr_reg_layout_name);
            surnameInput = (TextInputLayout) rootView.findViewById(R.id.fr_reg_layout_surname);
            emailInput = (TextInputLayout) rootView.findViewById(R.id.fr_reg_layout_email);
            phoneNumberInput = (TextInputLayout) rootView.findViewById(R.id.fr_reg_layout_phone_number);
            cityInput = (TextInputLayout) rootView.findViewById(R.id.fr_reg_layout_city);
            passInput = (TextInputLayout) rootView.findViewById(R.id.fr_reg_layout_pass);

            layoutCountryCodeAndPhone = (LinearLayout) rootView.findViewById(R.id.fr_reg_country_code_and_phone_layout);
            imageViewFlag = (ImageView) rootView.findViewById(R.id.fr_reg_flag_view);
            textViewCountryCode = (TextView) rootView.findViewById(R.id.fr_reg_country_code_text);
            btnChooseCity = (Button) rootView.findViewById(R.id.fg_reg_btn_choose_city);
            btnRegister = (Button) rootView.findViewById(R.id.fr_reg_btn_reg);
            btnRegisterFB = (Button) rootView.findViewById(R.id.fr_reg_btn_fb);
            btnAddphoto = (Button) rootView.findViewById(R.id.fr_reg_btn_add_photo);
            circleProfileImageView = (CircleImageView) rootView.findViewById(R.id.fr_reg_profile_circle_image);
            profileDefaultImage = (ImageView) rootView.findViewById(R.id.fr_reg_profile_default_image);

            profileDefaultImage.setVisibility(View.VISIBLE);
            circleProfileImageView.setVisibility(View.INVISIBLE);

            setCurrentCountry(Country.getCurrentLocaleCountryFromAvailable());

            layoutCountryCodeAndPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    CountryPickerDialog countryPicker = new CountryPickerDialog(getContext(),
                            new CountryPickerDialog.CountryPickerCallbacks() {
                        @Override
                        public void onCountrySelected(Country country) {
                            setCurrentCountry(country);
                        }
                    },
                            currentCountry.isoCode, true);

                    countryPicker.show();
                }
            });
            btnChooseCity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (currentCountryCities != null) {
                        CityPickerDialog cityPicker = new CityPickerDialog(getContext(), new CityPickerDialog.CityPickerCallbacks() {
                            @Override
                            public void onCitySelected(City city) {
                                setCurrentCity(city);
                            }
                        }, currentCountryCities);
                        cityPicker.show();
                    }
                }
            });
            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    registerClick();
                }
            });
            btnRegisterFB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    registerClickFB();
                }
            });
            btnAddphoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectImage();
                }
            });
            circleProfileImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectImage();
                }
            });
            profileDefaultImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectImage();
                }
            });

            return rootView;
        }

        private void setCurrentCountry(Country country){

            this.currentCountry = country;
            imageViewFlag.setImageResource(country.getResourceID());
            textViewCountryCode.setText("+" + country.dialingCode);

            Helper.showLoadingDialog(getActivity(), getString(R.string.loading_cities), getString(R.string.please_wait));

            ApiService.getCitiesByCountryID(country.id, new ApiService.ApiResponse() {
                @Override
                public void response(Boolean isSuccess, Object payload) {

                    Helper.dismissLoadingDialog();

                    ArrayList<City> cities = (ArrayList<City>) payload;

                    currentCountryCities = cities;

                    City firstCity = cities.get(0);
                    setCurrentCity(firstCity);
                }
            });
        }

        private void setCurrentCity(City city){
            currentCity = city;
            tfCity.setText(city.name);
        }

        private ChoosePhotoDialog.ChoosePhotoDialogItem choosePhotoDialogItem;
        private int REQUEST_CAMERA = 0, SELECT_FILE = 1;

        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

            switch (requestCode) {
                case Helper.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:

                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if(choosePhotoDialogItem.id == ChoosePhotoDialog.TAKE_PHOTO)
                            cameraIntent();
                        else if(choosePhotoDialogItem.id == ChoosePhotoDialog.CHOOSE_FROM_LIBRARY)
                            galleryIntent();
                    } else {
                        //code for deny
                    }
                    break;
            }
        }

        private void selectImage() {

            ChoosePhotoDialog choosePhotoPicker = new ChoosePhotoDialog(getContext(), new ChoosePhotoDialog.ChoosePhotoDialogCallback() {
                @Override
                public void onSelect(ChoosePhotoDialog.ChoosePhotoDialogItem item) {

                    choosePhotoDialogItem = item;
                    boolean result = Helper.checkPermissionReadExternalStorage(getContext(), RegisterFragment.this);

                    switch(item.id){
                        case ChoosePhotoDialog.TAKE_PHOTO:
                            if(result)
                                cameraIntent();
                            break;
                        case ChoosePhotoDialog.CHOOSE_FROM_LIBRARY:
                            if(result)
                                galleryIntent();
                            break;
                    }
                }
            });

            choosePhotoPicker.show();
        }

        private void galleryIntent()
        {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);//
            startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
        }

        private void cameraIntent()
        {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE)
                    onSelectFromGalleryResult(data);
                else if (requestCode == REQUEST_CAMERA)
                    onCaptureImageResult(data);
            }
        }

        private void onCaptureImageResult(Intent data) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            circleProfileImageView.setImageBitmap(thumbnail);
        }

        @SuppressWarnings("deprecation")
        private void onSelectFromGalleryResult(Intent data) {

            Bitmap bm=null;
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            profileDefaultImage.setVisibility(View.INVISIBLE);
            circleProfileImageView.setVisibility(View.VISIBLE);
            circleProfileImageView.setImageBitmap(bm);
        }


        private void registerClickFB(){
            LoginRegisterActivity activity = (LoginRegisterActivity) getActivity();
            activity.fbRegister();
        }

        private void registerClick() {

            if(!validateLogin()){
                return;
            }
            if(!validateName()){
                return;
            }
            if(!validateSurname()){
                return;
            }
            if(!validatePhoneNumber()){
                return;
            }
            if(!validateEmail()){
                return;
            }
            if(!validatePass()){
                return;
            }
            if(currentCountry == null){
                Helper.showAlert(getActivity(), R.string.registration_no_country_title, R.string.registration_no_country_message);
                return;
            }
            if(currentCity == null){
                Helper.showAlert(getActivity(), R.string.registration_no_city_title, R.string.registration_no_city_message);
                return;
            }

            Bitmap profileBitmap = Helper.getBitmapFromImageView(circleProfileImageView);

            if(profileBitmap == null){
                Helper.showAlert(getActivity(), R.string.registration_no_profile_photo_title, R.string.registration_no_profile_photo_message);
                return;
            }

            RegisterVO registerVO = new RegisterVO();

            registerVO.login = tfLogin.getText().toString();
            registerVO.pass = tfPass.getText().toString();
            registerVO.name = tfName.getText().toString();
            registerVO.surname = tfSurname.getText().toString();
            registerVO.phone = tfPhoneNumber.getText().toString();
            registerVO.email = tfEmail.getText().toString();
            registerVO.cityId = currentCity.id;
            registerVO.countryId = currentCountry.id;
            registerVO.profileImageBitmap = profileBitmap;

            LoginRegisterActivity activity = (LoginRegisterActivity) getActivity();
            activity.tryRegister(registerVO);
        }

        private boolean validateLogin() {
            if (!Helper.isValidPassword(tfLogin.getText().toString(), false)) {
                loginInput.setErrorEnabled(true);
                loginInput.setError(getString(R.string.wrong_login));
                requestFocus(loginInput);
                btnRegister.setEnabled(false);
                return false;
            } else {
                loginInput.setError(null);
                loginInput.setErrorEnabled(false);
            }

            return true;
        }

        private boolean validateName() {
            if (!Helper.validateFirstName(tfName.getText().toString(), 3)) {
                nameInput.setErrorEnabled(true);
                nameInput.setError(getString(R.string.wrong_name));
                requestFocus(nameInput);
                btnRegister.setEnabled(false);
                return false;
            } else {
                nameInput.setError(null);
                nameInput.setErrorEnabled(false);
            }

            return true;
        }

        private boolean validateSurname() {
            if (!Helper.validateLastName(tfSurname.getText().toString(), 3)) {
                surnameInput.setErrorEnabled(true);
                surnameInput.setError(getString(R.string.wrong_surname));
                requestFocus(surnameInput);
                btnRegister.setEnabled(false);
                return false;
            } else {
                surnameInput.setError(null);
                surnameInput.setErrorEnabled(false);
            }

            return true;
        }

        private boolean validateEmail() {
            if (!Helper.isValidEmail(tfEmail.getText().toString())) {
                emailInput.setErrorEnabled(true);
                emailInput.setError(getString(R.string.wrong_email));
                requestFocus(emailInput);
                btnRegister.setEnabled(false);
                return false;
            } else {
                emailInput.setError(null);
                emailInput.setErrorEnabled(false);
            }

            return true;
        }

        private boolean validatePhoneNumber() {
            if (!Helper.isValidPhone(tfPhoneNumber.getText().toString())) {
                phoneNumberInput.setErrorEnabled(true);
                phoneNumberInput.setError(getString(R.string.wrong_phone_number));
                requestFocus(phoneNumberInput);
                btnRegister.setEnabled(false);
                return false;
            } else {
                phoneNumberInput.setError(null);
                phoneNumberInput.setErrorEnabled(false);
            }

            return true;
        }

        private boolean validatePass() {
            if (!Helper.isValidPassword(tfPass.getText().toString(), false)) {
                passInput.setErrorEnabled(true);
                passInput.setError(getString(R.string.wrong_pass));
                requestFocus(passInput);
                btnRegister.setEnabled(false);
                return false;
            } else {
                passInput.setError(null);
                passInput.setErrorEnabled(false);
            }

            return true;
        }

        private void requestFocus(View view) {
            if (view.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        }

        private class RegisterTextWatcher implements TextWatcher {

            private View view;

            private RegisterTextWatcher(View view) {
                this.view = view;
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void afterTextChanged(Editable editable) {

                Boolean isValid = false;

                switch (view.getId()) {
                    case R.id.fr_reg_text_input_login:
                        isValid = validateLogin();
                        break;
                    case R.id.fr_reg_text_input_name:
                        isValid = validateName();
                        break;
                    case R.id.fr_reg_text_input_surname:
                        isValid = validateSurname();
                        break;
                    case R.id.fr_reg_text_input_phone_number:
                        isValid = validatePhoneNumber();
                        break;
                    case R.id.fr_reg_text_input_email:
                        isValid = validateEmail();
                        break;
                    case R.id.fr_reg_text_input_pass:
                        isValid = validatePass();
                        break;
                }

                btnRegister.setEnabled(isValid);
            }
        }
    }
}
