package com.gotop.yammy.login_register.city_picker;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatDialog;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.gotop.yammy.R;
import com.gotop.yammy.utils.Helper;

import java.util.List;

public class CityPickerDialog extends AppCompatDialog {

    public interface CityPickerCallbacks {
        void onCitySelected(City city);
    }

    private List<City> cities;
    private CityPickerCallbacks callbacks;
    private ListView listview;
    private boolean isDropDown;
    private City currentCity;

    public CityPickerDialog(Context context, CityPickerCallbacks callbacks,
                            List<City> cities) {
        super(context);
        this.callbacks = callbacks;
        this.cities = cities;
    }

    public CityPickerDialog(Context context, CityPickerCallbacks callbacks,
                            List<City> cities, View viewForPosition, City currentCity) {
        super(context);
        this.callbacks = callbacks;
        this.cities = cities;
        this.currentCity = currentCity;

        isDropDown = true;

        int[] locations = new int[2];
        viewForPosition.getLocationOnScreen(locations);
        int x = locations[0];
        int y = locations[1];

        Window window = this.getWindow();
        WindowManager.LayoutParams param = window.getAttributes();
        param.gravity = Gravity.TOP | Gravity.LEFT;
        param.x = x;
        param.y = y;
        window.setAttributes(param);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(isDropDown){
            supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        }

        setContentView(R.layout.city_picker);

        if(isDropDown){
            View titleSpace = findViewById(R.id.city_picker_title_space);
            titleSpace.setVisibility(View.GONE);
            View grayLine = findViewById(R.id.city_picker_gray_line);
            grayLine.setVisibility(View.GONE);
        }

        ViewCompat.setElevation(getWindow().getDecorView(), 3);
        listview = (ListView) findViewById(R.id.city_picker_listview);

        setTitle("Select your City");

        final int gold = ContextCompat.getColor(getContext(), R.color.gold);

        // Title
        final TextView title = (TextView)findViewById(android.support.v7.appcompat.R.id.title);
        if (title != null) {
            title.setTextColor(gold);
            Helper.setCustomTextViewFont(title, Helper.GOTHAM_PRO_REG_FONT_PATH, getContext());
        }

        CityListAdapter adapter = this.isDropDown ? new CityListAdapter(this.getContext(), cities, currentCity) : new CityListAdapter(this.getContext(), cities);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                hide();

                City city = cities.get(position);
                callbacks.onCitySelected(city);
            }
        });
    }
}
