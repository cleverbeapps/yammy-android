package com.gotop.yammy.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class GothamRegTextView extends TextView{
    public GothamRegTextView(Context context) {
        super(context);
        Helper.setCustomTextViewFont(this, Helper.GOTHAM_PRO_REG_FONT_PATH, context);
    }

    public GothamRegTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Helper.setCustomTextViewFont(this, Helper.GOTHAM_PRO_REG_FONT_PATH, context);
    }

    public GothamRegTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Helper.setCustomTextViewFont(this, Helper.GOTHAM_PRO_REG_FONT_PATH, context);
    }
}
