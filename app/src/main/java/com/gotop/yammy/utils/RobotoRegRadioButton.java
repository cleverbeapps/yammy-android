package com.gotop.yammy.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;

public class RobotoRegRadioButton extends RadioButton{

    public RobotoRegRadioButton(Context context) {
        super(context);
        Helper.setCustomButtonFont(this, Helper.ROBOTO_REG_FONT_PATH, context);
    }

    public RobotoRegRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Helper.setCustomButtonFont(this, Helper.ROBOTO_REG_FONT_PATH, context);
    }

    public RobotoRegRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Helper.setCustomButtonFont(this, Helper.ROBOTO_REG_FONT_PATH, context);
    }
}
