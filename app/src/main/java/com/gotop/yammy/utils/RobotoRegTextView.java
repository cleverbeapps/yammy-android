package com.gotop.yammy.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class RobotoRegTextView extends TextView{

    public RobotoRegTextView(Context context) {
        super(context);
        Helper.setCustomTextViewFont(this, Helper.ROBOTO_REG_FONT_PATH, context);
    }

    public RobotoRegTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Helper.setCustomTextViewFont(this, Helper.ROBOTO_REG_FONT_PATH, context);
    }

    public RobotoRegTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Helper.setCustomTextViewFont(this, Helper.ROBOTO_REG_FONT_PATH, context);
    }
}
