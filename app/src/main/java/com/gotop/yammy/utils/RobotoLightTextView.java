package com.gotop.yammy.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class RobotoLightTextView extends TextView {

    public RobotoLightTextView(Context context) {
        super(context);
        Helper.setCustomTextViewFont(this, Helper.ROBOTO_LIGHT_FONT_PATH, context);
    }

    public RobotoLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Helper.setCustomTextViewFont(this, Helper.ROBOTO_LIGHT_FONT_PATH, context);
    }

    public RobotoLightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Helper.setCustomTextViewFont(this, Helper.ROBOTO_LIGHT_FONT_PATH, context);
    }
}
