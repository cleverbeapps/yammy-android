package com.gotop.yammy.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class GothamLightTextView extends TextView{

    public GothamLightTextView(Context context) {
        super(context);
        Helper.setCustomTextViewFont(this, Helper.GOTHAM_PRO_LIGHT_FONT_PATH, context);
    }

    public GothamLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Helper.setCustomTextViewFont(this, Helper.GOTHAM_PRO_LIGHT_FONT_PATH, context);
    }

    public GothamLightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Helper.setCustomTextViewFont(this, Helper.GOTHAM_PRO_LIGHT_FONT_PATH, context);
    }
}
