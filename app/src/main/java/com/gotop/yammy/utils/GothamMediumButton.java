package com.gotop.yammy.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class GothamMediumButton extends Button{

    public GothamMediumButton(Context context) {
        super(context);
        Helper.setCustomButtonFont(this, Helper.GOTHAM_PRO_MEDIUM_FONT_PATH, context);
    }

    public GothamMediumButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Helper.setCustomButtonFont(this, Helper.GOTHAM_PRO_MEDIUM_FONT_PATH, context);
    }

    public GothamMediumButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Helper.setCustomButtonFont(this, Helper.GOTHAM_PRO_MEDIUM_FONT_PATH, context);
    }
}
