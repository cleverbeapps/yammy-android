package com.gotop.yammy.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class GothamMediumTextView extends TextView{

    public GothamMediumTextView(Context context) {
        super(context);
        Helper.setCustomTextViewFont(this, Helper.GOTHAM_PRO_MEDIUM_FONT_PATH, context);
    }

    public GothamMediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Helper.setCustomTextViewFont(this, Helper.GOTHAM_PRO_MEDIUM_FONT_PATH, context);
    }

    public GothamMediumTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Helper.setCustomTextViewFont(this, Helper.GOTHAM_PRO_MEDIUM_FONT_PATH, context);
    }
}
